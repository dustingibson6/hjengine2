#include "gltext.h"

GLTextFactory::GLTextFactory()
{
	gltext_map = new std::map<std::string,GLText>();
}

void GLTextFactory::AddText(std::string name, std::string type, std::string text, CPoint pnt, CPoint max_size, int size, Color font_color)
{
	gltext_map->insert(std::pair<std::string,GLText>(name, GLText(type,text,pnt,max_size,size,font_color)));
}

void GLTextFactory::RenderTexts()
{
	std::map<std::string,GLText>::iterator it = gltext_map->begin();
	for( it=gltext_map->begin(); it != gltext_map->end(); ++it) {
		it->second.RenderText();
	}
}

void GLText::ChangeColors(Color font_color)
{
	this->font_color = font_color;
	SDL_Color sdl_font_color = { font_color.r,font_color.g,font_color.b,font_color.a };
	for(int i=0; i < text_vector.size(); i++)  {
		SDL_Surface* font_surface = TTF_RenderText_Blended(font, text_vector[i].c_str(), sdl_font_color);
		//font_textures.push_back(0);
		glDeleteTextures(1, &font_textures.at(i));
		glGenTextures(1, &font_textures.at(i));
		glBindTexture(GL_TEXTURE_2D, font_textures.at(i));
		glTexImage2D(GL_TEXTURE_2D, 0, 4, font_surface->w, font_surface->h, 0, GL_BGRA, GL_UNSIGNED_BYTE, font_surface->pixels);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glBindTexture(GL_TEXTURE_2D, 0);
		SDL_FreeSurface(font_surface);
	}
}

void GLText::ChangePosition(CPoint pos)
{
	this->x = pos.x;
	this->y = pos.y;
	for (int i = 0; i <font_textures.size(); i++) {


		SDL_Color sdl_font_color = { font_color.r,font_color.g,font_color.b,font_color.a };
		SDL_Surface* font_surface = TTF_RenderText_Blended(font, text_vector[i].c_str(), sdl_font_color);


		std::vector<GLuint> index_vector = std::vector<GLuint>();

		index_vector.push_back(0);
		index_vector.push_back(1);
		index_vector.push_back(2);
		index_vector.push_back(3);

		std::vector<GLfloat> vertex_vector = GetVertexVector(i, font_surface);
		std::vector<GLfloat> clip_vector = GetClipVector();

		GLuint* index_data = &index_vector[0];
		GLfloat* vertex_data = &vertex_vector[0];
		GLfloat* clip_data = &clip_vector[0];

		poly_shaders[i]->LoadArrayData(index_data, vertex_data, clip_data, index_vector.size(), vertex_vector.size(), clip_vector.size());
		SDL_FreeSurface(font_surface);
	}
}

void GLText::Sizing()
{
	x = this->x * Globals::GetInstance().screen_ratx;
	y = this->y * Globals::GetInstance().screen_ratx;
}

void GLTextFactory::ChangeText( std::string name, std::string new_text )
{
	gltext_map->at(name).ChangeText( new_text );
}

GLText::GLText()
{
}

int GLText::GetFontHeight()
{
	return 0;
}

CPoint GLText::GetCurrentDim()
{
	return this->GetTextDim(this->text);
}

CPoint GLText::GetTextDim( std::string focus_text )
{
	if( focus_text == " " || focus_text == "" )
		return CPoint( 0,0 );
	SDL_Color font_color = {0,0,0};
	SDL_Surface* surface = TTF_RenderText_Blended( font, focus_text.c_str(), font_color );
	int t_w = surface->w * (Globals::GetInstance().screen_width / 1280.0f);
	int t_h = surface->h * (Globals::GetInstance().screen_height / 700.0f);
	SDL_FreeSurface( surface );
	return CPoint( t_w, t_h );
}

void GLText::RenderText( )
{
	for(int i=0; i < font_textures.size(); i++ ) {
		glBindTexture( GL_TEXTURE_2D, font_textures.at(i) );
		poly_shaders.at(i)->BindShader();
		//poly_shaders.at(i)->SetRotationVector(1.0f, 1.0f, 1.0f, 1.0f);
		poly_shaders.at(i)->SetTransVector( 0.0f, 0.0f );
		poly_shaders.at(i)->RenderStatic();
		glBindTexture( GL_TEXTURE_2D, 0);
		poly_shaders.at(i)->UnbindShader();
	}
}

void GLText::Reset()
{
	for(int i=0; i < text_vector.size(); i++) {
		glDeleteTextures( 1, &font_textures[i] );
		font_textures.pop_back();
		text_vector.pop_back();
	}
	font_textures.clear();
	text_vector.clear();
	this->text = "";
}

void GLText::OffsetPos( CPoint offset )
{
	ChangePosition(CPoint(x + offset.x, y + offset.y));
}

void GLText::ChangeText(std::string new_text)
{
	if( new_text[0] != 8 )
		this->text += new_text;
	else
		if( this->text.length() > 0 )
			this->text.pop_back();
	ChangeVertex( new_text );
	int row_length = 1;
	if( text_vector.size() > 0 )
		row_length = text_vector.size();
	cur_h = font_height * row_length;
	//if( this->text.length() > 0 ) {
	//	if( this->text[text.length()-1] == ""
	//	last_h = GetTextDim( std::string(1,this->text[text.length()-1]) ).y;
	//}

}

std::vector<GLfloat> GLText::GetVertexVector( int row, SDL_Surface* font_surface )
{
	GLfloat w = font_surface->w * (Globals::GetInstance().screen_width / 1280.0f);
	GLfloat h = font_surface->h * (Globals::GetInstance().screen_height / 700.0f);

	int row_y = y + font_height*row;
	end_x = this->x + w;
	end_y = row_y;
	GLfloat r_w = w  / (float)Globals::GetInstance().screen_width;
	GLfloat r_h = h  / (float)Globals::GetInstance().screen_height;
	GLfloat r_x = x / (float)Globals::GetInstance().screen_width;
	GLfloat r_y =   (float)Globals::GetInstance().screen_height - (row_y) - h;
 	r_y = r_y / (float)Globals::GetInstance().screen_height;

	std::vector<GLfloat> vertex_vector = std::vector<GLfloat>();

	vertex_vector.push_back(r_x);
	vertex_vector.push_back(r_y);

	vertex_vector.push_back(r_x+r_w);
	vertex_vector.push_back(r_y);

	vertex_vector.push_back(r_x+r_w);
	vertex_vector.push_back(r_y+r_h);

	vertex_vector.push_back(r_x);
	vertex_vector.push_back(r_y+r_h);

	return vertex_vector;

}

std::vector<GLfloat> GLText::GetClipVector()
{
	std::vector<GLfloat> clip_vector = std::vector<GLfloat>();
	
	clip_vector.push_back(0.0f);
	clip_vector.push_back(1.0f);

	clip_vector.push_back(1.0f);
	clip_vector.push_back(1.0f);

	clip_vector.push_back(1.0f);
	clip_vector.push_back(0.0f);

	clip_vector.push_back(0.0f);
	clip_vector.push_back(0.0f);

	return clip_vector;
}

void GLText::NewText(std::string new_text)
{
	Reset();
	ChangeText(new_text);
}

void GLText::ChangeVertex( std::string new_text )
{
	std::string focus_text = "";
	int last_element = 0;
	if ( new_text[0] == 8 ) {
		if ( text_vector.size() <= 0 )
			return;
		else if ( text_vector[0].length() <= 0 )
			return;
	}
	if (text_vector.size() > 0 ) {
		last_element = text_vector.size()-1;
		if( int(new_text[0]) == 8 ) {
			if( text_vector[last_element].size() > 0) {
				text_vector[last_element].pop_back();
				focus_text = text_vector[last_element];
			}
		}
		else
			focus_text = text_vector[last_element] + new_text;
		//Clear shader
		//delete poly_shaders[last_element];
		//poly_shaders.pop_back();
		//Clear texture
		glDeleteTextures( 1, &font_textures[last_element] );
		font_textures.pop_back();
		//Clear text
		if ( text_vector.size() > 0 && text_vector[last_element].size() <= 0 ) {
			text_vector.pop_back();
			if ( last_element - 1 > 0 )
				text_vector[last_element-1].pop_back();
			return;
		}
		text_vector.pop_back();
	}
	else {
		focus_text = text;
	}
	//for(int k=0; k < poly_shaders.size(); k++)
	//	delete poly_shaders[k];
	//for(int l=0; l < font_textures.size(); l++) { 
	//	glDeleteTextures( 1, &font_textures[l] );
	//}
	//poly_shaders.clear();
	//font_textures.clear();
	//text_vector.clear();
	//split string
	std::vector<std::string> string_vect = Globals::GetInstance().SplitString(focus_text,' ');
	if( focus_text[focus_text.length()-1] == ' ')
		string_vect.push_back(" ");
	for( int i=0; i < string_vect.size(); i++)
		if ( string_vect[i] == "" )
			string_vect[i] = " ";
	//Eval width
	int sum_width = 0;
	int last_line = 0;
	std::string line = "";
	if ( GetTextDim( focus_text ).x <= max_w ) {
		text_vector.push_back( focus_text );
	}
	else {
		if( string_vect.size() == 1 ) {
			string_vect.clear();
			std::string temp_string = "";
			int temp_width = 0;
			for( int i=0; i < focus_text.length(); i++ ) {
				temp_width += GetTextDim( std::string(1,focus_text[i]) ).x;
				if( temp_width > max_w ) {
					string_vect.push_back( temp_string );
					temp_width = 0;
					temp_string = "-";
				}
				temp_string += focus_text[i];
				if( i == focus_text.length() - 1 ) {
					string_vect.push_back( temp_string );
				}
			}
		}
		for( int i=0; i < string_vect.size(); i++ ) {
			sum_width = GetTextDim( line + string_vect.at(i) ).x;
			if( sum_width > max_w ) {
				text_vector.push_back( line );
				line = string_vect.at(i) + " ";
				last_line = i;
			} else {
				line += string_vect.at(i) + " ";
			}
		}
	}
	if( last_line < string_vect.size() && last_line ) {
		line.pop_back();
		text_vector.push_back( line );
	}
	this->x = x;
	this->y = y;
	for(int i=last_element; i < text_vector.size(); i++) {
		//center_x = this->x+((this->x+w)/2);
		//center_y = this->y+((this->y+h)/2);

		//TODO: Return blank if null

		SDL_Color sdl_font_color = {font_color.r,font_color.g,font_color.b,font_color.a};
		std::string text = (text_vector[i] != "") ? text_vector[i] : " ";
		SDL_Surface* font_surface = TTF_RenderText_Blended( font, text.c_str(), sdl_font_color);
		font_textures.push_back(0);
		glGenTextures(1, &font_textures.at(i));
		glBindTexture( GL_TEXTURE_2D, font_textures.at(i) );
		glTexImage2D( GL_TEXTURE_2D, 0, 4, font_surface->w, font_surface->h, 0, GL_BGRA, GL_UNSIGNED_BYTE,font_surface->pixels);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glBindTexture( GL_TEXTURE_2D, 0);
		if( poly_shaders.size() <= i ) {
			PolyShader* poly_shader = new PolyShader("texture");
			poly_shaders.push_back( poly_shader );
		}

		std::vector<GLuint> index_vector = std::vector<GLuint>();

		index_vector.push_back(0);
		index_vector.push_back(1);
		index_vector.push_back(2);
		index_vector.push_back(3);

		std::vector<GLfloat> vertex_vector = GetVertexVector( i , font_surface );
		std::vector<GLfloat> clip_vector = GetClipVector();

		GLuint* index_data = &index_vector[0];
		GLfloat* vertex_data = &vertex_vector[0];
		GLfloat* clip_data = &clip_vector[0];

		poly_shaders[i]->LoadArrayData(  index_data, vertex_data, clip_data, index_vector.size(), vertex_vector.size(), clip_vector.size() );
		vertex_vector.clear();
		index_vector.clear();
		SDL_FreeSurface(font_surface);
	}
	total_h = text_vector.size()*font_height;
}

GLText::GLText(std::string type, std::string text, CPoint pnt, CPoint max_size, int size, Color font_color)
{
	this->text = "";
	poly_shaders = std::vector<PolyShader*>();
	font_textures = std::vector<GLuint>();
	text_vector = std::vector<std::string>();
	this->font_color = font_color;
	//poly_shader = new PolyShader("texture");
	std::string fname = "res/fonts/" + type + ".ttf";
	font = TTF_OpenFont( fname.c_str() , size );
	font_height = GetTextDim("f").y;
	font_width = GetTextDim("a").x;
	total_h = font_height;
	this->last_h = font_height;
	cur_h = font_height;
	this->size = size;
	this->x = pnt.x;
	this->y = pnt.y;
	end_x = pnt.x;
	end_y = pnt.y;
	this->max_w = max_size.x;
	if( text != "" )
		ChangeText( text );
}

