#include "triangle_shader.h"

TriangleShader::TriangleShader(std::string fname, Color color, CPoint pnt, CPoint size, std::string side)
	: PolyShader( fname )
{
	this->x = pnt.x;
	this->y = pnt.y;
	this->w = size.x;
	this->h = size.y;
	this->side = side;
	this->color = color;
	tex_color_location = glGetUniformLocation(programID, "tex_color");
	//alpha_location = glGetUniformLocation( programID, "alpha" );
	BuildArrayData();
	glUseProgram(programID);
	SetRes(Globals::GetInstance().screen_width, Globals::GetInstance().screen_height);
	glUniform4f(tex_color_location, color.r, color.g, color.b, color.a);
	glUseProgram(0);
}

void TriangleShader::ChangePosition(int x, int y)
{
	this->x = x;
	this->y = y;
	BuildArrayData();
}

void TriangleShader::Sizing()
{
	this->x = x*Globals::GetInstance().screen_ratx;
	this->y = y*Globals::GetInstance().screen_raty;
	this->w = this->w*Globals::GetInstance().screen_ratx;
	this->h = this->h*Globals::GetInstance().screen_raty;
	BuildArrayData();
}

void TriangleShader::ChangeColors(Color new_colors)
{
	glUseProgram(programID);
	int r = new_colors.r;
	int g = new_colors.g;
	int b = new_colors.b;
	int a = new_colors.a;
	glUniform4f(tex_color_location, r, g, b, a);
	glUseProgram(0);
	BuildArrayData();
}


 
void TriangleShader::BuildArrayData()
{
	int temp_y = (float)Globals::GetInstance().screen_height - (y)-h;
	int center_y = (temp_y + (this->h / 2));

	GLfloat r_x = this->x / (float) Globals::GetInstance().screen_width;
	GLfloat r_y = (temp_y / (float) Globals::GetInstance().screen_height);
	GLfloat r_w = this->w / (float) Globals::GetInstance().screen_width;
	GLfloat r_h = this->h / (float) (Globals::GetInstance().screen_height);
	GLfloat r_cy = (center_y / (float)Globals::GetInstance().screen_height);

	std::vector<GLfloat> vertex_data = std::vector<GLfloat>();
	std::vector<GLfloat> clip_data = std::vector<GLfloat>();
	std::vector<GLuint> index_data = std::vector<GLuint>();
	index_data.push_back(0);
	index_data.push_back(1);
	index_data.push_back(2);
	index_data.push_back(3);


	if (side == "right") {
		vertex_data.push_back(x);
		vertex_data.push_back(temp_y);
		clip_data.push_back(0.0);
		clip_data.push_back(0.0);

		vertex_data.push_back(x + this->w);
		vertex_data.push_back(center_y);
		clip_data.push_back(1.0);
		clip_data.push_back(0.0);

		vertex_data.push_back(x + this->w);
		vertex_data.push_back(center_y);
		clip_data.push_back(1.0);
		clip_data.push_back(1.0);

		vertex_data.push_back(x);
		vertex_data.push_back(temp_y + this->h);
		clip_data.push_back(0.0);
		clip_data.push_back(1.0);
	}
	else if (side == "left") {
		vertex_data.push_back(x);
		vertex_data.push_back(center_y);
		clip_data.push_back(0.0);
		clip_data.push_back(0.0);

		vertex_data.push_back(x + this->w);
		vertex_data.push_back(temp_y);
		clip_data.push_back(1.0);
		clip_data.push_back(0.0);

		vertex_data.push_back(x + this->w);
		vertex_data.push_back(temp_y + this->h);
		clip_data.push_back(1.0);
		clip_data.push_back(1.0);

		vertex_data.push_back(x);
		vertex_data.push_back(center_y);
		clip_data.push_back(0.0);
		clip_data.push_back(1.0);
	}


	LoadArrayData( &index_data[0], &vertex_data[0], &clip_data[0], index_data.size(), vertex_data.size(), clip_data.size() );

	index_data.clear();
	vertex_data.clear();

}

TriangleShader::~TriangleShader()
{
	glDeleteBuffers(1, &VBO);
	glDeleteBuffers(1, &IBO);
	glDeleteVertexArrays(1, &VAO);
}