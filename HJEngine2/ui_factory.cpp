#include "ui_factory.h"

UIFactory::UIFactory()
{
	ui_map =  new std::map<std::string,UI*>();
	ui_vector = new std::vector<UI*>();
}

void UIFactory::AddTextInput( std::string name, CPoint pnt, CPoint max_size, int font_size, Color font_color, Color bg_color)
{
	TextInput* new_text_input = new TextInput( pnt, max_size, font_size, font_color, bg_color ); 
	ui_map->insert( std::pair<std::string, UI*>( name, new_text_input ) );
	ui_vector->push_back(new_text_input);
}

void UIFactory::AddConsole( std::string name, CPoint pnt, CPoint size )
{
	Console* console = new Console( pnt, size );
	ui_map->insert( std::pair<std::string, UI*>( name, console ));
	ui_vector->push_back(console);
}

void UIFactory::AddButton(std::string name, CPoint pnt, std::map<std::string, Definable> definable, ObjectFactory* object_factory)
{
	Button* button = new Button(pnt, definable, object_factory);
	ui_map->insert(std::pair<std::string, UI*>(name,button));
	ui_vector->push_back(button);
}

void UIFactory::AddToggleBox(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory, bool checked)
{
	ToggleBox* toggle_box = new ToggleBox(pnt, definables, object_factory, checked);
	ui_map->insert(std::pair<std::string, UI*>(name, toggle_box));
	ui_vector->push_back(toggle_box);
}

void UIFactory::AddSelectBox(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory, std::vector<SelectChoice> choices)
{
	SelectBox* select_box = new SelectBox(pnt, definables, object_factory, choices);
	ui_map->insert(std::pair<std::string, UI*>(name, select_box));
	ui_vector->push_back(select_box);
}

void UIFactory::AddImage(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory)
{
	ImageHead* image = new ImageHead(pnt, definables, object_factory);
	ui_map->insert(std::pair<std::string, UI*>(name, image));
	ui_vector->push_back(image);
}

void UIFactory::AddPane(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory)
{
	Pane* pane = new Pane(pnt, definables, object_factory);
	ui_map->insert(std::pair<std::string, UI*>(name, pane));
	ui_vector->push_back(pane);
}

void UIFactory::AddLabel(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory)
{
	Label* label = new Label(pnt, definables, object_factory);
	ui_map->insert(std::pair<std::string, UI*>(name, label));
	ui_vector->push_back(label);
}

void UIFactory::AddValueBox(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory)
{
	ValueBox* value_box = new ValueBox(pnt, definables, object_factory);
	ui_map->insert(std::pair<std::string, UI*>(name, value_box));
	ui_vector->push_back(value_box);
}

void UIFactory::ResizeUIs()
{
	std::map<std::string, UI*>::iterator it = ui_map->begin();
	for (it = ui_map->begin(); it != ui_map->end(); ++it) {
		it->second->Sizing();
	}
}

void UIFactory::RenderUIs()
{
	for (int i = 0; i < ui_vector->size(); i++) {
		ui_vector->at(i)->PollInput();
		ui_vector->at(i)->RenderUI();
	}
}

std::vector<UI*> UIFactory::GetUIList()
{
	std::vector<UI*> uis = std::vector<UI*>();
	std::map<std::string, UI*>::iterator it = ui_map->begin();
	for (it = ui_map->begin(); it != ui_map->end(); ++it) {
		uis.push_back(it->second);
	}
	return uis;
}

std::vector<UI*> UIFactory::GetClicked()
{
	std::vector<UI*> clicked_uis = std::vector<UI*>();
	std::map<std::string, UI*>::iterator it = ui_map->begin();
	for (it = ui_map->begin(); it != ui_map->end(); ++it) {
		if (it->second->events.click)
			clicked_uis.push_back(it->second);
	}
	return clicked_uis;
}