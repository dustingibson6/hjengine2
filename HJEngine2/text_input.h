#ifndef TEXT_INPUT_H
#define TEXT_INPUT_H

#include "ui.h"
#include "gltext.h"
#include "surface_shader.h"
#include "color.h"

class TextInput : public UI {
public:
	TextInput();
	TextInput(CPoint pnt, CPoint max_size, int font_size, Color font_color, Color bg_color);
	void RenderUI();
	void PollInput();
	void Reset();
	GLText gl_text;
	std::string text;
private:
	CPoint padding;
	SurfaceShader* surface_shader;
	SurfaceShader* cursor_shader;
};


#endif