#include "ui.h"

UI::UI(std::string type, CPoint pnt, std::map<std::string,Definable> definable, ObjectFactory* object_factory)
{
	this->type = type;
	this->x = pnt.x;
	this->y = pnt.y;
	this->event_queue = std::vector<std::string>();
	this->definables = definable;
	this->object_factory = object_factory;
}

void UI::RenderUI()
{

}

void UI::PollInput()
{

}

void UI::Sizing()
{

}

std::string UI::GetValueString()
{
	return "";
}

CPoint UI::GetValueCPoint()
{
	return CPoint(0, 0);
}

Definable UI::GetDefinable(std::string key)
{
	return definables.at(key);
}

Events::Events()
{
	click = false;
}

void Events::Reset()
{
	click = false;
}