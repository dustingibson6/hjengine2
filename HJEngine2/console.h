#ifndef CONSOLE_H
#define CONSOLE_H

#include <cstdint>
#include <stdio.h>
#include <SDL.h>
#include <SDL_TTF.h>
#include <GL/glew.h>
#include <String>
#include <vector>
#include "ui.h"
#include "globals.h"
#include "text_input.h"
#include "surface_shader.h"

class Console : public UI {
public:
	Console( CPoint pnt, CPoint size );
	Console( );
	void RedoText( int inserted_height );
	void RenderUI(  );
	std::vector<GLText> gltext_history;
	TextInput input;
private:
	SurfaceShader* surface_shader;
	void RedoText();
	
};

#endif