#ifndef GLTEXT_H
#define GLTEXT_H

#include <cstdint>
#include <stdio.h>
#include <SDL.h>
#include <SDL_TTF.h>
#include <GL/glew.h>
#include <String>
#include <map>
#include <vector>
#include <boost/algorithm/string.hpp>
#include "globals.h"
#include "poly_shader.h"
#include "color.h"

class GLText {
public:
	GLText();
	GLText(std::string type, std::string text, CPoint pnt, CPoint max_size, int size, Color font_color);
	CPoint GetTextDim(std::string);
	CPoint GetCurrentDim();
	int GetFontHeight();
	int font_height;
	int font_width;
	void NewText(std::string new_text);
	void ChangeText(std::string new_text);
	void ChangeVertex( std::string );
	void ChangeColors(Color font_color);
	void RenderText();
	void OffsetPos( CPoint offset );
	void Reset();
	void Sizing();
	void ChangePosition(CPoint pos);
	int end_x;
	int end_y;
	int cur_h;
	int last_h;
	int max_w;
	int max_h;
	int total_h;
	std::string text;
private:
	TTF_Font* font;
	std::vector<GLuint> font_textures;
	GLfloat center_x;
	GLfloat center_y;
	int size;
	std::vector<PolyShader*> poly_shaders;
	std::vector<std::string> text_vector;
	std::vector<GLfloat> GetVertexVector( int row, SDL_Surface* font );
	std::vector<GLfloat> GetClipVector();
	std::string type;
	int x;
	int y;
	Color font_color;
};

class GLTextFactory {
public:
	GLTextFactory();
	void AddText(std::string name, std::string type, std::string text, CPoint pnt, CPoint max_size, int size, Color font_color);
	void ChangeText( std::string name, std::string new_text );
	void RenderTexts( );
private:
	std::map<std::string,GLText>* gltext_map;
};


#endif