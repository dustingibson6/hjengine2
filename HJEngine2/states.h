#ifndef STATE_H
#define STATE_H

#include <cstdint>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <boost\lexical_cast.hpp>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_keyboard.h>
#include <SDL_keycode.h>
#include <SDL_TTF.h> 
#include <map>
#include <string>
#include <vector>
#include "game.h"
#include "gltext.h"
#include "color.h"

class Game;

class State {
public:
	State(std::string,std::string);
	virtual void HandleInput(Game*,std::vector<int>*,std::vector<int>*);
	void RunState();
	void TestFunct(Game*);
	std::string GetName();
	std::string status;
private:
	std::string type;
	std::string name;
};

class GameState : public State {
public:
	GameState(std::string,std::string);
	void HandleInput(Game*,std::vector<int>*,std::vector<int>*) override;
	void RunState();
};

class MenuState : public State {
public:
	MenuState(std::string,std::string);
	void HandleInput(Game*,std::vector<int>*,std::vector<int>*) override;
	void RunState();
};

class StateMachine {
public:
	StateMachine(std::string,std::string);
	~StateMachine();
	void TransitionState(std::string state_name);
	void AddState(std::string,std::string);
	void ControlStates(Game*);
	void RemoveState();
	State GetCurrentState();
private:
	State* current_state;
	TTF_Font* font;
	GLText fps_label;
};


#endif