#include "toggle_box.h"

ToggleBox::ToggleBox(CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory, bool checked) :
	UI("toggle box", pnt, definables, object_factory)
{
	this->max_size = definables.at("text size").ToCPoint();
	this->size = definables.at("size").ToCPoint();
	this->font = definables.at("font").ToString();
	this->text = definables.at("text").ToString();
	this->hover_constant = definables.at("hover constant").ToGLfloat();
	this->font_size = definables.at("font size").ToInt();
	this->font_color = definables.at("font color").ToColor();
	gl_text = GLText(font, text, CPoint(pnt.x + size.y*2,pnt.y), max_size, font_size, font_color);
	int text_height = gl_text.GetCurrentDim().y;
	gl_text.ChangePosition(CPoint(pnt.x + (2*text_height), pnt.y));
	status = "normal";
	Color alpha_color = Color(0,0,0,0);
	this->checked = checked;
	surface_shader = new SurfaceShader("surface", font_color, alpha_color, pnt, CPoint(text_height, text_height), 0, Globals::GetInstance().ToRads(45.0));
	int select_x = pnt.x + (text_height / 4);
	int select_y = pnt.y + (text_height / 4);
	float s_constant = 0.8;
	Color select_color = font_color.GetScaledColor(s_constant);
	select_shader = new SurfaceShader("surface", select_color, alpha_color, CPoint(select_x,select_y), CPoint(text_height*0.5, text_height*0.5), 0, Globals::GetInstance().ToRads(45.0));
}

void ToggleBox::Sizing()
{
	this->x = (float)this->x * Globals::GetInstance().screen_ratx;
	this->y = (float)this->y * Globals::GetInstance().screen_raty;
	this->size.x = (float)this->size.x * Globals::GetInstance().screen_ratx;
	this->size.y = (float)this->size.y * Globals::GetInstance().screen_raty;
	this->max_size.x = (float)this->max_size.x * Globals::GetInstance().screen_ratx;
	this->max_size.y = (float)this->max_size.y * Globals::GetInstance().screen_raty;
	gl_text = GLText(font, text, CPoint(x + y * 2, y), max_size, font_size, font_color);
	int text_height = gl_text.GetCurrentDim().y;
	status = "normal";
	Color alpha_color = Color(0, 0, 0, 0);
	surface_shader = new SurfaceShader("surface", font_color, alpha_color, CPoint(this->x,this->y), CPoint(text_height, text_height), 0, Globals::GetInstance().ToRads(45.0));
	int select_x = this->x + (text_height / 4);
	int select_y = this->y + (text_height / 4);
	float s_constant = 0.8;
	Color select_color = font_color.GetScaledColor(s_constant);
	gl_text.ChangePosition(CPoint(x + (text_height*2), y));
	select_shader = new SurfaceShader("surface", select_color, alpha_color, CPoint(select_x, select_y), CPoint(text_height*0.5, text_height*0.5), 0, Globals::GetInstance().ToRads(45.0));
}

void ToggleBox::PollInput()
{
	int mouse_x = 0;
	int mouse_y = 0;
	Color new_color = font_color.GetScaledColor(hover_constant);
	SDL_GetMouseState(&mouse_x, &mouse_y);
	//mouse_x = mouse_x*(1280 / Globals::GetInstance().screen_width);
	//mouse_y = mouse_y*(700 / Globals::GetInstance().screen_height);
	if (mouse_x >= this->x && mouse_x <= this->x + this->size.x && mouse_y >= this->y && mouse_y <= this->y + this->size.y) {
		if (Globals::GetInstance().click)
		{
			if (!checked)
				checked = true;
			else
				checked = false;
		}
		if (status == "normal")
			status = "hover start";
	}
	else {
		if (status == "hover")
			status = "hover out";
	}
	if (status == "hover start") {
		gl_text.ChangeColors(new_color);
		surface_shader->ChangeColors(new_color);
		status = "hover";
	}
	if (status == "hover out") {
		gl_text.ChangeColors(font_color);
		surface_shader->ChangeColors(font_color);
		status = "normal";
	}
	//Globals::GetInstance().
}

void ToggleBox::SetValue(bool value)
{
	this->checked = value;
	//Sizing();
}

void ToggleBox::RenderUI()
{
	surface_shader->BindShader();
	surface_shader->RenderStatic();
	surface_shader->UnbindShader();
	if (checked) {
		select_shader->BindShader();
		select_shader->RenderStatic();
		select_shader->UnbindShader();
	}
	gl_text.RenderText();
}