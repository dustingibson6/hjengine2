#ifndef EFFECT_H
#define EFFECT_H

#include <stdio.h>
#include <SDL.h>
#include <map>
#include <string>
#include <vector>
#include <iostream>
#include "shader.h"
#include "color.h"
#include "point.h"
#include "gauss_vector.h"

class Effect {
public:
	Effect(std::string,int,int,int,int);
	~Effect();
	virtual void RunEffect();
	virtual void Sizing();
	std::string name;
	int x;
	int y;
	int width;
	int height;
	std::string flag;

protected:
	Shader* shader;
	GLuint VBO;
	GLuint IBO;
	GLuint VAO;
};

class RainVector {
public:
	RainVector(GLfloat, GLfloat);
	GLfloat x;
	GLfloat y;
	GLfloat w;
	GLfloat h;
};

class RainGroup {
public:
	RainGroup(GLfloat,GLfloat,GLfloat,GLfloat);
	GLfloat x1;
	GLfloat x2;
	GLfloat y1;
	GLfloat y2;
	std::vector<RainVector> rain_vector;
};

class Rain : public Effect {
public:
	Rain(std::string,int,int,int);
	~Rain();
	std::vector<RainGroup> rain_group;
	void Sizing();
	GLfloat vector_spacing;
	int group_spacing;
	GLfloat min_width;
	GLfloat min_height;
	GLfloat velocity;
	GLfloat slope;
	glm::vec3 color;
private:

};

class Fade : public Effect {
public:
	Fade(std::string,int,int,int);
	~Fade();
	void StartEffect();
	void RunEffect();
	void Sizing();
	int speed;

private:
	GLuint speed_location;
	GLuint vertex_location;
	GLfloat alpha;
};


class GrayScale : public Effect {
public:
	GrayScale(std::string,int,int,int,int);
	void RunEffect();
	void Sizing();
	~GrayScale();

private:
	SDL_Palette* gray_palette;
};

class Star {
public:
	Star(CPoint pnt, Color color, GLfloat speed, GLfloat size);
	CPoint pnt;
	Color color;
	GLfloat speed;
	GLfloat size;
};

class StarField : public Effect {
public:
	StarField(std::string name, int stars_num);
	~StarField();
	void StartEffect();
	void RunEffect();
	void LoadBuffers();
	void MoveStars();
	void ReloadBuffers();
	void Sizing();
	void GenerateStars();

private:
	std::vector<Star> stars;
	std::vector<GLuint> index_data;
	std::vector<GLfloat> vertex_data;
	std::vector<GLfloat> color_data;
	std::vector<GLfloat> tex_coord_data;
	std::vector<Color> star_colors;
	GLuint vertex_location;
	GLuint trans_location;
	GLuint size_location;
	GLuint tex_coord_location;
	GLuint color_location;
	GLuint star_VBO[3];
	GaussVector star_size;
	GaussVector star_speed;
	int stars_num;
	//boost::basic_thread_pool threadpool;
};

class EffectFactory {
public:
	EffectFactory();
	~EffectFactory();
	void AddFadeEffect(std::string,int,int,int);
	void AddGrayScaleEffect(std::string,int,int,int,int);
	void AddStarFieldEffect(std::string name, int star_num);
	void RemoveEffect(std::string);
	void RunEffects();
	void ResizeEffects();

private:
	std::map<std::string,Effect*>* effect_map;
};

#endif