#ifndef UI_FACTORY_H
#define UI_FACTORY_H

#include "text_input.h"
#include "toggle_box.h"
#include "ui.h"
#include "console.h"
#include "button.h"
#include "select_box.h"
#include "definable.h"
#include "color.h"
#include "image_head.h"
#include "pane.h"
#include "label.h"
#include "value_box.h"

class UIFactory {

public:
	UIFactory();
	void AddTextInput( std::string name, CPoint pnt, CPoint max_size, int font_size, Color font_color, Color bg_color);
	void AddConsole( std::string name, CPoint pnt, CPoint size );
	void AddButton( std::string name, CPoint pnt, std::map<std::string, Definable> definable, ObjectFactory* object_factory);
	void AddToggleBox(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory, bool checked);
	void AddSelectBox(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory, std::vector<SelectChoice> choices);
	void AddImage(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory);
	void AddPane(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory);
	void AddLabel(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory);
	void AddValueBox(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory);
	void RenderUIs();
	void ResizeUIs();
	std::vector<UI*> GetUIList();
	std::vector<UI*> GetClicked();

private:
	std::map<std::string,UI*>* ui_map;
	std::vector<UI*>* ui_vector;
};

#endif