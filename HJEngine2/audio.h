#ifndef AUDIO_H
#define AUDIO_H

#include <SDL.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>

class Audio {
public:
	Audio(unsigned char*);
	~Audio();
	void FillAudio();
	SDL_RWops* audio_buffer;
	SDL_AudioSpec audio_spec;
	SDL_AudioDeviceID deviceid;
};

#endif