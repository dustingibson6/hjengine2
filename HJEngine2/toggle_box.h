#ifndef TOGGLE_BOX_H
#define TOGGLE_BOX_H

#include "ui.h"
#include "gltext.h"
#include "surface_shader.h"
#include "definable.h"
#include "color.h"
#include "object.h"
#include <math.h>

class ToggleBox : public UI
{
public:
	ToggleBox();
	ToggleBox(CPoint pnt, std::map<std::string,Definable> definables, ObjectFactory* object_factory, bool checked);
	bool checked;
	void SetValue(bool value);
	void PollInput();
	void Sizing();
	void RenderUI();
private:
	std::string text;
	std::string status;
	std::string font;
	int font_size;
	GLText gl_text;
	GLText gl_text_hover;
	Color font_color;
	Color bg_color;
	Color border_color;
	SurfaceShader* surface_shader;
	SurfaceShader* select_shader;
	GLfloat hover_constant;
	CPoint max_size;
};


#endif TOGGLE_BOX_H