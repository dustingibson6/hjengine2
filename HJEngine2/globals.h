#ifndef GLOBALS_H
#define GLOBALS_H

#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>
#include <GL/glew.h>
#include <glm.hpp>
#include <type_ptr.hpp>
#include <transform.hpp>
#include <SDL.h>
#include <SDL_TTF.h>
#include <string>
#include "point.h"
#include "config.h"
#include "logger.h"

#define PI 3.141592653589793

class Globals {
public:
	static Globals& GetInstance();
	std::string config_name;
	FPoint ToMeters(float,float,float,float);
	CPoint ToPixels(float,float,float,float);
	CPoint GetTextDim(std::string focus_text, std::string font_name, int font_size);
	GLfloat ToRads(float degrees);
	Logger logger;
	void Reinit();
	void Log(std::string value);
	glm::mat2 To2x2Matrix(float[4]);
	int screen_width;
	int screen_height;
	int res_width;
	int res_height;
	int key_ascii;
	int key_mod;
	int ticks;
	int frame;
	int fps;
	float sec;
	bool shift;
	bool enter;
	bool click;
	bool reinit_flag;
	bool update_text;
	bool quit_flag;
	GLfloat screen_ratx;
	GLfloat screen_raty;
	SDL_Window* sdl_window;
	std::vector<std::string> SplitString(std::string text, char delim);
	std::vector<int> num_mod;
private:
	Globals();
	Globals(Globals const&);
};

#endif