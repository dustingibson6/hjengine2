#include "console.h"
#include "color.h"

Console::Console()
	: UI("console", CPoint(0,0))
{
}

Console::Console( CPoint pnt, CPoint size ) 
	: UI( "console", pnt )
{
	gltext_history = std::vector<GLText>();
	Color font_colors = Color( 255, 255, 255, 0 );
	Color bg_colors = Color( 173, 216, 230, 128 );
	Color border_colors = Color( 0, 0, 0, 255 );
	CPoint text_pnt = CPoint( pnt.x, pnt.y + size.y );
	input = TextInput( text_pnt, size, 18, font_colors, bg_colors );
	this->size = size;
	surface_shader = new SurfaceShader( "surface", bg_colors, border_colors, pnt, size, 0);
}

void Console::RedoText( int inserted_height )
{
	for( int i = 0; i < gltext_history.size(); i++ ) {
		gltext_history[i].OffsetPos( CPoint(0, inserted_height ) );
	}
}

void Console::RenderUI()
{
	Color font_colors = Color(0, 255, 0, 0);
	if( Globals::GetInstance().enter ) {
		CPoint new_point = CPoint( this->x, this->y + size.y );
		GLText entered_text = GLText("Calibri",input.text,new_point,size,18,font_colors);
		gltext_history.push_back( entered_text );
		RedoText( entered_text.total_h*-1 );
		input.Reset();
	}
	input.RenderUI();
	surface_shader->BindShader();
	surface_shader->RenderStatic();
	surface_shader->UnbindShader();
	for( int i = 0; i < gltext_history.size(); i++)
		gltext_history[i].RenderText();
}