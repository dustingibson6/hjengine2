#include "surface_shader.h"

SurfaceShader::SurfaceShader( std::string fname, Color color, Color border_color, CPoint pnt, CPoint size, int border_size, float angle)
	: PolyShader( fname ) 
{
	this->x = pnt.x;
	this->y = pnt.y;
	this->w = size.x;
	this->h = size.y;
	this->angle = angle;
	color_buffer_id = -1;
	this->color = color;
	this->border_size = border_size;
	tex_color_location = glGetUniformLocation( programID, "tex_color") ;
	//tex_color_location = glGetUniformBlockIndex(programID, "tex_color");
	border_size_location = glGetUniformLocation( programID, "border_size");
	center_location = glGetUniformLocation(programID, "center");
	border_colors_location = glGetUniformLocation(programID, "border_color");
	//alpha_location = glGetUniformLocation( programID, "alpha" );
	BuildArrayData();
	FPoint render_border_size = FPoint( border_size / (float)this->w , border_size / (float) this->h ); 
	glUseProgram( programID );
	SetRes(Globals::GetInstance().screen_width, Globals::GetInstance().screen_height);
	glUniform2f(center_location, c_x, c_y);
	SetAngle(angle);
	glUniform4f( tex_color_location ,color.r, color.g, color.b, color.a );
	//glUniformBlockBinding(tex_color_location, this->colors[0], this->colors[1], this->colors[2], this->colors[3]);
	glUniform2f( border_size_location, render_border_size.x, render_border_size.y );
	glUniform4f( border_colors_location, border_color.r, border_color.g, border_color.b, border_color.a);
//	glUniform4f(rotation_vector_location, 0.0f, 1.0f, -1.0f, 0.0f);
	glUseProgram( 0 );
}

void SurfaceShader::Sizing()
{
	this->x = x*Globals::GetInstance().screen_ratx;
	this->y = y*Globals::GetInstance().screen_raty;
	this->w = w*Globals::GetInstance().screen_ratx;
	this->h = h*Globals::GetInstance().screen_raty;
	BuildArrayData();
}

void SurfaceShader::ChangePosition( int x, int y)
{
	this->x = x;
	this->y = y;
	BuildArrayData();
}

void SurfaceShader::ChangeSize( int w, int h )
{
	this->w = w;
	this->h = h;
	FPoint render_border_size = FPoint( border_size / (float) this->w , border_size / (float) this->h ); 
	glUseProgram( programID );
	glUniform2f( border_size_location, render_border_size.x, render_border_size.y );
	//glUniform2f(center_location, c_x, c_y);
	glUseProgram( 0 );
	BuildArrayData();
}

void SurfaceShader::ChangeColors(Color new_color)
{
	glUseProgram(programID);
	int r = new_color.r;
	int g = new_color.g;
	int b = new_color.b;
	int a = new_color.a;
	glUniform4f(tex_color_location, r, g, b, a);
	glUseProgram(0);
	BuildArrayData();
	//BuildArrayData();
}

void SurfaceShader::BuildArrayData()
{
	int temp_y = (float)Globals::GetInstance().screen_height - (y)-h;
	float my_angle = 0.7854f;
	float sin_a = sin(my_angle);
	float cos_a = cos(my_angle);
	GLfloat center_x = (this->x + (this->w / 2));
	GLfloat center_y = (temp_y + (this->h / 2));
	c_x = center_x;
	c_y = center_y;
	//this->x = center_x + ( (this->x - center_x)*cos_a - (temp_y - center_y)*sin_a );
	//temp_y = center_y + ( (this->x - center_x)*sin_a + (temp_y - center_y)*cos_a );


	GLfloat r_x = this->x / (float) Globals::GetInstance().screen_width;
	GLfloat r_y = (temp_y / (float) Globals::GetInstance().screen_height);
	GLfloat r_w = this->w / (float) Globals::GetInstance().screen_width;
	GLfloat r_h = this->h / (float) (Globals::GetInstance().screen_height);
	//r_w = 0.1;
	//r_h = 0.1;

	//c_x = center_x / (float)Globals::GetInstance().screen_width;
	//c_y = center_y / (float)Globals::GetInstance().screen_height;
	//c_x = (r_x)+(r_w / 2);
	//c_y = (r_y)+(r_h / 2);

	std::vector<GLfloat> vertex_data = std::vector<GLfloat>();
	std::vector<GLuint> index_data = std::vector<GLuint>();
	std::vector<GLfloat> clip_data = std::vector<GLfloat>();

	index_data.push_back(0);
	index_data.push_back(1);
	index_data.push_back(2);
	index_data.push_back(3);


		//vertex_data.push_back(r_x);
		//vertex_data.push_back(r_y);
		vertex_data.push_back(x);
		vertex_data.push_back(temp_y);
		clip_data.push_back(0.0);
		clip_data.push_back(0.0);

		//vertex_data.push_back(r_x + r_w);
		//vertex_data.push_back(r_y);
		vertex_data.push_back(x + this->w);
		vertex_data.push_back(temp_y);
		clip_data.push_back(1.0);
		clip_data.push_back(0.0);

		//vertex_data.push_back(r_x + r_w);
		//vertex_data.push_back(r_y + r_h);
		vertex_data.push_back(x + this->w);
		vertex_data.push_back(temp_y + this->h);
		clip_data.push_back(1.0);
		clip_data.push_back(1.0);

		//vertex_data.push_back(r_x);
		//vertex_data.push_back(r_y + r_h);
		vertex_data.push_back(x);
		vertex_data.push_back(temp_y + this->h);
		clip_data.push_back(0.0);
		clip_data.push_back(1.0);


	LoadArrayData( &index_data[0], &vertex_data[0], &clip_data[0], index_data.size(), vertex_data.size(), clip_data.size());

	index_data.clear();
	vertex_data.clear();

}

SurfaceShader::~SurfaceShader() 
{
	glDeleteBuffers(1,&VBO);
	glDeleteBuffers(1,&IBO);
	glDeleteVertexArrays(1,&VAO);
}