#ifndef LOGGER_H
#define LOGGER_H

#include <string>
#include <fstream>
#include <time.h>

class Logger {

public:
	Logger();
	Logger(std::string fname);
	void Log(std::string value);

private:
	std::string fname;
};

#endif