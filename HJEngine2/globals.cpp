#include "Globals.h"


Globals& Globals::GetInstance()
{
	static Globals instance;
	return instance;
}

Globals::Globals()
{
	ticks = 0;
	frame = 0;
	sec = 0;
	num_mod = std::vector<int>();
	num_mod.push_back(int(')'));
	num_mod.push_back(int('!'));
	num_mod.push_back(int('@'));
	num_mod.push_back(int('#'));
	num_mod.push_back(int('$'));
	num_mod.push_back(int('%'));
	num_mod.push_back(int('^'));
	num_mod.push_back(int('&'));
	num_mod.push_back(int('*'));
	num_mod.push_back(int('('));
	enter = false;
	click = false;
	reinit_flag = false;
	update_text = false;
	quit_flag = false;
	logger = Logger("main");
	fps = 0;
}

Globals::Globals(Globals const&)
{
}

GLfloat Globals::ToRads(float degrees)
{
	return  (degrees * PI) / 180.0 ;
}

CPoint Globals::ToPixels(float x,float y,float w,float h)
{
	w = w * 0.1;
	h = h * 0.1;
	y = y * -1.0;
	x  = (x - (w/2.0))*10.0;
	y = (y - (h/2.0))*10.0;
	//x = x * 10.0;
	//y = y * -10.0;
	return CPoint(x,y);
}

FPoint Globals::ToMeters(float x,float y,float w,float h)
{
	x = ((w/2.0) + x)*0.1;
	y = ((h/2.0) + y)*-0.1;
	//x = x * 0.1;
	//y = y * -0.1;
	return FPoint(x,y);
}

glm::mat2 Globals::To2x2Matrix(float data[4])
{
	glm::mat2 results = glm::make_mat2(data);
	return glm::transpose(results);
}

std::vector<std::string> Globals::SplitString(std::string text, char delim)
{
	std::vector<std::string> string_vector = std::vector<std::string>();
	std::string temp_string = "";
	for(int i = 0; i < text.length(); i++) {
		if( text.at(i) != delim && i != text.length()-1)
			temp_string += text.at(i);
		else {
			if( text.at(i) != delim )
				temp_string += text.at(i);
			string_vector.push_back( temp_string );
			temp_string = "";
		}
	}
	return string_vector;
}

CPoint Globals::GetTextDim(std::string focus_text, std::string font_name, int font_size)
{
	if (focus_text == " " || focus_text == "")
		return CPoint(0, 0);
	SDL_Color font_color = { 0,0,0 };
	std::string fname = "res/fonts/" + font_name + ".ttf";
	TTF_Font* font = TTF_OpenFont(fname.c_str(), font_size);
	SDL_Surface* surface = TTF_RenderText_Blended(font, focus_text.c_str(), font_color);
	int t_w = surface->w * (screen_width / 1280);
	int t_h = surface->h * (screen_height / 700);
	SDL_FreeSurface(surface);
	return CPoint(t_w, t_h);
}

void Globals::Log(std::string value)
{
	logger.Log(value);
}

void Globals::Reinit()
{
	//InitDisplay();
	//InitScreen();
	Config main_config = Config("main");
	int new_width = main_config.GetCPointValue("resolution").x;
	int new_height = main_config.GetCPointValue("resolution").y;
	GLfloat test_x = (float)new_width / (float)Globals::GetInstance().screen_width;
	Globals::GetInstance().screen_ratx = (float)new_width / (float)Globals::GetInstance().screen_width;
	Globals::GetInstance().screen_raty = (float)new_height / (float)Globals::GetInstance().screen_height;
	Globals::GetInstance().screen_width = new_width;
	Globals::GetInstance().screen_height = new_height;
	SDL_SetWindowSize(sdl_window, Globals::GetInstance().screen_width, Globals::GetInstance().screen_height);
	//this->sdl_window = NULL;
	//this->screen_surface = NULL;
	//SetUpOpenGL();
	glViewport(0, 0, Globals::GetInstance().screen_width, Globals::GetInstance().screen_height);
}