#include "player.h"

Player::Player(b2World* tile_world,ObjectInstance* objInst)
{
	col_dir = std::vector<int>(4);
	parts = std::vector<Property*>();
	pnt = objInst->pnt;

	//parts.push_back(new Property(*objInst->prop_map->at("default head")));
	//parts.push_back(new Property(*objInst->prop_map->at("default body")));
	//parts.push_back(new Property(*objInst->prop_map->at("default arm")));
	//parts.push_back(new Property(*objInst->prop_map->at("default arm")));
	//parts.push_back(new Property(*objInst->prop_map->at("default leg")));
	parts.push_back(new Property(*objInst->prop_map->at("default")));
	SetBuffers(parts[0], pnt);
	//SetPartsBuffer();
	ply_pnt = pnt;
	width = objInst->prop_map->at("default")->texture_data->width;
	height = objInst->prop_map->at("default")->texture_data->height;

	vx_c = 180.0;
	vy_c = 180.0;
	vx = 0;
	vy = 0;
	vy_cap = 10;
	jump_triggered = false;
	//width = player;
	//height = player_texture->height;
	//SDL_QueryTexture(this->player_texture->GetRegTex(),NULL,NULL,&width,&height);
	FPoint b2_pnt = Globals::GetInstance().ToMeters(pnt.x, pnt.y, width, height);
	b2BodyDef body_def;
	body_def.type = b2_dynamicBody;
	body_def.position.Set(b2_pnt.x, b2_pnt.y);
	b2d_body = tile_world->CreateBody(&body_def);
	b2PolygonShape box_shape;
	b2FixtureDef box_fixture;
	box_fixture.shape = &box_shape;
	box_fixture.density = 1.0;
	box_shape.SetAsBox( (float)(width*0.1)/2, (float)(height*0.1)/2);
	b2d_body->CreateFixture(&box_fixture);
	//body->SetFixedRotation(false);
}

void Player::SetPartsBuffer()
{
	ply_pnt = pnt;

	float head_w = parts.at(0)->texture_data->width;
	float head_h = parts.at(0)->texture_data->height;
	float body_w = parts.at(1)->texture_data->width;
	float body_h = parts.at(1)->texture_data->height;
	float arm_w = parts.at(2)->texture_data->width;
	float leg_w = parts.at(4)->texture_data->width;
	float leg_h = parts.at(4)->texture_data->height;

	for (int i = 0; i < parts.size(); i++) {
		Property* cur_part = parts.at(i);
		switch (i) {
		//Head
		case 0:
			SetBuffers(cur_part, CPoint( (pnt.x + (body_w/2)) - (head_w/2), pnt.y));
			break;
		//Body
		case 1:
			body_w = cur_part->texture_data->width;
			body_h = cur_part->texture_data->height;
			SetBuffers(cur_part, CPoint(pnt.x, pnt.y + head_w));
			break;
		//Left Arm
		case 2:
			ply_pnt.x = pnt.x - arm_w;
			SetBuffers(cur_part, CPoint(pnt.x - arm_w, pnt.y + head_w));
			break;
		//Right Arm
		case 3:
			SetBuffers(cur_part, CPoint(pnt.x + body_w, pnt.y + head_w));
			break;
		//Left Leg
		case 4:
			SetBuffers(cur_part, CPoint(pnt.x, pnt.y + body_h + head_w));
			break;
		//Right Leg
		case 5:
			SetBuffers(cur_part, CPoint(pnt.x + body_w - leg_w, pnt.y + body_h + head_w));
			break;
		}
	}
	width = arm_w + body_w + arm_w;
	height = head_h + body_h + leg_h;
	FPoint b2_pnt = Globals::GetInstance().ToMeters(ply_pnt.x, ply_pnt.y, width, height);
}

void Player::SetBuffers(Property* cur_prop, CPoint pnt)
{
	std::vector<GLfloat> vertex_data_vector = std::vector<GLfloat>();
	std::vector<GLuint> index_data_vector = std::vector<GLuint>();
	std::vector<GLfloat> clip_data_vector = std::vector<GLfloat>();

	CPoint cur_size = CPoint(cur_prop->texture_data->width, cur_prop->texture_data->height);

	int resx = Globals::GetInstance().res_width;
	int resy = Globals::GetInstance().res_height;

	float temp_y = resy - pnt.y - cur_size.y;

	float r_x = (float)pnt.x / resx;
	float r_y = (float)temp_y / resy;
	float rw_r = (float)cur_size.x / resx;
	float rh_r = (float)cur_size.y / resy;

	vertex_data_vector.push_back(r_x);
	vertex_data_vector.push_back(r_y);
	clip_data_vector.push_back(0.0f);
	clip_data_vector.push_back(0.0f);

	vertex_data_vector.push_back(r_x + rw_r);
	vertex_data_vector.push_back(r_y);
	clip_data_vector.push_back(1.0f);
	clip_data_vector.push_back(0.0f);

	vertex_data_vector.push_back(r_x + rw_r);
	vertex_data_vector.push_back(r_y + rh_r);
	clip_data_vector.push_back(1.0f);
	clip_data_vector.push_back(1.0f);

	vertex_data_vector.push_back(r_x);
	vertex_data_vector.push_back(r_y + rh_r);
	clip_data_vector.push_back(0.0f);
	clip_data_vector.push_back(1.0f);

	index_data_vector.push_back(0);
	index_data_vector.push_back(1);
	index_data_vector.push_back(2);
	index_data_vector.push_back(3);

	GLuint* index_data = &index_data_vector[0];
	GLfloat* vertex_data = &vertex_data_vector[0];
	GLfloat* clip_data = &clip_data_vector[0];

	int index_data_size = sizeof(GLuint)*index_data_vector.size();
	int vertex_data_size = sizeof(GLfloat)*vertex_data_vector.size();
	cur_prop->texture_data->SetupPolyShader();
	cur_prop->texture_data->polyShader->LoadArrayData(index_data, vertex_data, clip_data, index_data_vector.size(), vertex_data_vector.size(), clip_data_vector.size());
}


void Player::ReloadBuffers(Property* cur_prop, CPoint pnt)
{
	std::vector<GLfloat> vertex_data_vector = std::vector<GLfloat>();

	CPoint cur_size = CPoint(cur_prop->texture_data->width, cur_prop->texture_data->height);

	int resx = Globals::GetInstance().res_width;
	int resy = Globals::GetInstance().res_height;

	float temp_y = resy - pnt.y - cur_size.y;

	float r_x = (float)pnt.x / resx;
	float r_y = (float)temp_y / resy;
	float rw_r = (float)cur_size.x / resx;
	float rh_r = (float)cur_size.y / resy;

	vertex_data_vector.push_back(r_x);
	vertex_data_vector.push_back(r_y);

	vertex_data_vector.push_back(r_x + rw_r);
	vertex_data_vector.push_back(r_y);

	vertex_data_vector.push_back(r_x + rw_r);
	vertex_data_vector.push_back(r_y + rh_r);

	vertex_data_vector.push_back(r_x);
	vertex_data_vector.push_back(r_y + rh_r);

	GLfloat* vertex_data = &vertex_data_vector[0];

	int vertex_data_size = sizeof(GLfloat)*vertex_data_vector.size();

	glBindBuffer(GL_ARRAY_BUFFER, cur_prop->texture_data->polyShader->VBO[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*vertex_data_size, vertex_data, GL_STATIC_DRAW);
	glVertexAttribPointer(cur_prop->texture_data->polyShader->vertexPosLocation, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(cur_prop->texture_data->polyShader->vertexPosLocation);
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(cur_prop->texture_data->polyShader->vertexPosLocation);
}

CPoint Player::Traverse(int vx, int vy) 
{
	pnt.x += vx;
	pnt.y += vy;
	return CPoint(vx,vy);
}

void Player::SetForce(int force)
{
}

void Player::SetVelocity(int vx, int vy)
{

}

FPoint Player::GetPosition()
{
	return FPoint(b2d_body->GetPosition().x, b2d_body->GetPosition().y);
}

void Player::ResetVelocity()
{
	vx = 0;
	vy = 0;
}

void Player::Traverse(int dir)
{
	switch(dir) {
	case 1:
		vy = vy_c*Globals::GetInstance().sec;
		break;
	case 2:
		vx = vx_c*Globals::GetInstance().sec;
		break;
	case 3:
		vy = vy_c * -1 * Globals::GetInstance().sec;
		break;
	case 4:
		vx = vx_c * -1 * Globals::GetInstance().sec;
		break;
	}
}

void Player::UpdateMovement()
{
	b2Vec2 vel = b2d_body->GetLinearVelocity();
	vel.x = vx*0.1f;
	vel.y = vy*0.1f;
	b2d_body->SetTransform(b2Vec2(GetPosition().x + vel.x, GetPosition().y + vel.y),0.0f);
}

void Player::RenderPlayer(FPoint camera_pnt)
{
	CPoint ply_pnt = Globals::GetInstance().ToPixels(b2d_body->GetPosition().x, b2d_body->GetPosition().y,width,height);
	//ply_pnt.y = Globals::GetInstance().screen_height - ply_pnt.y - height;
	pnt = ply_pnt;



	//for (int i = 0; i < parts.size; i++) {
	//	Property* cur_part = parts.at(i);
	//}
	for (int i = 0; i < parts.size(); i++) {
		Property* cur_part = parts.at(i);
		glBindTexture(GL_TEXTURE_2D, cur_part->texture_data->GetTexture());
		cur_part->texture_data->polyShader->BindShader();
		cur_part->texture_data->polyShader->SetAngle(0.0f);
		cur_part->texture_data->polyShader->SetTransVector(camera_pnt.x, camera_pnt.y*-1);
		ReloadBuffers(cur_part, pnt);
		cur_part->texture_data->polyShader->RenderStatic();
		glBindTexture(GL_TEXTURE_2D, 0);
		cur_part->texture_data->polyShader->UnbindShader();
	}

}
  