#ifndef HOBJECT_H
#define HOBJECT_H

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <boost\lexical_cast.hpp>
#include <Box2D/Box2D.h>
#include <SDL.h>
#include <SDL_image.h>
#include <map>
#include <string>
#include <vector>
#include <SDL_mixer.h>
#include "texture_group.h"
#include "point.h"
#include "image.h"

class Property {
public:
	Property();
	Property(const Property &prop);
	~Property();
	Property(std::string,std::string,int,unsigned char*);
	Property(std::string,std::string,int,std::string,int,unsigned char*);
	TextureGroup* ToTexture();
	std::string ToString();
	Mix_Chunk* ToAudio();
	int ToInteger();
	std::string string_data;
	TextureGroup* texture_data;
	Mix_Chunk* audio_data;
	int integer_data;
	std::string name;
	std::string data_type;
	std::string asset_name;
	int use_asset;
	int size;
private:
	void ClassifyData();
	void* CreateMonoCopy(int,int);
	unsigned char* value;
};

class ImageStates {  
public:
	ImageStates();
	~ImageStates();
	void AddState(std::string, TextureGroup*);
	Image* GetImage(std::string);
private:
	std::map<std::string,Image*>* state_map;
};

class Asset : public Property {
public:
	 Asset(std::string, std::string, int, unsigned char*);
};

class ObjectInstance {
public:
	ObjectInstance(std::string, std::map<std::string, Property*>*, CPoint, int );
	~ObjectInstance();
	std::string GetStringValue(std::string);
	float GetFloatValue(std::string);
	TextureGroup* GetTextureValue(std::string prop_name);
	Mix_Chunk* GetAudioValue(std::string);
	int GetIntValue(std::string);
	b2Body* body;
	void RenderObject(FPoint,Image*,int,int);
	std::map<std::string, Property*>* prop_map;
	std::string name;
	CPoint pnt;
	int visible;
	std::string state;
	std::string image_state;
	Image* CurrentImage;
};

class ObjectTemplate {
public:
	ObjectTemplate(std::string, std::map<std::string,Property>*, ImageStates);
	~ObjectTemplate();
	Image* GetImageFromInstance(ObjectInstance*);
private:
	ImageStates image_states;
	std::map<std::string,Property>* prop_map;
	std::string name;
};

class ObjectFactory {
public:
	ObjectFactory();
	~ObjectFactory();
	ObjectInstance* GetInstanceAtIndex(int);
	ObjectInstance* GetObjectInstanceByName(std::string name);
	Image* GetImageFromInstance(ObjectInstance*);
	ObjectTemplate GetObjectTemplateFromName(std::string);
	void RenderObjects(FPoint,int,int);
	void AddObject(std::string, std::map<std::string, Property*>*, CPoint, int);
	void AddTemplate(std::string,std::map<std::string, Property>*,ImageStates);
	void AddAssetMap(std::string,std::string,int,unsigned char*);
	int GetObjectTemplateSize();
	int GetObjectInstanceSize();
	ObjectInstance* player_instance;
	ObjectInstance* globals_instance;
private:
	std::vector<ObjectInstance*>* obj_instance_list;
	std::map<std::string, ObjectTemplate>* object_templates;
	std::map<std::string, Asset>* asset_map;
};

#endif
