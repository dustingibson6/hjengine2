#ifndef IMAGE_HEAD_H
#define IMAGE_HEAD_H

#include "ui.h"
#include "gltext.h"
#include "surface_shader.h"
#include "color.h"
#include "object.h"

class ImageHead : public UI {
public:
	ImageHead();
	ImageHead(CPoint pnt, std::map<std::string, Definable> definable, ObjectFactory* object_factory);
	void LoadVertexData();
	void RenderUI();
	void Sizing();
private:
	PolyShader* poly_shader;
	TextureGroup* current_texture;
};

#endif