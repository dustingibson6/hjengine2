#include "texture_group.h"

TextureGroup::TextureGroup(GLuint tex)
{
	this->texture = texture;
}

TextureGroup::TextureGroup(unsigned char* buffer,int size)
{ 
	this->texture = CreateTextureFromBytes(buffer,size);
}

void TextureGroup::SetupPolyShader()
{
	polyShader = new PolyShader("texture");
}

TextureGroup::TextureGroup()
{

}

TextureGroup::~TextureGroup()
{
}

GLuint TextureGroup::GetTexture() 
{
	return this->texture;
}

GLuint TextureGroup::CreateTextureFromBytes(unsigned char* buffer,int size)
{
	ILuint img_id = 0;
	GLuint bytes;
	GLuint* pixels;
	ilGenImages(1,&img_id);
	ilBindImage(img_id);
	ilLoadL(IL_PNG,buffer,size);
	ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
	pixels = (GLuint*)ilGetData();
	width = (GLuint)ilGetInteger(IL_IMAGE_WIDTH);
	height = (GLuint)ilGetInteger(IL_IMAGE_HEIGHT);
	glGenTextures(1,&bytes);
	glBindTexture(GL_TEXTURE_2D,bytes);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,width,height,0,GL_RGBA,GL_UNSIGNED_BYTE,pixels);
	glBindTexture(GL_TEXTURE_2D,NULL);
	return bytes;
}