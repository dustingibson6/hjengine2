#include "menu.h"
#include "select_box.h"
#include "color.h"
#include "definable.h"
#include "tile_map.h"
#include "object.h"

MenuFactory::MenuFactory()
{
	menu_map = new std::map<std::string,Menu*>();
	effect_factory = new EffectFactory();
}

unsigned char MenuFactory::CharToInt8(std::string s)
{
	int to_int = atoi(s.c_str());
	return (unsigned char)to_int;
}

void MenuFactory::AddFromXML(std::string name)
{
	effect_factory->AddStarFieldEffect("star field", 2000);
	rapidxml::xml_document<> doc;
	rapidxml::xml_node<>* root_node;
	std::ifstream xml_file("res/menu/" + name + ".xml");
	std::vector<char> buffer((std::istreambuf_iterator<char>(xml_file)),std::istreambuf_iterator<char>());
	buffer.push_back('\0');
	doc.parse<0>(&buffer[0]);
	root_node = doc.first_node("menusystem");
	std::string config_name = root_node->first_attribute("config")->value();
	std::string res_name = root_node->first_attribute("resource")->value();
	TileMap* temp_map = new TileMap("res/maps/" + res_name + ".hjm");
	ObjectFactory* object_factory = temp_map->object_factory;
	config = Config(config_name);
	for(rapidxml::xml_node<>* menu_node = root_node->first_node("menu"); menu_node; menu_node = menu_node->next_sibling()) {
		std::string menu_name = menu_node->first_attribute("name")->value();
		std::string bg_color_string = menu_node->first_attribute("bg_color")->value();
		std::string font_color_string = menu_node->first_attribute("font_color")->value();
		std::string border_color_string = menu_node->first_attribute("border_color")->value();
		Definable font_name = Definable(menu_node->first_attribute("font")->value());
		Definable font_size = Definable(menu_node->first_attribute("font_size")->value());
		Definable brightness = Definable(menu_node->first_attribute("brightness")->value());
		Definable bg_color = Definable(Color(bg_color_string));
		Definable font_color = Definable(Color(font_color_string));
		Definable border_color = Definable(Color(border_color_string));
		std::map<std::string, Definable> global_definables = std::map<std::string, Definable>();
		//START GLOBAL PROPERTIES
		font_name.AddToDefinables("font", global_definables);
		font_size.AddToDefinables("font size", global_definables);
		brightness.AddToDefinables("hover constant", global_definables);
		bg_color.AddToDefinables("background color", global_definables);
		font_color.AddToDefinables("font color", global_definables);
		border_color.AddToDefinables("border color", global_definables);
		//END GLOBAL PROPERTIES
		AddMenu(menu_name);
		int cnt = 0;
		for(rapidxml::xml_node<>* item_node = menu_node->first_node("item"); item_node; item_node = item_node->next_sibling()) 
		{
			std::string menu_item_type = item_node->first_attribute("type")->value();
			Definable menu_item_text = Definable(item_node->first_attribute("text")->value());
			std::string menu_item_name = item_node->first_attribute("name")->value();
			std::string menu_item_x_string = item_node->first_attribute("x")->value();
			std::string menu_item_y_string = item_node->first_attribute("y")->value();
			int menu_item_w = atof(item_node->first_attribute("w")->value())*Globals::GetInstance().screen_width;
			int menu_item_h = atof(item_node->first_attribute("h")->value())*Globals::GetInstance().screen_height;
			int menu_item_x = 0;
			int menu_item_y = 0;
			CPoint text_size = Globals::GetInstance().GetTextDim(menu_item_text.ToString(), font_name.ToString(), font_size.ToInt());
			if (menu_item_x_string == "center") {
				int rel_width = text_size.x / 2;
				menu_item_x = (Globals::GetInstance().screen_width / 2) - rel_width;
			}
			else
				menu_item_x = atof(menu_item_x_string.c_str())*Globals::GetInstance().screen_width;
			menu_item_y = atof(menu_item_y_string.c_str())*Globals::GetInstance().screen_height;
			CPoint menu_item_pnt = CPoint(menu_item_x, menu_item_y);
			CPoint menu_item_size = CPoint(menu_item_w, menu_item_h);
			if(menu_item_type == "button") {
				Definable border_size = Definable(item_node->first_attribute("border_size")->value());
				Definable link = Definable(item_node->first_attribute("link")->value());
				Definable action = Definable(item_node->first_attribute("action")->value());
				Definable size = Definable(menu_item_size);
				std::map<std::string, Definable> definables = std::map<std::string, Definable>();
				
				//BUTTON PROPERTIES START
				definables.insert(global_definables.begin(), global_definables.end());
				border_size.AddToDefinables("border size", definables);
				link.AddToDefinables("link", definables);
				action.AddToDefinables("action", definables);
				size.AddToDefinables("size", definables);
				menu_item_text.AddToDefinables("text", definables);
				//BUTTON PROPERTIES END	
				menu_map->at(menu_name)->AddButton(menu_item_name, menu_item_pnt, definables, object_factory );
			}
			if (menu_item_type == "toggle box") {
				Definable bind = Definable(item_node->first_attribute("bind")->value());
				Definable size = Definable(menu_item_size);
				bool checked = false;
				if (config.GetValue(bind.ToString()) == "true")
					checked = true;
				Definable new_size = Definable(CPoint(text_size.x + (text_size.y * 2), text_size.y));
				Definable border_size = atoi(item_node->first_attribute("border_size")->value());
				std::map<std::string, Definable> definables = std::map<std::string, Definable>();
				
				//TOGGLE BOX PROPERTIES START
				definables.insert(global_definables.begin(), global_definables.end());
				bind.AddToDefinables("bind",definables);
				new_size.AddToDefinables("size", definables);
				border_size.AddToDefinables("border size", definables);
				menu_item_text.AddToDefinables("text", definables);
				size.AddToDefinables("text size", definables);
				//TOGGLE BOX PROPERTIES END
				
				menu_map->at(menu_name)->AddToggleBox(menu_item_name, menu_item_pnt, definables, object_factory, checked);
				//menu_map->at(menu_name)->AddToggleBox(menu_item_name, menu_item_pnt, new_size, menu_item_text, font_name, font_size, font_sdl_color, bg_sdl_color, border_sdl_color, border_size, brightness, checked, definables);
			}
			if (menu_item_type == "select box") {
				std::vector<SelectChoice> choices =  std::vector<SelectChoice>();
				std::string bind_value = config.GetValue(item_node->first_attribute("bind")->value());
				Definable bind = item_node->first_attribute("bind")->value();
				Definable size = Definable(menu_item_size);
				for (rapidxml::xml_node<>* choice_node = item_node->first_node("choice"); choice_node; choice_node = choice_node->next_sibling()) {
					std::string text = choice_node->first_attribute("text")->value();
					std::string value = choice_node->first_attribute("value")->value();
					bool selected = false;
					if (bind_value == value)
						selected = true;
					choices.push_back(SelectChoice(text, value, selected));
				}
				Definable label_font_size = Definable(item_node->first_attribute("label_font_size")->value());
				Definable border_size = Definable(item_node->first_attribute("border_size")->value());
				std::map<std::string, Definable> definables = std::map<std::string, Definable>();
				//SELECTION BOX PROPERTIES START
				definables.insert(global_definables.begin(), global_definables.end());
				bind.AddToDefinables("bind", definables);
				label_font_size.AddToDefinables("label font size", definables);
				border_size.AddToDefinables("border size", definables);
				menu_item_text.AddToDefinables("text", definables);
				size.AddToDefinables("size", definables);
				//SELECTION BOX PROPERTIES END
				menu_map->at(menu_name)->AddSelectBox(menu_item_name, menu_item_pnt, definables, object_factory, choices);
				//menu_map->at(menu_name)->AddSelectBox(menu_item_name, menu_item_pnt, menu_item_size, menu_item_text, font_name, label_font_size, font_color, bg_color, border_color, border_size, brightness , choices);
				//menu_map->at(menu_name)->AddSelectBox(menu_item_name, menu_item_pnt, menu_item_size, menu_item_text, font_name, font_size, font_sdl_color, bg_sdl_color, border_sdl_color, border_size, brightness, label_font_size, choices, definables);
			}
			if (menu_item_type == "image") {
				Definable size = Definable(menu_item_size);
				Definable image_name = Definable(item_node->first_attribute("image_name")->value());
				TextureGroup* temp_texture = object_factory->GetObjectInstanceByName("menu")->GetTextureValue(image_name.ToString());
				if (menu_item_x_string == "center") {
					int rel_width = menu_item_size.x / 2;
					menu_item_x = (Globals::GetInstance().screen_width / 2) - rel_width;
				}
				CPoint menu_item_pnt = CPoint(menu_item_x, menu_item_y);
				std::map<std::string, Definable> definables = std::map<std::string, Definable>();
				size.AddToDefinables("size", definables);
				image_name.AddToDefinables("image name", definables);
				menu_map->at(menu_name)->AddImage(menu_item_name, menu_item_pnt, definables, object_factory);
			}
			if (menu_item_type == "pane") {
				Definable size = Definable(menu_item_size);
				Definable pane_color = Definable(item_node->first_attribute("pane_color")->value());
				Definable border_size = Definable(item_node->first_attribute("border_size")->value());
				std::map<std::string, Definable> definables = std::map<std::string, Definable>();
				definables.insert(global_definables.begin(), global_definables.end());
				size.AddToDefinables("size", definables);
				border_size.AddToDefinables("border_size", definables);
				pane_color.AddToDefinables("pane_color", definables);
				menu_map->at(menu_name)->AddPane(menu_item_name, menu_item_pnt, definables, object_factory);
			}
			if (menu_item_type == "label") {
				Definable label_font_size = item_node->first_attribute("font_size")->value();
				Definable size = Definable(menu_item_size);
				std::map<std::string, Definable> definables = std::map<std::string, Definable>();
				definables.insert(global_definables.begin(), global_definables.end());
				size.AddToDefinables("size", definables);
				menu_item_text.AddToDefinables("text", definables);
				label_font_size.AddToDefinables("label font size", definables);
				menu_map->at(menu_name)->AddLabel(menu_item_name, menu_item_pnt, definables, object_factory);
			}
			if (menu_item_type == "value box") {
				std::string bind_value = config.GetValue(item_node->first_attribute("bind")->value());
				Definable bind = item_node->first_attribute("bind")->value();
				Definable size = Definable(menu_item_size);
				Definable label_font_size = Definable(item_node->first_attribute("label_font_size")->value());
				Definable border_size = Definable(item_node->first_attribute("border_size")->value());
				Definable max_value = Definable(item_node->first_attribute("max")->value());
				Definable cur_value = Definable(config.GetValue(bind.ToString()));
				std::map<std::string, Definable> definables = std::map<std::string, Definable>();
				//VALUE BOX PROPERTIES START
				definables.insert(global_definables.begin(), global_definables.end());
				bind.AddToDefinables("bind", definables);
				label_font_size.AddToDefinables("label font size", definables);
				border_size.AddToDefinables("border size", definables);
				menu_item_text.AddToDefinables("text", definables);
				size.AddToDefinables("size", definables);
				max_value.AddToDefinables("max value", definables);
				cur_value.AddToDefinables("current value", definables);
				//VALUE BOX PROPERTIES END
				menu_map->at(menu_name)->AddValueBox(menu_item_name, menu_item_pnt, definables, object_factory);
			}
			cnt++;
		}
	}
}

void Menu::ResizeUIs()
{
	return ui_factory.ResizeUIs();
}

void MenuFactory::DrawMenu()
{
	if (Globals::GetInstance().reinit_flag) {
		std::map<std::string, Menu*>::iterator it;
		for (it = menu_map->begin(); it != menu_map->end(); ++it)
			it->second->ResizeUIs();
		effect_factory->ResizeEffects();
	}
	effect_factory->RunEffects();
	menu_map->at(active_menu_name)->DrawMenu();
}

void MenuFactory::SetMenu(std::string name)
{
	active_menu_name = name;
}

void MenuFactory::AddMenu(std::string name)
{
	menu_map->insert(std::pair<std::string,Menu*>(name, new Menu(name)));
}

Menu::Menu(std::string name)
{
	this->name = name;
	this->ui_factory = UIFactory();
	active_index = -1;
	menu_item_list = new std::vector<MenuItem*>();
}

MenuItem* Menu::GetMenuItemByIndex(int index)
{
	return menu_item_list->at(index);
}

int Menu::MenuItemSize()
{
	return menu_item_list->size();
}

void Menu::ChangeIndex(int index)
{
	active_index = index;
}

void MenuFactory::ChangeActiveMenu(std::string name)
{
	active_menu_name = name;
	LoadValues();
}

std::vector<UI*> Menu::PollEvents()
{
	std::vector<UI*> all_events = std::vector<UI*>();
	std::vector<UI*> all_clicked = ui_factory.GetClicked();
	for (int i = 0; i < all_clicked.size(); i++) {
		if (all_clicked.at(i)->type == "button") {
			all_events.push_back(all_clicked.at(i));
			all_events.push_back(all_clicked.at(i));
		}
	}
	return all_events;
}

void MenuFactory::SaveConfigValues()
{
	Menu* active_menu = GetActiveMenu();
	std::vector<UI*> ui_list = active_menu->GetUIList();
	for (int i = 0; i < ui_list.size(); i++) {
		//Get config binding
		if (ui_list.at(i)->type == "toggle box") {
			std::string bind = ui_list.at(i)->GetDefinable("bind").ToString();
			if (((ToggleBox*)ui_list.at(i))->checked)
				config.SetValue(bind, "true");
			else
				config.SetValue(bind,"false");
		}
		if (ui_list.at(i)->type == "select box") {
			std::string bind = ui_list.at(i)->GetDefinable("bind").ToString();
			std::string value = ((SelectBox*)ui_list.at(i))->GetValueString();
			config.SetValue(bind, value);
		}
		if (ui_list.at(i)->type == "value box") {
			std::string bind = ui_list.at(i)->GetDefinable("bind").ToString();
			std::string value = ((ValueBox*)ui_list.at(i))->GetValueString();
			config.SetValue(bind, value);
		}
	}
	config.SaveConfig();
}

void MenuFactory::LoadValues()
{
	Menu* active_menu = GetActiveMenu();
	std::vector<UI*> ui_list = active_menu->GetUIList();
	for (int i = 0; i < ui_list.size(); i++) {
		//Get config binding
		if (ui_list.at(i)->type == "toggle box") {
			std::string bind = ui_list.at(i)->GetDefinable("bind").ToString();
			bool value = config.GetBoolValue(bind);
			((ToggleBox*)ui_list.at(i))->SetValue(value);
		}
		if (ui_list.at(i)->type == "select box") {
			std::string bind = ui_list.at(i)->GetDefinable("bind").ToString();
			std::string value = config.GetValue(bind);
			((SelectBox*)ui_list.at(i))->SetValue(value);
		}
		if (ui_list.at(i)->type == "value box") {
			std::string bind = ui_list.at(i)->GetDefinable("bind").ToString();
			int value = config.GetValueInt(bind);
			((ValueBox*)ui_list.at(i))->SetValue(value);
		}
	}
}

std::vector<UI*> Menu::GetUIList()
{
	return ui_factory.GetUIList();
}

std::vector<std::string> MenuFactory::PollMenuEvents()
{
	std::vector<std::string> results = std::vector<std::string>();
	Menu* active_menu = GetActiveMenu();
	std::vector<UI*> all_events = active_menu->PollEvents();
	for (int i = 0; i < all_events.size(); i++) {
		if (all_events.at(i)->events.click) {
			if (all_events.at(i)->type == "button") {
				std::string link = all_events.at(i)->GetDefinable("link").ToString();
				std::string action = all_events.at(i)->GetDefinable("action").ToString();
				results.push_back(action);
				if (action == "save") {
					SaveConfigValues();
					if( link != "")
						ChangeActiveMenu(link);
				}
				if (link != "none" && action == "change menu")
					ChangeActiveMenu(link);
				if (link != "none" && action == "change state")
					results.push_back(link);
				return results;
			}
			//If link is save then let's save value as config
		}
	}
	results.push_back("");
	return results;
}

void MenuFactory::PollActiveMenuItem(int mx, int my, bool mouse_click)
{
	Menu* active_menu = GetActiveMenu();
	for(int i=0; i < active_menu->MenuItemSize(); i++) {
		int x = active_menu->GetMenuItemByIndex(i)->x;
		int y = active_menu->GetMenuItemByIndex(i)->y;
		int w = active_menu->GetMenuItemByIndex(i)->inactive_image->GetWidth();
		int h = active_menu->GetMenuItemByIndex(i)->inactive_image->GetHeight();
		if( (mx >= x && mx <= x + w) && (my >= y && my <= y + h) ) {
			MenuItem* current_item = active_menu->GetMenuItemByIndex(i);
			active_menu->ChangeIndex(i);
			if( mouse_click )
				ChangeActiveMenu(static_cast<ButtonItem*>(current_item)->GetLink());
			return;
		}
	}
}

void Menu::AddButton(std::string name, CPoint pnt, std::map<std::string, Definable> definable, ObjectFactory* object_factory)
{
	this->ui_factory.AddButton( name, pnt, definable, object_factory);
}

void Menu::AddToggleBox(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory, bool checked)
{
	this->ui_factory.AddToggleBox(name, pnt, definables, object_factory, checked);
}

void Menu::AddSelectBox(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory, std::vector<SelectChoice> choices)
{
	this->ui_factory.AddSelectBox(name, pnt, definables, object_factory, choices);
}

void Menu::AddImage(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory)
{
	this->ui_factory.AddImage(name, pnt, definables, object_factory);
}

void Menu::AddPane(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory)
{
	this->ui_factory.AddPane(name, pnt, definables, object_factory);
}

void Menu::AddLabel(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory)
{
	this->ui_factory.AddLabel(name, pnt, definables, object_factory);
}

void Menu::AddValueBox(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory)
{
	this->ui_factory.AddValueBox(name, pnt, definables, object_factory);
}

void Menu::DrawMenu()
{
	ui_factory.RenderUIs();
}

void Menu::AddButtonItem(std::string text, std::string link, int x, int y)
{
	ButtonItem* temp_item = new ButtonItem("button",x,y,text,font,text_color,border_color,link);
	menu_item_list->push_back(temp_item);
}

void ButtonItem::DrawMenu(SDL_Renderer* renderer,int mode)
{
	if( mode == 0) {
		//SDL_Rect text_rect = {x,y,inactive_image->GetWidth(),inactive_image->GetHeight()};
		//SDL_RenderCopy(renderer,inactive_image->GetTextureGroup()->GetTexture(),NULL,&text_rect);
	}
	else {
		//SDL_Rect text_rect = {x,y,hover_image->GetWidth(),hover_image->GetHeight()};
		//SDL_RenderCopy(renderer,hover_image->GetTextureGroup()->GetTexture(),NULL,&text_rect);
	}

}

Menu* MenuFactory::GetMenu(std::string name)
{
	return menu_map->at(name);
}

Menu* MenuFactory::GetActiveMenu()
{
	return GetMenu(active_menu_name);
}

void MenuItem::DrawMenu(SDL_Renderer* renderer,int mode)
{

}

MenuItem::MenuItem(std::string type, int x, int y, TTF_Font* font, SDL_Color text_color, SDL_Color border_color)
{
	this->type = type;
	this->x = x;
	this->y = y;
	this->font = font;
	this->text_color = text_color;
	this->border_color = border_color;
}

std::string ButtonItem::GetLink()
{
	return link;
}

ButtonItem::ButtonItem(std::string type, int x, int y, std::string text, TTF_Font* font, SDL_Color text_color, SDL_Color border_color, std::string link)
	: MenuItem(type,x,y,font,text_color,border_color)
{
	this->link = link;
	this->text = text;
	inactive_surface = TTF_RenderText_Solid(font, text.c_str(), text_color);
	hover_surface = TTF_RenderText_Solid(font, text.c_str(), border_color);
	//SDL_Texture* inactive_texture = SDL_CreateTextureFromSurface(renderer, inactive_surface);
	//SDL_Texture* hover_texture = SDL_CreateTextureFromSurface(renderer, hover_surface);
	//inactive_image = new Image(new TextureGroup(inactive_texture));
	//hover_image = new Image(new TextureGroup(hover_texture));
}
