#ifndef SHADER_H
#define SHADER_H

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <GL/glew.h>
#include <glm.hpp>
#include <transform.hpp>
#include <type_ptr.hpp>


class Shader {
public:
	Shader(std::string fname);
	~Shader();
	GLuint LoadShaderFromFile(std::string,GLenum);
	void BindShader();
	void UnbindShader();
	GLuint programID;
	GLuint modMatrixLocation;
	GLuint proMatrixLocation;
	void SetProMatrix(glm::mat4);
	void SetModMatrix(glm::mat4);

protected:
	GLuint frag_shader;
	GLuint vertex_shader;
};

#endif