#ifndef PANE_H
#define PANE_H

#include "ui.h"
#include "gltext.h"
#include "surface_shader.h"
#include "definable.h"
#include "color.h"
#include "object.h"

class Pane : public UI
{
public:
	Pane();
	Pane(CPoint pnt, std::map<std::string, Definable>, ObjectFactory* object_factory);
	void PollInput();
	void Sizing();
	void RenderUI();
private:
	SurfaceShader* surface_shader;
	Color border_color;
	Color bg_color;
	CPoint size;
	int border_size;
};

#endif