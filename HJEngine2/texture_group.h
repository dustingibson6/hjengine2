#ifndef TEXTUREGROUP_H
#define TEXTUREGROUP_H

#include <stdio.h>
#include <SDL.h>
#include <SDL_image.h>
#include <GL/glew.h>
#include <IL/il.h>
#include <iostream>
#include <map>
#include "point.h"
#include "poly_shader.h"

class TextureGroup {
	public:
		TextureGroup(GLuint);
		TextureGroup(unsigned char*,int);
		TextureGroup();
		~TextureGroup();
		GLuint GetTexture();
		GLuint texture_id; 
		GLuint width;
		GLuint height;
		PolyShader* polyShader;
		void SetupPolyShader();
		GLfloat vertexData[32];
		GLuint VBO;
		GLuint VAO;
		GLuint IBO;
	private:
		GLuint CreateTextureFromBytes(unsigned char*,int);
		GLuint texture;
};

#endif