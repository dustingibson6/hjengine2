#include "tile_sheet.h"

TileSet::TileSet(int tile_width, int tile_height, int row, int col)
{
	this->tile_width = tile_width;
	this->tile_height = tile_height;
	this->row = row;
	this->col = col;
	//tile_images = new std::map<CPoint,TextureGroup*>();
}

TileSet::~TileSet()
{
	delete tile_images;
}

void TileSet::AddTile(TextureGroup* tile_image, CPoint pnt)
{
	//return NULL;
	tile_images->insert(std::pair<CPoint,TextureGroup*>(pnt,tile_image));
	//std::cout << CountTileImages(pnt) << std::endl;
	//std::cout << tile_images->size() << std::endl;
}

int TileSet::CountTileImages(CPoint pnt)
{
	int cnt = 0;
	for( std::map<CPoint,TextureGroup*>::iterator it=tile_images->begin(); it != tile_images->end(); ++it)
		if(CPoint(it->first.x,it->first.y) == pnt)
			cnt++;
	return cnt;
}

TextureGroup* TileSet::GetBitmap(CPoint pnt)
{
	if(pnt.x < 0 || pnt.y < 0)
		return NULL;
	//if(CountTileImages(pnt) <= 0)
		//return NULL;
	return tile_images->at(pnt);
}