#include "effects.h"
#include "globals.h"

EffectFactory::EffectFactory()
{
	effect_map = new std::map<std::string,Effect*>();
}

void EffectFactory::AddFadeEffect(std::string name,int w, int h, int speed)
{
	Fade* fade = new Fade(name,w,h,speed);
	effect_map->insert(std::pair<std::string,Effect*>(name, fade) );
}

void EffectFactory::AddGrayScaleEffect(std::string name, int x, int y, int w, int h)
{
	GrayScale* gray_scale = new GrayScale(name,x,y,w,h);
	effect_map->insert(std::pair<std::string,Effect*>(name,gray_scale));
}

void EffectFactory::AddStarFieldEffect(std::string name, int star_num)
{
	StarField* starfield = new StarField(name,star_num);
	effect_map->insert(std::pair<std::string, Effect*>(name, starfield));
}

void EffectFactory::RemoveEffect(std::string name)
{
	effect_map->erase(name);
}

void EffectFactory::ResizeEffects()
{
	std::map<std::string, Effect*>::iterator it = effect_map->begin();
	while (it != effect_map->end()) {
		it->second->Sizing();
		it++;
	}
}

void EffectFactory::RunEffects()
{
	std::map<std::string,Effect*>::iterator it = effect_map->begin();
	while(it != effect_map->end()) {
		if(it->second->flag == "run")
			it->second->RunEffect();
		if(it->second->flag == "destroy")
			effect_map->erase(it++);
		it++;
	}
	//for (std::map<std::string,Effect*>::iterator it=effect_map->begin(); it!=effect_map->end(); ++it) {
	//	if(it->second->flag == "run")
	//		it->second->RunEffect(cur_render);
	//	if(it->second->flag == "destroy")
	//		RemoveEffect(it->first);
	//}
}

EffectFactory::~EffectFactory()
{
	delete effect_map;
}

void Effect::RunEffect()
{
}

void Effect::Sizing()
{

}

void Fade::RunEffect()
{
	if(alpha <= 0) {
		flag = "destroy queue";
		return;
	}
	//glClear( GL_COLOR_BUFFER_BIT );
	shader->BindShader();
	glUniform1f( speed_location, alpha );
	//glBindVertexArray( VAO );
	glEnableClientState( GL_VERTEX_ARRAY );
	glBindBuffer( GL_ARRAY_BUFFER, VBO );
	glVertexPointer( 2, GL_FLOAT, 0, NULL );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, IBO );
	glDrawElements( GL_QUADS, 4*8, GL_UNSIGNED_INT, NULL );
	//glBindVertexArray(0);
	shader->UnbindShader();
	alpha -= 0.01;
	glDisableClientState( GL_VERTEX_ARRAY );
}

void Fade::Sizing()
{

}

Effect::Effect(std::string name, int x, int y, int w, int h)
{
	this->name = name;
	this->x = x;
	this->y = y;
	this->width = w;
	this->height = h;
	this->flag = "start";
}

Effect::~Effect()
{
}

Fade::Fade(	std::string name, int w, int h, int speed) :
Effect(name, 0, 0, w, h)
{
	alpha = 1.0;
	shader = new Shader("fade");
	flag = "run";

	speed_location = glGetUniformLocation( shader->programID, "alpha" );
	vertex_location = glGetAttribLocation( shader->programID, "vertex_data");

	GLfloat vertex_data[] = {0.0,0.0,
						1.0,0.0,
						1.0,1.0,
						0.0,1.0};
	int index_data[] = {0,1,2,3};


	glGenBuffers( 1, &VBO );
	glGenVertexArrays( 1, &VAO );
	glGenBuffers( 1, &IBO );

	glBindVertexArray( VAO );
	glBindBuffer( GL_ARRAY_BUFFER, VBO );
	glEnableVertexAttribArray( vertex_location );
	glVertexAttribPointer( vertex_location, 2, GL_FLOAT, GL_FALSE, 0,NULL);
	glBufferData( GL_ARRAY_BUFFER, sizeof(GLfloat)*8, vertex_data, GL_STATIC_DRAW );
	
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, IBO );
	glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(index_data), index_data, GL_STATIC_DRAW);

	glBindVertexArray( 0 );
	glDisableVertexAttribArray( vertex_location );
	glBindBuffer( GL_ARRAY_BUFFER, 0);
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0);
}

void StarField::LoadBuffers()
{
	//(0,0), (1,0)
	//(1,1), (0,1)
	color_data.clear();
	vertex_data.clear();
	for (int i = 0; i < stars.size(); i++) {
		GLfloat size = stars.at(i).size;
		if (IBO == 0) {
			index_data.push_back(i * 4 + 0);
			index_data.push_back(i * 4 + 1);
			index_data.push_back(i * 4 + 2);
			index_data.push_back(i * 4 + 3);
		}

		vertex_data.push_back(stars.at(i).pnt.x);
		vertex_data.push_back(stars.at(i).pnt.y);

		vertex_data.push_back(stars.at(i).pnt.x + size);
		vertex_data.push_back(stars.at(i).pnt.y);

		vertex_data.push_back(stars.at(i).pnt.x + size);
		vertex_data.push_back(stars.at(i).pnt.y + size);

		vertex_data.push_back(stars.at(i).pnt.x);
		vertex_data.push_back(stars.at(i).pnt.y + size);

		for (int j = 0; j < 4; j++) {
			color_data.push_back(stars.at(i).color.r);
			color_data.push_back(stars.at(i).color.g);
			color_data.push_back(stars.at(i).color.b);
			color_data.push_back(stars.at(i).color.a);
		}

		tex_coord_data.push_back(0.0f);
		tex_coord_data.push_back(0.0f);

		tex_coord_data.push_back(1.0f);
		tex_coord_data.push_back(0.0f);

		tex_coord_data.push_back(1.0f);
		tex_coord_data.push_back(1.0f);

		tex_coord_data.push_back(0.0f);
		tex_coord_data.push_back(1.0f);
	}

	GLuint* index_data_buffer = &index_data[0];
	GLfloat* color_data_buffer = &color_data[0];
	GLfloat* vertex_data_buffer = &vertex_data[0];
	GLfloat* tex_coord_buffer = &tex_coord_data[0];


	if(VAO == 0)
		glGenVertexArrays(1, &VAO);

	glBindVertexArray(VAO);

	if (star_VBO[0] == 0)
		glGenBuffers(3, star_VBO);

	glBindBuffer(GL_ARRAY_BUFFER, star_VBO[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vertex_data.size(), vertex_data_buffer, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(vertex_location, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(vertex_location);

	glBindBuffer(GL_ARRAY_BUFFER, star_VBO[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * color_data.size(), color_data_buffer, GL_STATIC_DRAW);
	glVertexAttribPointer(color_location, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(color_location);


	glBindBuffer(GL_ARRAY_BUFFER, star_VBO[2]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * tex_coord_data.size(), tex_coord_buffer, GL_STATIC_DRAW);
	glVertexAttribPointer(tex_coord_location, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(tex_coord_location);


	if(IBO == 0)
		glGenBuffers(1, &IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * index_data.size(), index_data_buffer, GL_STATIC_DRAW);

	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(vertex_location);
	glDisableVertexAttribArray(color_location);
	glDisableVertexAttribArray(tex_coord_location);
}

void StarField::Sizing()
{
	stars.clear();
	GenerateStars();
}

void StarField::ReloadBuffers()
{
	GLfloat* color_data_buffer = &color_data[0];
	GLfloat* vertex_data_buffer = &vertex_data[0];
	glBindBuffer(GL_ARRAY_BUFFER, star_VBO[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vertex_data.size(), vertex_data_buffer, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}


StarField::StarField(std::string name, int stars_num) :
	Effect(name, 0, 0, Globals::GetInstance().screen_width, Globals::GetInstance().screen_height)
{
	VAO = 0;
	IBO = 0;
	star_VBO[0] = 0;
	star_size = GaussVector(1.0, 20.0, 12.0, 4.0);
	star_speed = GaussVector(1.0, 5.0, 120.0, 3.0);
	index_data = std::vector<GLuint>();
	vertex_data = std::vector<GLfloat>();
	color_data = std::vector<GLfloat>();
	tex_coord_data = std::vector<GLfloat>();
	stars = std::vector<Star>();
	this->stars_num = stars_num;
	star_colors = std::vector<Color>();
	star_colors.push_back(Color(255, 61, 61, 255));
	for(int k=0; k < 7; k++)
		star_colors.push_back(Color(255, 255, 255, 255));
	star_colors.push_back(Color(249, 255, 84, 255));
	star_colors.push_back(Color(84, 92, 255, 255));

	shader = new Shader("starfield");
	flag = "run";

	vertex_location = glGetAttribLocation(shader->programID, "LVertexPos2D");
	trans_location = glGetUniformLocation(shader->programID, "trans_vector");
	size_location = glGetUniformLocation(shader->programID, "size");
	color_location = glGetAttribLocation(shader->programID, "tex_color");
	tex_coord_location = glGetAttribLocation(shader->programID, "tex_coord");

	GenerateStars();
}

void StarField::GenerateStars()
{
	srand(time(NULL));
	for (int i = 0; i < stars_num; i++) {
		float cur_size = star_size.ReturnValue();
		GLfloat v = star_speed.ReturnValue();
		int rand_x = (rand() % Globals::GetInstance().screen_width) - cur_size;
		int rand_y = (rand() % (Globals::GetInstance().screen_height + (int)cur_size)) - cur_size;
		Color new_color = star_colors.at(rand() % star_colors.size());
		stars.push_back(Star(CPoint(rand_x, rand_y), new_color, v, cur_size));
	}
	shader->BindShader();
	glUniform2f(trans_location, 1.0f, 0.0f);
	glUniform2f(size_location, Globals::GetInstance().screen_width, Globals::GetInstance().screen_height);
	shader->UnbindShader();
	LoadBuffers();
}

void StarField::MoveStars()
{
	int cnt = 0;
	//stars.at(i / 8).speed;
	for (int i = 0; i < vertex_data.size(); i += 8 ) {
		Star cur_star = stars.at(i / 8);
		int screen_height = Globals::GetInstance().screen_height;
		GLfloat cur_speed = cur_star.speed / (float)Globals::GetInstance().fps;
		cur_speed = cur_speed > 10 ? 10 : cur_speed;
		vertex_data.at(i + 1) -= cur_speed;
		vertex_data.at(i + 3) -= cur_speed;
		vertex_data.at(i + 5) -= cur_speed;
		vertex_data.at(i + 7) -= cur_speed;
		if (vertex_data.at(i + 1) < 0 - cur_star.size) {
			vertex_data.at(i + 1) = screen_height + cur_star.size;
			vertex_data.at(i + 3) = screen_height + cur_star.size;
			vertex_data.at(i + 5) = screen_height;
			vertex_data.at(i + 7) = screen_height;
		}
		cnt++;
	}
	ReloadBuffers();
}

void StarField::RunEffect()
{
	MoveStars();
	shader->BindShader();
	glBindVertexArray(VAO);
	glDrawElements(GL_QUADS, 4 * stars.size(), GL_UNSIGNED_INT, NULL);
	glBindVertexArray(0);
	shader->UnbindShader();
}

Star::Star(CPoint pnt, Color color, GLfloat speed, GLfloat size)
{
	this->pnt = pnt;
	this->color = color;
	this->speed = speed;
	this->size = size;
}

Fade::~Fade()
{
}

RainGroup::RainGroup( GLfloat x1, GLfloat x2, GLfloat y1, GLfloat y2 )
{
	this->x1 = x1;
	this->x2 = x2;
	this->y1 = y1;
	this->y2 = y2;

}

Rain::Rain( std::string name, int screen_width, int screen_height, int spacing )
	: Effect(name, 0, 0, screen_width, screen_height)
{
	for( int i=0; i < (screen_width/group_spacing)+2; i++) {
		float x1 = (i*group_spacing)/screen_width;
		float x2 = 1.0;
		float y1 = 0;
		float y2 = ((screen_width*slope)+(i*group_spacing))/screen_height;
		rain_group.push_back( RainGroup( x1, x2, y1, y2) );		
	}
	for( int j=0; j < (screen_height/group_spacing)+2; j++) {
		float x1 = 0;
		float x2 = ((screen_width/slope)+(j*group_spacing))/screen_width;
		float y1 = (j*group_spacing)/screen_height;
		float y2 = 1.0;
		rain_group.push_back( RainGroup( x1, x2, y1, y2) );		
	}
}

void Rain::Sizing()
{
	
}

GrayScale::GrayScale(std::string name, int x, int y, int w, int h) :
Effect(name,x,y,w,h)
{
	//this->flag = "run";
}

void GrayScale::Sizing()
{

}

void GrayScale::RunEffect()
{
}