#ifndef OBJECT_H
#define OBJECT_H

class CPoint {
public:
	CPoint();
	CPoint(int, int);
	bool operator==(const CPoint& other);
	bool IsEquals(CPoint);
	int x;
	int y;
	bool operator<(const CPoint& other ) const
	{
		return (other.x * 999 + other.y) < (x * 999 + y); 
	}
};

class FPoint {
public:
	FPoint();
	FPoint(float,float);
	bool operator==(const FPoint& other);
	bool IsEquals(FPoint);
	float x;
	float y;
	bool operator<(const FPoint& other ) const
	{
		return (other.x * 999 + other.y) < (x * 999 + y); 
	}
};

#endif
