#ifndef COLOR_H
#define COLOR_H

#include <string>
#include <vector>
#include <boost/algorithm/string.hpp>

class Color
{
public:
	Color();
	Color(std::string);
	Color(int r, int g, int b, int a);
	Color GetScaledColor(float num);
	int r;
	int g;
	int b;
	int a;
};

#endif