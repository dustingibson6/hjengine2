#ifndef TILEMAP_H
#define TILEMAP_H

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <boost\lexical_cast.hpp>
#include <Box2D/Box2D.h>
#include <SDL.h>
#include <SDL_image.h>
#include <map>
#include <string>
#include <vector>
#include <math.h>
#include <fstream>
#include "texture_group.h"
#include "point.h"
#include "object.h"
#include "tile_sheet.h"
#include "tile.h"
#include "player.h"

class TileMap : public b2ContactListener {
	
public:
	TileMap(std::string file_name);
	~TileMap();
	virtual void BeginContact(b2Contact* contact);
	virtual void EndContact(b2Contact* contact);
	TextureGroup* GetBitmap(std::vector<std::vector<TileValue>*>*, CPoint);
	ObjectFactory* object_factory;
	int InsertToMapSet(std::map<CPoint,TileValue>*, CPoint, TileValue);
	int CountMapCells(std::map<CPoint,TileValue>*, CPoint pnt);
	int CheckPlayerCollision(Player*,int,int);
	unsigned char* TraverseToBytes(unsigned char*&, int);
	void RenderTile(CPoint,CPoint,int,int,int,int);
	void RenderScene(int,int);
	void RenderVisualScene( Player*, int, int );
	void RenderBlack(CPoint,int,int,int,int);
	ObjectInstance* GetPlayer();
	ObjectInstance* GetGlobals();
	FPoint AdjustCamera(float,float);
	FPoint GetCamera();
	SDL_Window* sdl_window;
	b2World* tile_world;
	TextureGroup* full_sheet;
	float t_sin[150];
	float t_cos[150];
	FPoint vel_point;

private:
	void LoadData();
	void RenderOverlay(SDL_Renderer* );
	void Reveal(int,int);
	std::vector<int> CheckCollision(CPoint&, int ,int ,int, int, int, int, int, int);
	int CheckCollisionNorth(CPoint&, int ,int ,int, int, int, int, int, int);
	int CheckCollisionEast(CPoint&, int ,int ,int, int, int, int, int, int);
	int CheckCollisionSouth(CPoint&, int ,int ,int, int, int, int, int, int);
	int CheckCollisionWest(CPoint&, int ,int ,int, int, int, int, int, int);
	bool IsIntersection(FPoint, FPoint, FPoint, FPoint);
	float GetDistance(FPoint, FPoint);
	float Min(float,float);
	float Max(float,float);
	FPoint FindIntersection(FPoint,FPoint,FPoint,FPoint);
	unsigned char *GetByteArray();
	std::string TraverseToString(unsigned char*&,int);
	TextureGroup* TraverseToBitmap(unsigned char*&,int);
	FPoint camera_point;
	int TraverseToInt(unsigned char*&,int);
	std::string file_name;
	std::string name;
	int tile_width;
	int tile_height;
	int col_num;
	int row_num;
	int max_col;
	int tile_set_x;
	int tile_set_y;
	TileSet* tile_set;
	SDL_Surface* black_surface;
	SDL_Texture * black_texture;
	//std::vector<ObjectInstance*>* obj_instance_list;
	//std::map<std::string, ObjectTemplate>* object_templates;
	std::vector<std::vector<std::vector<TileValue>*>*>* layer_set;
	std::vector<HardTile*>* hard_tile_set;
	SDL_Renderer* sdl_render;

	GLuint VAO;
	GLuint VBO;
	GLuint IBO;

	std::vector<GLfloat> vertex_data_vector;
	std::vector<GLfloat> clip_data_vector;
	std::vector<GLuint> index_data_vector;
	void AddVertexData(CPoint,CPoint);
};

#endif