#include "pane.h"

Pane::Pane(CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory) :
	UI("pane", pnt, definables, object_factory)
{
	this->size = definables.at("size").ToCPoint();
	this->bg_color = definables.at("pane_color").ToColor();
	this->border_color = definables.at("border color").ToColor();
	this->border_size = definables.at("border_size").ToInt();
	surface_shader = new SurfaceShader("surface", bg_color, border_color, pnt, size, border_size);
}

void Pane::Sizing()
{
	this->x = this->x * Globals::GetInstance().screen_ratx;
	this->y = this->y * Globals::GetInstance().screen_raty;
	this->size.x = size.x * Globals::GetInstance().screen_ratx;
	this->size.y = size.y * Globals::GetInstance().screen_raty;
	surface_shader = new SurfaceShader("surface", bg_color, border_color, CPoint(this->x,this->y), size, border_size);
}

void Pane::PollInput()
{

}

void Pane::RenderUI()
{
	surface_shader->BindShader();
	surface_shader->RenderStatic();
	surface_shader->UnbindShader();
}