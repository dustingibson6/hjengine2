#include "image.h"

Image::Image(TextureGroup* texture)
{
	this->image = texture;
	this->width = texture->width;
	this->height = texture->height;
	//SDL_QueryTexture(image->GetRegTex(),NULL,NULL,&this->width, &this->height);
}

TextureGroup* Image::GetTextureGroup()
{
	return this->image;
}

int Image::GetWidth()
{
	return this->width;
}

int Image::GetHeight()
{
	return this->height;
}