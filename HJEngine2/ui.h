#ifndef UI_H
#define UI_H

#include <stdio.h>
#include <iostream>
#include <SDL.h>
#include <GL/glew.h>
#include <string>
#include <map>
#include <vector>
#include "globals.h"
#include "definable.h"
#include "object.h"


class Events {
public:
	Events();
	bool click;
	void Reset();
};


class UI {

public:
	UI(std::string type, CPoint pnt, std::map<std::string, Definable> definable = std::map<std::string, Definable>(), ObjectFactory* object_Factory = new ObjectFactory());
	virtual void RenderUI();
	virtual void PollInput();
	virtual void Sizing();
	virtual std::string GetValueString();
	virtual CPoint GetValueCPoint();
	Definable GetDefinable(std::string key);
	CPoint size;
	std::string type;
	std::vector<std::string> event_queue;
	Events events;
protected:
	int x;
	int y;
	std::map<std::string, Definable> definables;
	ObjectFactory* object_factory;
};


#endif