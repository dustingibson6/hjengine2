#include "select_box.h"

SelectChoice::SelectChoice(std::string text, std::string value, bool selected)
{
	this->text = text;
	this->value = value;
	this->selected = selected;
}

SelectBox::SelectBox(CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory, std::vector<SelectChoice> choices) :
	UI("select box",pnt, definables, object_factory)
{
	this->size = definables.at("size").ToCPoint();
	status = "normal";
	left_arrow_status = "normal";
	right_arrow_status = "normal";
	this->hover_constant = definables.at("hover constant").ToGLfloat();
	this->choices = choices;
	this->choice_index = 0;
	this->font_label_size = definables.at("label font size").ToInt();
	this->text = definables.at("text").ToString();
	for(int i = 0; i < choices.size(); i++)
		if( this->choices[i].selected)
			this->choice_index = i;
	this->font = definables.at("font").ToString();
	this->font_size = definables.at("font size").ToInt();
	this->font_color = definables.at("font color").ToColor();
	this->bg_color = definables.at("background color").ToColor();
	this->border_color = definables.at("border color").ToColor();
	this->border_size = definables.at("border size").ToInt();
	//Let label dictate overall dimensions of the control
	gl_text_label = GLText(font, text, pnt, size, font_label_size, font_color);
	GLText temp = GLText(font, this->choices.at(choice_index).text, CPoint(0, 0), size, font_size, font_color);
	CPoint temp_size = temp.GetCurrentDim();
	CPoint label_dim = gl_text_label.GetCurrentDim();
	surface_dim = CPoint(this->size.x - (this->size.y*2), temp_size.y);
	left_arrow_pnt = CPoint(this->x, this->y + label_dim.y);
	surface_pnt = CPoint(this->x + surface_dim.y, this->y + label_dim.y);
	text_pnt = CPoint(surface_pnt.x + ((surface_dim.x / 2) - (temp_size.x / 2)), surface_pnt.y);
	gl_text_choice = GLText(font, this->choices.at(choice_index).text, text_pnt, size, font_size, font_color);
	CPoint text_dim = gl_text_choice.GetCurrentDim();
	arrow_dim = CPoint(surface_dim.y, surface_dim.y );
	CPoint right_triangle_pnt = CPoint(surface_pnt.x + surface_dim.y, surface_pnt.y);
	right_arrow_pnt = CPoint(surface_pnt.x + surface_dim.x, this->y + label_dim.y);
	surface_shader = new SurfaceShader("surface", bg_color, border_color, surface_pnt, surface_dim, border_size);
	left_triangle = new TriangleShader("triangle", border_color, left_arrow_pnt, arrow_dim, "left");
	right_triangle = new TriangleShader("triangle", border_color, right_arrow_pnt, arrow_dim, "right");
}

void SelectBox::Sizing()
{
	this->x = (float)this->x * Globals::GetInstance().screen_ratx;
	this->y = (float)this->y * Globals::GetInstance().screen_raty;
	this->size.x = (float)this->size.x * Globals::GetInstance().screen_ratx;
	this->size.y = (float)this->size.y * Globals::GetInstance().screen_raty;
	gl_text_label = GLText(font, text , CPoint(this->x,this->y), size, font_label_size, font_color);
	GLText temp = GLText(font, this->choices.at(choice_index).text, CPoint(0, 0), size, font_size, font_color);
	CPoint temp_size = temp.GetCurrentDim();
	CPoint label_dim = gl_text_label.GetCurrentDim();
	surface_dim = CPoint(this->size.x - (this->size.y * 2), temp_size.y);
	left_arrow_pnt = CPoint(this->x, this->y + label_dim.y);
	surface_pnt = CPoint(this->x + surface_dim.y, this->y + label_dim.y);
	text_pnt = CPoint(surface_pnt.x + ((surface_dim.x / 2) - (temp_size.x / 2)), surface_pnt.y);
	gl_text_choice = GLText(font, this->choices.at(choice_index).text, text_pnt, size, font_size, font_color);
	CPoint text_dim = gl_text_choice.GetCurrentDim();
	arrow_dim = CPoint(surface_dim.y, surface_dim.y);
	CPoint right_triangle_pnt = CPoint(surface_pnt.x + surface_dim.y, surface_pnt.y);
	right_arrow_pnt = CPoint(surface_pnt.x + surface_dim.x, this->y + label_dim.y);
	surface_shader = new SurfaceShader("surface", bg_color, border_color, surface_pnt, surface_dim, border_size);
	left_triangle = new TriangleShader("triangle", border_color, left_arrow_pnt, arrow_dim, "left");
	right_triangle = new TriangleShader("triangle", border_color, right_arrow_pnt, arrow_dim, "right");
}

void SelectBox::SetValue(std::string value)
{
	for (int i = 0; i < choices.size(); i++) {
		if (choices.at(i).value == value)
			choice_index = i;
	}
	gl_text_choice = GLText(font, this->choices.at(choice_index).text, text_pnt, size, font_size, font_color);
}

void SelectBox::PollInput()
{
	int mouse_x = 0;
	int mouse_y = 0;
	Color new_color = border_color.GetScaledColor(hover_constant);
	SDL_GetMouseState(&mouse_x, &mouse_y);
	if (mouse_x >= left_arrow_pnt.x && mouse_x <= left_arrow_pnt.x + arrow_dim.x && mouse_y >= left_arrow_pnt.y && mouse_y <= left_arrow_pnt.y + arrow_dim.y) {
		if (Globals::GetInstance().click)
		{
			this->choice_index = this->choice_index - 1;
			if (this->choice_index < 0)
				this->choice_index = this->choices.size() - 1;
			CPoint temp_size = GLText(font, this->choices.at(choice_index).text, text_pnt, size, font_size, font_color).GetCurrentDim();
			text_pnt = CPoint(surface_pnt.x + ((surface_dim.x / 2) - (temp_size.x / 2)), surface_pnt.y);
			surface_dim = CPoint(this->size.x - (this->size.y * 2), temp_size.y);
			gl_text_choice = GLText(font, this->choices.at(choice_index).text, text_pnt, size, font_size, font_color);
		}
		if (left_arrow_status == "normal")
			left_arrow_status = "hover start";
	}
	else {
		if (left_arrow_status == "hover")
			left_arrow_status = "hover out";
	}


	if (mouse_x >= right_arrow_pnt.x && mouse_x <= right_arrow_pnt.x + arrow_dim.x && mouse_y >= right_arrow_pnt.y && mouse_y <= right_arrow_pnt.y + arrow_dim.y) {
		if (Globals::GetInstance().click)
		{
			this->choice_index = this->choice_index + 1;
			if (this->choice_index >= this->choices.size())
				this->choice_index = 0;
			CPoint temp_size = GLText(font, this->choices.at(choice_index).text, text_pnt, size, font_size, font_color).GetCurrentDim();
			text_pnt = CPoint(surface_pnt.x + ((surface_dim.x / 2) - (temp_size.x / 2)), surface_pnt.y);
			surface_dim = CPoint(this->size.x - (this->size.y * 2), temp_size.y);
			gl_text_choice = GLText(font, this->choices.at(choice_index).text, text_pnt, size, font_size, font_color);

		}
		if (right_arrow_status == "normal")
			right_arrow_status = "hover start";
	}
	else {
		if (right_arrow_status == "hover")
			right_arrow_status = "hover out";
	}


	if (right_arrow_status == "hover start") {
		right_triangle->ChangeColors(new_color);
		right_arrow_status = "hover";
	}
	if (right_arrow_status == "hover out") {
		right_triangle->ChangeColors(border_color);
		right_arrow_status = "normal";
	}


	if (left_arrow_status == "hover start") {
		left_triangle->ChangeColors(new_color);
		left_arrow_status = "hover";
	}
	if (left_arrow_status == "hover out") {
		left_triangle->ChangeColors(border_color);
		left_arrow_status = "normal";
	}

}

std::string SelectBox::GetValueString()
{
	return this->choices.at(choice_index).value;
}

CPoint SelectBox::GetValueCPoint()
{
	std::string value = GetValueString();
	std::vector<std::string> values = std::vector<std::string>();
	boost::split(values, value, boost::is_any_of(","));
	return CPoint( atoi(values.at(0).c_str()), atoi(values.at(1).c_str()));
}


void SelectBox::RenderUI()
{
	surface_shader->BindShader();
	surface_shader->RenderStatic();
	surface_shader->UnbindShader();
	left_triangle->BindShader();
	left_triangle->RenderStatic();
	left_triangle->UnbindShader();
	right_triangle->BindShader();
	right_triangle->RenderStatic();
	right_triangle->UnbindShader();
	gl_text_label.RenderText();
	gl_text_choice.RenderText();
}