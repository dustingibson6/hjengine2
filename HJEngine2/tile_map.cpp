#include "tile_map.h"

TileMap::TileMap(std::string file_name)
{
	vertex_data_vector = std::vector<GLfloat>();
	index_data_vector = std::vector<GLuint>();
	clip_data_vector = std::vector<GLfloat>();
	hard_tile_set = new std::vector<HardTile*>();
	b2Vec2 gravity(0.0,0.0);
	tile_world = new b2World(gravity);
	this->file_name = file_name;
	this->layer_set = new std::vector<std::vector<std::vector<TileValue>*>*>();
	this->camera_point = FPoint(0,0);
	this->object_factory = new ObjectFactory();
	LoadData();
	for(int k=0; k < layer_set->size(); k++)
		for(int i=0; i < col_num; i++)
			for(int j=0; j < row_num; j++) {
				int* tmp_col = layer_set->at(k)->at(i)->at(j).col;
					bool has_col = 0;
					if( tmp_col[0] == 1 || tmp_col[1] == 1 || tmp_col[2] == 1 || tmp_col[3] == 1 ) {
						//float x = (tile_width/2.0) + (tile_width*i); 
						//float y = (tile_height/2.0) + (tile_height*j) ;
						float x = i*tile_width;
						float y = j*tile_height;
						HardTile* hard_tile = new HardTile(tile_world,FPoint(x,y),tile_width,tile_height, tmp_col);
						hard_tile_set->push_back(hard_tile);
						//return;
					}
			}
	tile_world->SetContinuousPhysics(true);
	tile_world->SetWarmStarting(true);
	tile_world->SetSubStepping(false);
	tile_world->SetAllowSleeping(false);
	float gravity_x = GetGlobals()->GetFloatValue("gravity x");
	float gravity_y = GetGlobals()->GetFloatValue("gravity y");
	//tile_world->SetGravity( b2Vec2(gravity_x,gravity_y));
	float s_step = (2.0*3.14159)/150;
	int cnt = 0;
	for(float s = 0.0; s <= 2.0*3.14159; s += s_step)
	{
		t_sin[cnt] = sin(s);
		t_cos[cnt] = cos(s);
		cnt++;
	}
	vel_point = FPoint( 0.0f, 0.0f );
}

TileMap::~TileMap()
{
	delete object_factory;
	delete tile_set;
}

void TileMap::BeginContact(b2Contact* contact)
{
	B2_NOT_USED(contact);
}

void TileMap::EndContact(b2Contact* contact)
{
	B2_NOT_USED(contact);
}

FPoint TileMap::AdjustCamera(float dx, float dy)
{
	vel_point = FPoint( dx, dy );
	camera_point.x += dx;
	camera_point.y += dy;
	return camera_point;
}

FPoint TileMap::GetCamera()
{
	return camera_point;
}

std::string TileMap::TraverseToString(unsigned char* &buffer, int size)
{
	unsigned char* string_buffer = new unsigned char[size];
	for(int i=0; i < size; i++)
		*(string_buffer + i) = *(buffer++);
	return std::string((char*)string_buffer);
}

void TileMap::RenderTile(CPoint clip_point, CPoint render_point, int sw, int sh, int w, int h)
{
	float r_x = (float) render_point.x / sw + camera_point.x;
	float r_y = (float) render_point.y / sh - camera_point.y;
	float c_x = (float) clip_point.x / full_sheet->width;
	float c_y = (float) clip_point.y / full_sheet->height;
	float cw_r = (float) w / full_sheet->width;
	float ch_r = (float) h / full_sheet->height;
	float rw_r = (float) w / sw;
	float rh_r = (float) h / sh;

	full_sheet->polyShader->RenderStatic();
}

float TileMap::Min(float a, float b)
{
	if( a < b)
		return a;
	return b;
}

float TileMap::Max(float a, float b)
{
	if( a >= b)
		return a;
	return b;
}

FPoint TileMap::FindIntersection(FPoint blockA, FPoint blockB, FPoint rayA, FPoint rayB)
{


	float xa0 = blockA.x;
	float ya0 = blockA.y;
	float ma = (blockB.x - blockA.x);
	float na = (blockB.y - blockA.y);
	float xb0 = rayA.x;
	float yb0 = rayA.y;
	float mb = (rayB.x - rayA.x);
	float nb = (rayB.y - rayA.y);

	if( ma == 0 )
	{
		float t = ( xa0 - xb0 ) / mb;
		float xb = xb0 + mb*t;
		float yb = yb0 + nb*t;
		float s =  (yb - ya0) / na;
			if( t >= 0.0 && t <= 1.0 && s >= 0.0 && s <= 1.0 )
				return FPoint( xb, yb );
	}

	else if( na == 0 )
	{
		float t = ( ya0 - yb0 ) / nb;
		float xb = xb0 + mb*t;
		float yb = yb0 + nb*t;
		float s =  (xb - xa0) / ma;
			if( t >= 0.0 && t <= 1.0 && s >= 0.0 && s <= 1.0 )
				return FPoint( xb, yb );
	}
	return FPoint(-1,-1);
}

float TileMap::GetDistance(FPoint a, FPoint b)
{
	return abs( a.x - b.x ) + abs( a.y - b.y );
	//return sqrt( ((a.x - b.x)*(a.x - b.x)) + ((a.y - b.y)*(a.y - b.y)) );
}

bool TileMap::IsIntersection(FPoint a, FPoint b, FPoint s, FPoint t)
{
	if( a.x >= s.x && a.x <= t.x && a.y >= s.y && a.y <= t.y)
		if( b.x >= s.x && b.x <= t.x && b.y >= s.y && b.y <= t.y)
			return true;
	return false;
}


void TileMap::RenderVisualScene(Player* ply, int width, int height )
{
	CPoint ply_pnt = Globals::GetInstance().ToPixels(ply->b2d_body->GetPosition().x,ply->b2d_body->GetPosition().y,ply->width,ply->height);
	for(int i = 0; i < 150; i++)
	{
		FPoint rayA = FPoint(0.0,0.0);
		FPoint rayB = FPoint(0.0,0.0);
		float angle_x = t_cos[i];
		float angle_y = t_sin[i];
		float ar = (float)width/height;
		float unorm_tx = (float)(width/2) + angle_x*width;
		float unorm_ty = (float)(height/2) + angle_y*height;
		float c_x = camera_point.x * width;
		float c_y = camera_point.y * height;
		float r_x = (ply_pnt.x+16.0) + c_x;
		float r_y = height - (ply_pnt.y+16.0) - c_y;	
		float min_distance = 999999999;
		FPoint intPoint = FPoint( unorm_tx, unorm_ty );
		for(int i = 0; i < hard_tile_set->size(); i++)
		{
			HardTile* curTile = hard_tile_set->at(i);
			//curTile->pnt.x = curTile->pnt.x + c_x;
			//curTile->pnt.y = curTile->pnt.y - c_y;
			float bx = curTile->pnt.x + c_x;
			float by = curTile->pnt.y + c_y;
			rayA = FPoint( r_x , r_y + 32.0 );
			rayB = FPoint( unorm_tx, unorm_ty);
			FPoint northA = FPoint( bx, by);
			FPoint northB = FPoint( bx + tile_width, by);
			FPoint southA = FPoint( bx, by + tile_height);
			FPoint southB = FPoint( bx + tile_width, by + tile_height);
			FPoint westA = FPoint( bx, by);
			FPoint westB = FPoint( bx, by + tile_height);
			FPoint eastA = FPoint( bx + tile_width, by );
			FPoint eastB = FPoint( bx + tile_width, by + tile_height);
	
			FPoint northInt = FindIntersection( northA, northB, rayA, rayB );
			FPoint southInt = FindIntersection( southA, southB, rayA, rayB );
			FPoint eastInt = FindIntersection( eastA, eastB, rayA, rayB );
			FPoint westInt = FindIntersection( westA, westB, rayA, rayB );

			int tile_x = curTile->pnt.x / tile_set->tile_width;
			int tile_y = curTile->pnt.y  / tile_set->tile_height;

			if( tile_x > 0 && tile_y > 0 && tile_x < width/tile_width && tile_y < height/tile_height )
				for(int k=0; k < layer_set->size(); k++)
					layer_set->at(k)->at(tile_x)->at(tile_y).view = true;

			if( !(northInt == FPoint(-1,-1)))
			{
				float dist = GetDistance( rayA, northInt );
				if( dist <= min_distance ) {
					min_distance = dist;
					intPoint = northInt;
				}
			}

			if( !(eastInt == FPoint(-1,-1)))
			{
				float dist = GetDistance( rayA, eastInt );
				if( dist <= min_distance ) {
					min_distance = dist;
					intPoint = eastInt;
				}
			}

			if( !(southInt == FPoint(-1,-1)))
			{
				float dist = GetDistance( rayA, southInt );
				if( dist <= min_distance ) {
					min_distance = dist;
					intPoint = southInt;
				}
			}

			if( !(westInt == FPoint(-1,-1)))
			{
				float dist = GetDistance( rayA, westInt );
				if( dist <= min_distance ) {
					min_distance = dist;
					intPoint = westInt;
				}
			}
		}
		float norm_int_x = intPoint.x/width;
		float norm_int_y = ((height-intPoint.y)/height);
		float norm_rx = r_x / width;
		float norm_ry = (r_y / height);
		float pX = rayA.x - c_x; 
		float pY = rayA.y - c_y;
		float iX = intPoint.x - c_x;
		float iY = intPoint.y - c_y;
		float t_step = 0.01;
		for(float t = 0.0; t <= 1.0; t += t_step)
		{
			float f_x = (pX + (iX - pX)*t) ;
			float f_y = (pY + (iY - pY)*t) ;

			int tile_x = f_x / tile_width;
			int tile_y = f_y / tile_height;
			
			CPoint render_point = CPoint(f_x,f_y);

			if( tile_x > 0 && tile_y > 0 && tile_x < width/tile_width && tile_y < height/tile_height )
				for(int k=0; k < layer_set->size(); k++)
					layer_set->at(k)->at(tile_x)->at(tile_y).view = true;
		}
	}
}

void TileMap::RenderScene( int width, int height)  
{
	glBindTexture(GL_TEXTURE_2D,full_sheet->GetTexture());
	full_sheet->polyShader->BindShader();
	full_sheet->polyShader->SetAngle(0.0f);
	full_sheet->polyShader->SetTransVector(camera_point.x, camera_point.y*-1);
	float vx = vel_point.x;
	float vy = vel_point.y;
	RenderTile(CPoint(tile_set->tile_width, tile_set->tile_height),CPoint(0,0),1920,1080, tile_set->tile_width, tile_set->tile_height);
	glBindTexture(GL_TEXTURE_2D,0);
	full_sheet->polyShader->UnbindShader();
}

void TileMap::RenderBlack(CPoint render_point, int sw, int sh, int w, int h)
{
	float r_x = (float) render_point.x / sw + camera_point.x;
	float r_y = (float) render_point.y / sh - camera_point.y;
	float rw_r = (float) w / sw;
	float rh_r = (float) h / sh;
	glColor3f(0.0f,0.0f,0.0f);
	glBegin( GL_QUADS);
		glVertex2f( r_x, r_y);
		glVertex2f( r_x + rw_r, r_y );
		glVertex2f( r_x + rw_r, r_y + rh_r);
		glVertex2f( r_x, r_y + rh_r);
	glEnd();
}


void TileMap::Reveal(int x, int y)
{
}

ObjectInstance* TileMap::GetPlayer()
{
	return object_factory->player_instance;
}

ObjectInstance* TileMap::GetGlobals()
{
	return object_factory->globals_instance;
}

int TileMap::TraverseToInt(unsigned char* &buffer, int size)
{
	unsigned char* int_buffer = new unsigned char[size];
	int resulting_int = 0;
	for(int i=0; i < size; i++)
		*(int_buffer + i) = *(buffer++);
	if(size == 4) {
		resulting_int = (resulting_int << 8) + int_buffer[3];
		resulting_int = (resulting_int << 8) + int_buffer[2];
		resulting_int = (resulting_int << 8) + int_buffer[1];
		resulting_int = (resulting_int << 8) + int_buffer[0];
	}
	else {
		resulting_int = (resulting_int << 8) + int_buffer[1];
		resulting_int = (resulting_int << 8) + int_buffer[0];
	}
	return resulting_int;
}

TextureGroup* TileMap::TraverseToBitmap(unsigned char* &buffer, int size)
{
	unsigned char* image_buffer = new unsigned char[size];
	for(int i=0; i < size; i++) {
		*(image_buffer + i) = *(buffer++);
	}
	TextureGroup* resulting_bitmap = new TextureGroup(image_buffer,size);
	return resulting_bitmap;
}

unsigned char* TileMap::GetByteArray()
{
	std::ifstream file_stream(file_name,std::ifstream::binary);
	file_stream.seekg(0, std::ios::end );
	std::size_t length = file_stream.tellg();
	unsigned char* byte_array = new unsigned char[length];
	file_stream.seekg(0, std::ios::beg);
	file_stream.read((char*)byte_array, length);
	file_stream.close();
	return byte_array;
}

int TileMap::CountMapCells(std::map<CPoint,TileValue>* map_set, CPoint pnt)
{
	int cnt = 0;
	for( std::map<CPoint,TileValue>::iterator it=map_set->begin(); it != map_set->end(); ++it)
		if(CPoint(it->first.x,it->first.y) == pnt)
			cnt++;
	return cnt;
}

int TileMap::InsertToMapSet(std::map<CPoint,TileValue>* map_set, CPoint pnt, TileValue tile_value )
{
		map_set->insert(std::pair<CPoint,TileValue>(pnt,tile_value));	
	return 0;
}

unsigned char* TileMap::TraverseToBytes(unsigned char* &buffer, int size)
{
	unsigned char* byte_buffer = new unsigned char[size];
	for(int i=0; i < size; i++) 
		*(byte_buffer + i) = *(buffer++);
	return byte_buffer;
}

void TileMap::AddVertexData(CPoint vert_coord, CPoint tex_coord)
{
	//0: (0,0)
	//1: (1,0)
	//2: (1,1)
	//3: (0,1)

	float w = tile_set->tile_width ;
	float h = tile_set->tile_height ;
	float sw = Globals::GetInstance().res_width;
	float sh = Globals::GetInstance().res_height;

	vert_coord.x = vert_coord.x*w;
	vert_coord.y = vert_coord.y*h;
	vert_coord.y = sh-(vert_coord.y)-h;

	if(tex_coord.x < 0 )
		tex_coord.x = 0;
	if(tex_coord.y < 0 )
		tex_coord.y = 0;

	tex_coord.x = tex_coord.x*w; 
	tex_coord.y = tex_coord.y*h;

	tex_coord.y = full_sheet->height - tex_coord.y  - h;


	float r_x = (float) vert_coord.x / sw;
	float r_y = (float) vert_coord.y / sh;
	float c_x = (float) tex_coord.x / (full_sheet->width);
	float c_y = (float) tex_coord.y / (full_sheet->height);
	float cw_r = (float) w / (full_sheet->width);
	float ch_r = (float) h / (full_sheet->height);
	float rw_r = (float) w / sw;
	float rh_r = (float) h / sh;

	vertex_data_vector.push_back( r_x );
	vertex_data_vector.push_back( r_y );

	vertex_data_vector.push_back( r_x + rw_r );
	vertex_data_vector.push_back( r_y );

	vertex_data_vector.push_back( r_x + rw_r );
	vertex_data_vector.push_back( r_y + rh_r );

	vertex_data_vector.push_back( r_x );
	vertex_data_vector.push_back( r_y + rh_r );


	clip_data_vector.push_back(c_x);
	clip_data_vector.push_back(c_y);

	clip_data_vector.push_back(c_x + cw_r);
	clip_data_vector.push_back(c_y);

	clip_data_vector.push_back(c_x + cw_r);
	clip_data_vector.push_back(c_y + ch_r);

	clip_data_vector.push_back(c_x);
	clip_data_vector.push_back(c_y + ch_r);


}

void TileMap::LoadData()
{
	unsigned char* main_buffer = GetByteArray();
	name = TraverseToString(main_buffer,20);
	//Tile Width
	tile_width = TraverseToInt(main_buffer,2); 
	//Tile Height
	tile_height = TraverseToInt(main_buffer,2);
	//Rows
	row_num = TraverseToInt(main_buffer,2);
	//Columns
	col_num = TraverseToInt(main_buffer,2);
	//Tile Sheet Columns
	tile_set_x = TraverseToInt(main_buffer,2);
	//Tile Sheet Rows
	tile_set_y = TraverseToInt(main_buffer,2);
	//Tile Set MaxCol
	max_col = TraverseToInt(main_buffer,2);
	tile_set = new TileSet(tile_width,tile_height,tile_set_x,tile_set_y);
	//Number of Layers
	int layer_count = TraverseToInt(main_buffer,2);
	//Tile Set Images
	for(int i=0; i < tile_set_x; i++)
		for(int j=0; j < tile_set_y; j++)
		{
			if( j == tile_set_y - 1 && max_col < i)
				break;
			int img_size = TraverseToInt(main_buffer,4);
			TraverseToBitmap(main_buffer,img_size);
			//tile_set->AddTile(bitmap_val,CPoint(i,j));

		}
	//Full Bitmap Size
	int full_bitmap_size = TraverseToInt(main_buffer,4);
	//Full Bitmap
	this->full_sheet = TraverseToBitmap(main_buffer,full_bitmap_size);
	//Map Set
	int cnt=0;
	for(int i = 0; i < layer_count; i++)
	{
		std::map<CPoint,TileValue>* map_set = new std::map<CPoint,TileValue>();
		std::vector<std::vector<TileValue>*>* temp_vect_x = new std::vector<std::vector<TileValue>*>();
		for(int j = 0; j < col_num; j++) {
			std::vector<TileValue>* temp_vect_y = new std::vector<TileValue>();
			for(int k=0; k < row_num; k++) {
				int tmp_x = TraverseToInt(main_buffer,2);
				int tmp_y = TraverseToInt(main_buffer,2);
				int tmp_val_x = (short)TraverseToInt(main_buffer,2);
				int tmp_val_y = (short)TraverseToInt(main_buffer,2);
				unsigned char* tmp_col = TraverseToBytes(main_buffer,2);
				CPoint key_pnt = CPoint(tmp_x,tmp_y);
				CPoint val_pnt = CPoint(tmp_val_x,tmp_val_y);
				TileValue tile_val = TileValue(tile_world,val_pnt,tmp_col);
				temp_vect_y->insert(temp_vect_y->end(),tile_val);
				AddVertexData( key_pnt, val_pnt );
				index_data_vector.push_back( cnt );
				cnt++;
				index_data_vector.push_back( cnt );
				cnt++;
				index_data_vector.push_back( cnt );
				cnt++;
				index_data_vector.push_back( cnt );
				cnt++;
			}
			temp_vect_x->insert(temp_vect_x->end(),temp_vect_y);
		}
		layer_set->insert(layer_set->begin(),temp_vect_x);
	}
	//Number of object templates
	int num_obj_template = TraverseToInt(main_buffer, 2);
	for(int i=0; i < num_obj_template; i++)
	{
		std::map<std::string, Property>* prop_map = new std::map<std::string, Property>();
		ImageStates image_states = ImageStates();
		//Object Name
		std::string obj_name = TraverseToString(main_buffer, 20);
		//Number of Properties
		int prop_num = TraverseToInt(main_buffer, 4);
		for(int j=0; j < prop_num; j++)
		{
			//Property name
			std::string prop_name = TraverseToString(main_buffer, 20);
			//Property data type
			std::string prop_data_type = TraverseToString(main_buffer, 20);
			Property temp_prop = Property(prop_name, prop_data_type, 0, NULL);
			prop_map->insert(std::pair<std::string, Property>(prop_name, temp_prop));
		}
		//Number of States
		int state_num = TraverseToInt(main_buffer, 2);
		for(int j=0; j < state_num; j++)
		{
			//State name
			std::string state_name = TraverseToString(main_buffer, 20);
			//Bitmap size
			int state_size = TraverseToInt(main_buffer, 4);
			//Bitmap itself
			TextureGroup* state_image = TraverseToBitmap(main_buffer, state_size);
			image_states.AddState(state_name,state_image);
		}
		object_factory->AddTemplate(obj_name,prop_map,image_states);
	}
	//Number of Object Instance
	int obj_instance_num = TraverseToInt(main_buffer,2);
	for(int j=0; j < obj_instance_num; j++)
	{
		std::map<std::string, Property*>* cur_prop_map = new std::map<std::string, Property*>();
		//Name of Object
		std::string obj_inst_name = TraverseToString(main_buffer,20);
		//Visible
		int visible = TraverseToInt(main_buffer,2);
		//Int x
		int x_inst = TraverseToInt(main_buffer, 2);
		//Int y
		int y_inst = TraverseToInt(main_buffer, 2);
		//Number of Properties
		int prop_num = TraverseToInt(main_buffer, 2);
		for(int j=0; j < prop_num; j++)
		{
			//Property name
			std::string prop_name = TraverseToString(main_buffer,20);
			//Propety Data Type
			std::string prop_data_type = TraverseToString(main_buffer, 20);
			//User asset?
			int use_asset = TraverseToInt(main_buffer, 2);
			//Asset name
			std::string asset_name = TraverseToString(main_buffer, 20);
			//Property size
			int prop_size = TraverseToInt(main_buffer, 4);
			//Propety value
			unsigned char* prop_value = TraverseToBytes(main_buffer, prop_size);
			Property* prop_temp = new Property(prop_name, prop_data_type, use_asset, asset_name, prop_size, prop_value);
			cur_prop_map->insert(std::pair<std::string, Property*>(prop_name,prop_temp));
		}
		CPoint inst_point = CPoint(x_inst, y_inst);
		//ObjectInstance* obj_instance = new ObjectInstance(obj_inst_name, cur_prop_map, inst_point, visible);
		object_factory->AddObject(obj_inst_name, cur_prop_map, inst_point, visible);
		//obj_instance_list->insert(obj_instance_list->begin(), obj_instance);
	}
	//Number of Assets
	int num_assets = (short)TraverseToInt(main_buffer, 2);
	for(int i = 0; i < num_assets; i++)
	{
		//Asset name
		std::string asset_name = TraverseToString(main_buffer, 20);
		//Asset data type
		std::string asset_data_type = TraverseToString(main_buffer, 20);
		//Asset size
		int asset_size = TraverseToInt(main_buffer, 4);
		//Asset buffer
		unsigned char* asset_buffer = TraverseToBytes(main_buffer, asset_size);
		object_factory->AddAssetMap(asset_name,asset_data_type,asset_size,asset_buffer);
	}
	
	full_sheet->SetupPolyShader();

	GLuint* index_data = &index_data_vector[0];
	GLfloat* vertex_data = &vertex_data_vector[0];
	GLfloat* clip_data = &clip_data_vector[0];

	int index_data_size = sizeof(GLuint)*index_data_vector.size();
	int vertex_data_size = sizeof(GLfloat)*vertex_data_vector.size();
	int clip_data_size = sizeof(GLfloat)*clip_data_vector.size();

	full_sheet->polyShader->LoadArrayData(index_data, vertex_data, clip_data, index_data_vector.size(), vertex_data_vector.size(), clip_data_vector.size()); 
}