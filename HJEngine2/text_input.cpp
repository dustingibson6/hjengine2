#include "text_input.h"

TextInput::TextInput( CPoint pnt, CPoint max_size, int font_size, Color font_color, Color bg_color ) :
	UI( "text", pnt )
{
	Color alpha_color = Color( 0,0,0,0 );
	text = "";
	padding = CPoint( 0, 0 );
	CPoint p_pnt = CPoint( pnt );
	p_pnt.x = p_pnt.x + padding.x;
	p_pnt.y = p_pnt.y + padding.y;
	gl_text = GLText("Calibri",text,p_pnt, max_size, font_size, font_color);
	surface_shader = new SurfaceShader("surface", bg_color, alpha_color, pnt, CPoint(gl_text.max_w + padding.x * 2, gl_text.cur_h + padding.y * 2), 3);
	cursor_shader = new SurfaceShader( "surface", font_color, alpha_color, p_pnt, CPoint(gl_text.font_width, gl_text.font_height), 0 );
}

TextInput::TextInput()
	: UI("text",CPoint(0,0))
{
}

void TextInput::Reset()
{
	gl_text.Reset();
	surface_shader->ChangeSize( gl_text.max_w+padding.x*2, gl_text.cur_h+padding.y*2 );
	text = "";
}

void TextInput::PollInput()
{
}

void TextInput::RenderUI()
{
	int ch = Globals::GetInstance().key_ascii;
	std::string new_text = "";
	new_text += char(ch);
	if( ch != 0 ) {
		if( ch != 8 )
			text += char( ch );
		else
			if( text != "" )
				text.pop_back();
		gl_text.ChangeText( new_text );
		cursor_shader->ChangePosition( gl_text.end_x, gl_text.end_y );
		surface_shader->ChangeSize( gl_text.max_w + padding.x*2, gl_text.cur_h + padding.y*2);
	}

	surface_shader->BindShader();
	surface_shader->RenderStatic();
	surface_shader->UnbindShader();

	cursor_shader->BindShader();
	cursor_shader->RenderStatic();
	cursor_shader->UnbindShader();

	gl_text.RenderText();
}