#ifndef GAUSS_VECTOR_H
#define GAUSS_VECTOR_H

#include <stdio.h>
#include <vector>
#include <math.h>
#include <time.h>
#include <random>

class ProbValue {
public:
	ProbValue(float lower, float upper, float value, float prob);
	float lower;
	float upper;
	float value;
	float prob;
};


class GaussVector {
public:
	GaussVector(float lower, float upper, float mean, float sd);
	GaussVector();
	void Insert(float value);
	float ReturnValue();
	std::vector<float> values;
private:
	float ComputeGauss(float x, float mean, float sd);
	float IntegrateGauss(float lower, float upper, float mean, float sd);
	float FindValue(int cur_num);
	int prob_delta;
	std::vector<ProbValue> prob_values;
	std::default_random_engine random_generator;
	std::uniform_int_distribution<int> distribution;
};


#endif