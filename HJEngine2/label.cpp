#include "label.h"

Label::Label(CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory) :
	UI("label", pnt, definables, object_factory)
{
	this->size = definables.at("size").ToCPoint();
	this->font_size = definables.at("label font size").ToInt();
	this->font_color = definables.at("font color").ToColor();
	this->font = definables.at("font").ToString();
	this->text = definables.at("text").ToString();
	gl_text = GLText(font, text, pnt, size, font_size, font_color);
}

void Label::Sizing()
{
	this->x = this->x * Globals::GetInstance().screen_ratx;
	this->y = this->y * Globals::GetInstance().screen_raty;
	this->size.x = size.x * Globals::GetInstance().screen_ratx;
	this->size.y = size.y * Globals::GetInstance().screen_raty;
	gl_text = GLText(font, text, CPoint(this->x,this->y), size, font_size, font_color);
}

void Label::PollInput()
{

}

void Label::RenderUI()
{
	gl_text.RenderText();
}