#ifndef MENU_H
#define MENU_H

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <vector>
#include <map>
#include <boost/algorithm/string.hpp>
#include "point.h"
#include "globals.h"
#include "image.h"
#include "rapidxml.hpp"
#include "ui_factory.h"
#include "config.h"
#include "effects.h"

class MenuItem {
public:
	MenuItem(std::string,int,int,TTF_Font*,SDL_Color,SDL_Color);
	virtual void DrawMenu(SDL_Renderer*,int);
	Image* inactive_image;
	Image* hover_image;
	int x;
	int y;
protected:
	std::string type;
	TTF_Font* font;
	SDL_Color text_color;
	SDL_Color border_color;
	SDL_Surface* inactive_surface;
	SDL_Surface* hover_surface;
};

class ButtonItem : public MenuItem {
public:
	ButtonItem(std::string,int,int,std::string,TTF_Font*,SDL_Color,SDL_Color,std::string);
	void DrawMenu(SDL_Renderer*,int) override;
	std::string GetLink();
private:
	std::string text;
	std::string type;
	std::string link;
};

class Menu {
public:
	Menu(std::string);
	void AddButtonItem(std::string,std::string,int,int);
	void AddButton(std::string name, CPoint pnt, std::map<std::string, Definable> definable, ObjectFactory* object_factory);
	void AddToggleBox(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory, bool checked);
	void AddSelectBox(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory, std::vector<SelectChoice> choices);
	void AddImage(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory);
	void AddPane(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory);
	void AddLabel(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory);
	void AddValueBox(std::string name, CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory);
	void DrawMenu();
	void ChangeIndex(int);
	std::vector<UI*> GetUIList();
	std::vector<UI*> PollEvents();
	MenuItem* GetMenuItemByIndex(int);
	void ResizeUIs();
	int MenuItemSize();
private:
	int active_index;
	std::string name;
	std::vector<MenuItem*>* menu_item_list;
	std::vector<Menu*>* sub_menus;
	UIFactory ui_factory;
	TTF_Font* font;
	SDL_Color text_color;
	SDL_Color border_color;
};

class MenuFactory {
public:
	MenuFactory();
	void SaveConfigValues();
	void AddFromXML(std::string);
	void AddMenu(std::string);
	void DrawMenu();
	void SetMenu(std::string);
	void ChangeActiveMenu(std::string);
	void PollActiveMenuItem(int,int,bool);
	void LoadValues();
	Menu* GetMenu(std::string);
	Menu* GetActiveMenu();
	std::vector<std::string> PollMenuEvents();
	~MenuFactory();
private:
	EffectFactory* effect_factory;
	std::string active_menu_name;
	std::map<std::string,Menu*>* menu_map;
	unsigned char CharToInt8(std::string);
	Config config;
};

#endif
