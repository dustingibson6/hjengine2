#include "color.h"

Color::Color(std::string value)
{
	std::vector<std::string> color_strings = std::vector<std::string>();
	boost::split(color_strings, value, boost::is_any_of(","));
	this->r = atoi(color_strings[0].c_str());
	this->g = atoi(color_strings[1].c_str());
	this->b = atoi(color_strings[2].c_str());
	this->a = atoi(color_strings[3].c_str());
	//Color(new_r, new_g, new_b, new_a);
}

Color::Color(int nr, int ng, int nb, int na)
{
	this->r = (nr <= 255) ? nr : 255 ;
	this->g = (ng <= 255) ? ng : 255 ;
	this->b = (nb <= 255) ? nb : 255 ;
	this->a = na;
}

Color::Color()
{

}

Color Color::GetScaledColor(float num)
{
	int new_r = (float)r * num;
	int new_g = (float)g * num;
	int new_b = (float)b * num;
	return Color(new_r, new_g, new_b, a);
}