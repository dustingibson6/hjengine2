#include "value_box.h"

ValueBox::ValueBox(CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_factory ) :
	UI("value box", pnt, definables, object_factory)
{
	this->size = definables.at("size").ToCPoint();
	status = "normal";
	left_arrow_status = "normal";
	right_arrow_status = "normal";
	this->max_value = definables.at("max value").ToInt();
	this->hover_constant = definables.at("hover constant").ToGLfloat();
	this->choice_index = 0;
	this->font_label_size = definables.at("label font size").ToInt();
	this->text = definables.at("text").ToString();
	this->font = definables.at("font").ToString();
	this->font_color = definables.at("font color").ToColor();
	this->bg_color = definables.at("background color").ToColor();
	this->border_color = definables.at("border color").ToColor();
	this->border_size = definables.at("border size").ToInt();
	this->cur_value = definables.at("current value").ToInt();
	//Let label dictate overall dimensions of the control
	GLfloat square_w = (size.x - (size.y * 2)) / (float) ( max_value );
	box_shaders = std::vector<SurfaceShader*>();
	gl_text_label = GLText(font, text, pnt, size, font_label_size, font_color);
	CPoint label_dim = gl_text_label.GetCurrentDim();
	CPoint surface_dim = CPoint(size.x - (size.y * 2), size.y);
	left_arrow_pnt = CPoint(this->x, this->y + label_dim.y);
	surface_pnt = CPoint(this->x + size.y, this->y + label_dim.y);
	arrow_dim = CPoint(size.y, size.y);
	CPoint right_triangle_pnt = CPoint(this->x, surface_pnt.y);
	right_arrow_pnt = CPoint(surface_pnt.x + surface_dim.x, this->y + label_dim.y);
	surface_shader = new SurfaceShader("surface", bg_color, border_color, surface_pnt, surface_dim, border_size);
	left_triangle = new TriangleShader("triangle", border_color, left_arrow_pnt, arrow_dim, "left");
	right_triangle = new TriangleShader("triangle", border_color, right_arrow_pnt, arrow_dim, "right");
	for (int i = 0; i < max_value; i++) {
		CPoint square_size = CPoint( square_w - 0.2*square_w , size.y - 0.4*size.y);
		CPoint square_pnt = CPoint(surface_pnt.x + (square_w*i) + 0.2*square_w , surface_pnt.y + 0.2*size.y);
		box_shaders.push_back(new SurfaceShader("surface", border_color, border_color, square_pnt, square_size, 0));
	}
}

void ValueBox::Sizing()
{

}

void ValueBox::PollInput()
{
	int mouse_x = 0;
	int mouse_y = 0;
	Color new_color = border_color.GetScaledColor(hover_constant);
	SDL_GetMouseState(&mouse_x, &mouse_y);
	if (mouse_x >= left_arrow_pnt.x && mouse_x <= left_arrow_pnt.x + arrow_dim.x && mouse_y >= left_arrow_pnt.y && mouse_y <= left_arrow_pnt.y + arrow_dim.y) {
		if (Globals::GetInstance().click)
		{
			cur_value = cur_value - 1;
			if (cur_value < 0)
				cur_value = cur_value + 1;
		}
		if (left_arrow_status == "normal")
			left_arrow_status = "hover start";
	}
	else {
		if (left_arrow_status == "hover")
			left_arrow_status = "hover out";
	}
	if (mouse_x >= right_arrow_pnt.x && mouse_x <= right_arrow_pnt.x + arrow_dim.x && mouse_y >= right_arrow_pnt.y && mouse_y <= right_arrow_pnt.y + arrow_dim.y) {
		if (Globals::GetInstance().click)
		{
			cur_value = cur_value + 1;
			if (cur_value > max_value)
				cur_value = cur_value - 1;
		}
		if (right_arrow_status == "normal")
			right_arrow_status = "hover start";
	}
	else {
		if (right_arrow_status == "hover")
			right_arrow_status = "hover out";
	}


	if (right_arrow_status == "hover start") {
		right_triangle->ChangeColors(new_color);
		right_arrow_status = "hover";
	}
	if (right_arrow_status == "hover out") {
		right_triangle->ChangeColors(border_color);
		right_arrow_status = "normal";
	}


	if (left_arrow_status == "hover start") {
		left_triangle->ChangeColors(new_color);
		left_arrow_status = "hover";
	}
	if (left_arrow_status == "hover out") {
		left_triangle->ChangeColors(border_color);
		left_arrow_status = "normal";
	}
}

std::string ValueBox::GetValueString()
{
	return std::to_string( cur_value );
}

void ValueBox::SetValue(int cur_value)
{
	this->cur_value = cur_value;
}

void ValueBox::RenderUI()
{
	surface_shader->BindShader();
	surface_shader->RenderStatic();
	surface_shader->UnbindShader();
	left_triangle->BindShader();
	left_triangle->RenderStatic();
	left_triangle->UnbindShader();
	right_triangle->BindShader();
	right_triangle->RenderStatic();
	right_triangle->UnbindShader();
	gl_text_label.RenderText();
	for (int i = 0; i < cur_value; i++) {
		box_shaders.at(i)->BindShader();
		box_shaders.at(i)->RenderStatic();
		box_shaders.at(i)->UnbindShader();
	}
}