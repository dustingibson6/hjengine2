#ifndef BUTTON_H
#define BUTTON_H

#include "ui.h"
#include "gltext.h"
#include "surface_shader.h"
#include "color.h"
#include "object.h"

class Button : public UI {
public:
	Button();
	Button(CPoint pnt, std::map<std::string, Definable> definable, ObjectFactory* object_factory);
	void RenderUI();
	void PollInput();
	void Reset();
	void Sizing();
private:
	std::string status;
	std::string font;
	std::string label;
	CPoint max_size;
	int pnt_size;
	GLText gl_text;
	GLText gl_text_hover;
	Mix_Chunk* hover_sound;
	SurfaceShader* surface_shader;
	Color font_color;
	Color bg_colors;
	Color border_colors;
	GLfloat hover_constant;
};


#endif