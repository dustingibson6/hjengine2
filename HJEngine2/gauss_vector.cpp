#include "gauss_vector.h"

ProbValue::ProbValue(float lower, float upper, float value, float prob)
{
	this->lower = lower;
	this->upper = upper;
	this->value = value;
	this->prob = prob;
}

float GaussVector::ComputeGauss(float x, float mean, float sd)
{
	return ((1 / sqrt(2 * pow(sd,2) * 3.14))*exp((-1 * pow((x - mean),2)) / ((2 * (pow(sd,2))))));
}

GaussVector::GaussVector()
{

}

float GaussVector::IntegrateGauss(float lower, float upper, float mean, float sd)
{
	float delta = (upper - lower) / 1000;
	float sum = 0;
	for (float i = lower; i <= upper; i += delta) {
		sum += delta * ComputeGauss(i, mean, sd);
	}
	return sum;
}

GaussVector::GaussVector(float lower, float upper, float mean, float sd)
{
	prob_values = std::vector<ProbValue>();
	values = std::vector<float>();
	float lower_most = mean - (sd*3.0);
	float upper_most = mean + (sd*3.0);
	float delta = (upper_most - lower_most) / 100.0;
	float sum = 0.0;
	prob_delta = 100000;
	float sum_check = 0.0;
	for (float i = lower_most; i < upper_most; i += delta) {
		float prob = IntegrateGauss(i, (i+delta), mean, sd);
		sum_check += prob;
		float expected = floor(prob_delta * prob);
		prob_values.push_back(ProbValue(sum, sum+expected, i, prob));
		sum += expected;
	}
	prob_delta = sum;
	distribution = std::uniform_int_distribution<int>(1, prob_delta);
}

float GaussVector::FindValue(int cur_num)
{
	for (int i = 0; i < prob_values.size(); i++) {
		if (cur_num >= prob_values[i].lower && cur_num <= prob_values[i].upper)
			return prob_values[i].value;
	}
}

void GaussVector::Insert(float value)
{
	int cur_num = (rand() % prob_delta) + 1;
	values.push_back(value);
}

float GaussVector::ReturnValue()
{
	int cur_num = distribution(random_generator);
	return FindValue(cur_num);
}