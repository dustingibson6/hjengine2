#include "config.h"

Config::Config(std::string fname)
{
	this->fname = fname;
	this->values = new std::map<std::string,std::string>();
	Reload();
}

Config::Config()
{

}

void Config::Reload()
{
	rapidxml::xml_document<> doc;
	rapidxml::xml_node<>* root_node;
	std::ifstream xml_file("res/config/" + fname + ".xml");
	std::vector<char> buffer((std::istreambuf_iterator<char>(xml_file)), std::istreambuf_iterator<char>());
	buffer.push_back('\0');
	doc.parse<0>(&buffer[0]);
	root_node = doc.first_node("config");
	for (rapidxml::xml_node<>* setting_node = root_node->first_node("setting"); setting_node; setting_node = setting_node->next_sibling()) {
		std::string key = setting_node->first_attribute("key")->value();
		std::string value = setting_node->first_attribute("value")->value();
		this->values->insert(std::pair<std::string,std::string>(key,value));
	}
	xml_file.close();
}

void Config::SaveConfig()
{
	rapidxml::xml_document<> doc;
	rapidxml::xml_node<>* root_node;
	std::ifstream xml_file("res/config/" + fname + ".xml");
	std::vector<char> buffer((std::istreambuf_iterator<char>(xml_file)), std::istreambuf_iterator<char>());
	buffer.push_back('\0');
	doc.parse<0>(&buffer[0]);
	root_node = doc.first_node("config");
	for (rapidxml::xml_node<>* setting_node = root_node->first_node("setting"); setting_node; setting_node = setting_node->next_sibling()) {
		std::string key = setting_node->first_attribute("key")->value();
		setting_node->first_attribute("value")->value(values->at(key).c_str());
	}
	//xml_file.close();
	std::ofstream fileWrite;
	fileWrite.open("res/config/" + fname + ".xml");
	fileWrite << doc;
	fileWrite.close();
}

CPoint Config::GetCPointValue(std::string key)
{
	std::string value = GetValue(key);
	std::vector<std::string> values = std::vector<std::string>();
	boost::split(values, value, boost::is_any_of(","));
	return CPoint(atoi(values.at(0).c_str()), atoi(values.at(1).c_str()));
}

bool Config::GetBoolValue(std::string key)
{
	if (GetValue(key) == "true")
		return true;
	return false;
}

std::string Config::GetValue(std::string key)
{
	return values->at(key);
}

int Config::GetValueInt(std::string key)
{
	return atoi(values->at(key).c_str());
}

void Config::SetValue(std::string key, std::string value)
{
	values->at(key) = value;
}