#ifndef HIMAGE_H
#define HIMAGE_H

#include <stdio.h>
#include <iostream>
#include <SDL.h>
#include <SDL_image.h>
#include "texture_group.h"

class Image {
public:
	Image(TextureGroup*);
	TextureGroup* GetTextureGroup();
	int GetWidth();
	int GetHeight();

private:
	TextureGroup* image;
	int width;
	int height;
};

#endif