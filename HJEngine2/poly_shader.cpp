#include "poly_shader.h"

PolyShader::PolyShader(std::string fname) : Shader( fname )
{
	VAO = 0;
	VBO[0] = 0;
	VBO[1] = 0;
	IBO = 0;
	vertexPosLocation = -1;
	texCoordLocation = -1;
	proMatrixLocation = -1;
	modMatrixLocation = -1;
	texUnitLocation = -1;
	angleLocation = -1;
	vertexPosLocation = glGetAttribLocation( programID, "LVertexPos2D");
	texCoordLocation = glGetAttribLocation( programID, "LTexCoord");
	texUnitLocation = glGetUniformLocation( programID, "LTextureUnit");
	transVectorLocation = glGetUniformLocation( programID, "LTransVector");
	angleLocation = glGetUniformLocation(programID, "angle");
	res_location = glGetUniformLocation(programID, "size");
	//modMatrixLocation = glGetUniformLocation( programID, "rotation_matrix");
	//glUseProgram( programID );
	//glUniformMatrix4fv( proMatrixLocation, 1, GL_FALSE, glm::value_ptr(proj));
	//glUseProgram( 0 );
	//SetModMatrix( glm::mat4() );
}

PolyShader::~PolyShader()
{
	glDeleteBuffers(1,&VBO[0]);
	glDeleteBuffers(1, &VBO[1]);
	glDeleteBuffers(1,&IBO);
	glDeleteVertexArrays(1,&VAO);
}

void PolyShader::RenderStatic()
{
	glBindVertexArray( VAO );
	glDrawElements( GL_QUADS, num_vertex*4, GL_UNSIGNED_INT, NULL );
	glBindVertexArray(0);
}

void PolyShader::LoadArrayData( GLuint* index_data, GLfloat* vertex_data, GLfloat* clip_data, int index_size, int vertex_size, int clip_size)
{
	num_vertex = vertex_size;
	num_index = index_size;
	num_clip = clip_size;

	if( VAO == 0 )
		glGenVertexArrays(1, &VAO);

	glBindVertexArray(VAO);

	if( VBO[0] == 0 )
		glGenBuffers(2, VBO );

	glBindBuffer( GL_ARRAY_BUFFER, VBO[0] );
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*vertex_size, vertex_data, GL_STATIC_DRAW);
	glVertexAttribPointer(vertexPosLocation, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray( vertexPosLocation );

	glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*clip_size, clip_data, GL_STATIC_DRAW);
	glVertexAttribPointer(texCoordLocation, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(texCoordLocation);


	if( IBO == 0 )
		glGenBuffers( 1, &IBO );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, IBO );
	glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint)*index_size, index_data, GL_STATIC_DRAW);

	glBindVertexArray( 0 );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glDisableVertexAttribArray(vertexPosLocation);
	glDisableVertexAttribArray(texCoordLocation);
}

void PolyShader::EnableVertexPointer()
{
	glEnableVertexAttribArray( vertexPosLocation );
}

void PolyShader::EnableCoordPointer()
{
	glEnableVertexAttribArray( texCoordLocation );
}

void PolyShader::SetVertexPointer(GLsizei stride, const GLvoid* data)
{
	glVertexAttribPointer( vertexPosLocation, 2, GL_FLOAT, GL_FALSE, stride, data);
}

void PolyShader::SetTexCoordPointer(GLsizei stride, const GLvoid* data)
{
	glVertexAttribPointer( texCoordLocation, 2, GL_FLOAT, GL_FALSE, stride, data);
}

void PolyShader::UnsetCoordPointer()
{
	glDisableVertexAttribArray(texCoordLocation);
}

void PolyShader::UnsetVertexPointer()
{
	glDisableVertexAttribArray(vertexPosLocation);
}

void PolyShader::SetTexture(GLint texID)
{
	glUniform1i( texUnitLocation, texID);
}

void PolyShader::SetTransVector(GLfloat xv, GLfloat yv)
{
	glUniform4f( transVectorLocation, xv, yv, 0.0f, 0.0f );
}

void PolyShader::SetAngle(GLfloat angle)
{
	glUniform1f(angleLocation, angle);
}

void PolyShader::SetRes(GLfloat w, GLfloat h)
{
	glUniform2f(res_location, w, h);
}