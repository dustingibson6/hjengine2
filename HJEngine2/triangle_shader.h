#pragma once
#include <stdio.h>
#include <iostream>
#include <String>
#include <GL/glew.h>
#include "poly_shader.h"
#include "globals.h"
#include "surface_shader.h"
#include "color.h"

class TriangleShader : public PolyShader {
public:
	TriangleShader(std::string fname, Color color, CPoint pnt, CPoint size, std::string side);
	~TriangleShader();
	void BuildArrayData();
	void ChangePosition(int x, int y);
	void ChangePositionAndSize(int x, int y, int w, int h);
	void ChangeSize(int w, int h);
	void ChangeColors(Color new_colors);
	void Sizing();
private:
	int x;
	int y;
	int w;
	int h;
	std::string side;
	Color color;
	GLuint VBO;
	GLuint VAO;
	GLuint tex_color_location;
	GLuint vertex_location;
};