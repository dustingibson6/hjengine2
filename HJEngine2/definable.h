#ifndef DEFINABLE_H
#define DEFINABLE_H

#include <string>
#include <vector>
#include <map>
#include <GL/glew.h>
#include "point.h"
#include "color.h"
#include <boost/algorithm/string.hpp>

class Definable
{
public:
	Definable(std::string value);
	Definable(int value);
	Definable(CPoint value);
	Definable(Color color);
	Definable(GLfloat value);
	void AddToDefinables(std::string name,  std::map<std::string, Definable> &definables  );
	void PutInt(int v);
	void PutCPoint(CPoint v);
	void PutColor(Color v);
	void PutString(std::string v);
	void PutGLfloat(GLfloat v);
	int ToInt();
	CPoint ToCPoint();
	std::string ToString();
	Color ToColor();
	GLfloat ToGLfloat();

private:
	std::string value;
};

#endif 