#pragma once

#ifndef VALUE_BOX_H
#define VALUE_BOX_H

#include "ui.h"
#include "gltext.h"
#include "surface_shader.h"
#include "triangle_shader.h"
#include "definable.h"
#include "color.h"
#include "object.h"
#include <math.h>

class ValueBox : public UI
{
public:
	ValueBox();
	ValueBox(CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_Factory);
	void Sizing();
	void RenderUI();
	void PollInput();
	std::string GetValueString();
	void SetValue(int cur_value);
	int choice_index;
private:
	std::string text;
	std::string status;
	std::string left_arrow_status;
	std::string right_arrow_status;
	std::string font;
	CPoint text_pnt;
	CPoint left_arrow_pnt;
	CPoint arrow_dim;
	CPoint right_arrow_pnt;
	CPoint surface_dim;
	CPoint surface_pnt;
	int font_label_size;
	int border_size;
	GLText gl_text_label;
	SurfaceShader* surface_shader;
	std::vector<SurfaceShader*> box_shaders;
	TriangleShader* left_triangle;
	TriangleShader* right_triangle;
	GLfloat hover_constant;
	Color font_color;
	Color bg_color;
	Color border_color;
	int max_value;
	int cur_value;
};

#endif SELECT_BOX_H