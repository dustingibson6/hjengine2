#include "states.h"

StateMachine::StateMachine(std::string init_state_name, std::string init_state_type)
{
	font = TTF_OpenFont("res\\fonts\\Calibri.ttf",18 );
	//TTF_CloseFont( font );
	AddState(init_state_name, init_state_type);
}

void StateMachine::AddState(std::string name, std::string type)
{
	if( type == "game")
		current_state = new GameState(name,type);
	else if(type == "menu")
		current_state = new MenuState(name,type);
	current_state->status = "starting";
	fps_label = GLText("system", std::to_string(Globals::GetInstance().fps), CPoint(0, 0), CPoint(100, 100), 18, Color(255, 0, 0, 255));
}

void StateMachine::TransitionState(std::string new_state)
{
	AddState(new_state, "game");
}

void StateMachine::ControlStates(Game* game)
{
	std::string current_state_name = current_state->GetName();
	//game->CheckPlayerCollision();
	game->player->ResetVelocity();
	if( current_state_name == "main menu") {
		if (current_state->status == "starting") {
			game->menu_factory->ChangeActiveMenu("main menu");
			current_state->status = "started";
		}
		if (current_state->status == "started") {
			std::vector<std::string> current_event = game->menu_factory->PollMenuEvents();
			if (current_event[0] == "save" ) {
				Globals::GetInstance().reinit_flag = true;
				game->Reinit();
			}
			if (current_event[0] == "quit") {
				Globals::GetInstance().quit_flag = true;
			}
			if (current_event[0] == "change state") {
				TransitionState(current_event[1]);
			}
			game->menu_factory->DrawMenu();
		}
	}
	if( current_state_name == "main") {
		current_state->HandleInput(game, game->active_keys, game->active_mouse);
		game->AutoAdjustCamera();
		game->RenderScene();
		game->effect_factory->RunEffects();
		game->player->UpdateMovement();
		//game->gl_text_factory->RenderTexts();
		//SDL_Color font_color = {0,0,255};
		//SDL_Surface* font_surface = TTF_RenderText_Solid( font, "Calibri 12", font_color );
		//SDL_Rect font_rect = {100,100,0,0};
		//SDL_BlitSurface( font_surface, NULL, game->screen_surface,&font_rect);
		//SDL_FreeSurface( font_surface );
		game->ui_factory->RenderUIs();
	}
	else if( current_state_name == "close") {
	}
	if( Globals::GetInstance().update_text)
		fps_label.NewText(std::to_string(Globals::GetInstance().fps));
	fps_label.RenderText();
	//Draw UI

	//Debug Mode

}

State::State(std::string name, std::string type)
{
	this->name = name;
	this->type = type;
}

void State::TestFunct(Game* game)
{
}

void State::HandleInput(Game* game, std::vector<int>* active_keys, std::vector<int>* active_mouse)
{
}

void GameState::HandleInput(Game* game, std::vector<int>* active_keys, std::vector<int>* active_mouse)
{
	const Uint8* key_state = SDL_GetKeyboardState(NULL);
	if (key_state[SDL_SCANCODE_LEFT])
		game->player->Traverse(4);
	if (key_state[SDL_SCANCODE_RIGHT])
		game->player->Traverse(2);
	if (key_state[SDL_SCANCODE_UP])
		game->player->Traverse(1);
	if (key_state[SDL_SCANCODE_DOWN])
		game->player->Traverse(3);
	active_keys->clear();
}

void MenuState::HandleInput(Game *game,std::vector<int>* active_keys, std::vector<int>* active_mouse)
{
	//game->menu_factory->PollActiveMenuItem(game->mouse_x,game->mouse_y,game->mouse_clicked);
}

std::string State::GetName()
{
	return name;
}

GameState::GameState(std::string name, std::string type)
	: State(name, type)
{

}

MenuState::MenuState(std::string name, std::string type)
	:State(name, type)
{

}