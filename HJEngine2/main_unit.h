#ifndef MAIN_UNIT_H
#define MAIN_UNIT_H

#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <GL/glew.h>
#include <glm.hpp>
#include "globals.h"
#include "point.h"
#include "texture_group.h"

class UnitPart {
public:
	UnitPart();
	int height;
	int width;
	TextureGroup texture;
	CPoint pos;
};

class UnitBody {
public:
	UnitBody();
	UnitPart left_leg;
	UnitPart right_leg;
	UnitPart left_arm;
	UnitPart right_arm;
	UnitPart head;
	UnitPart body;
	int height;
	int width;
};

class MainUnit {
};

#endif