#include "game.h"

Game::Game(std::string config)
{
	Globals::GetInstance().config_name = config;
	Config main_config = Config(Globals::GetInstance().config_name);
	//int screen_width = main_config.GetCPointValue("resolution").x;
	//int screen_height = main_config.GetCPointValue("resolution").y;
	Globals::GetInstance().screen_width = main_config.GetCPointValue("resolution").x;
	Globals::GetInstance().screen_height = main_config.GetCPointValue("resolution").y;
	Globals::GetInstance().screen_ratx = 1.0f;
	Globals::GetInstance().screen_raty = 1.0f;
	this->use_opengl = true;
	this->full_screen = main_config.GetBoolValue("fullscreen");
	this->window_width = Globals::GetInstance().screen_width;
	this->window_height = Globals::GetInstance().screen_height;
	this->sdl_window = NULL;
	this->screen_surface = NULL;
	//this->display = NULL;
	this->redraw = false;
	std::string fps_value = main_config.GetValue("cap");
	this->FPS = 144;
	if (fps_value != "none") {
		this->FPS = atoi(fps_value.c_str());
	}
	this->vsync = main_config.GetBoolValue("vsync");
	this->sdl_render = NULL;
	this->reinit_flag = false;
	//InitDisplay();
	SetUpOpenGL();
	gl_text_factory = new GLTextFactory();
	//gl_text_factory->AddText("test_text","Calibri","TESTING TESTING",0,0,1000,1000,20);

	std::string map_fname = "demo.hjm";

	if ( main_config.GetBoolValue("demo") ) {
		map_fname = main_config.GetValue("demo_map");
	}

	this->tile_map = new TileMap(map_fname);
	tile_map->tile_world->SetContactListener(this);
	this->player = new Player(this->tile_map->tile_world,this->tile_map->GetPlayer());
	this->effect_factory = new EffectFactory();
	if (main_config.GetBoolValue("demo")) 
		this->state_machine = new StateMachine("main", "game");
	else
		this->state_machine = new StateMachine("main menu","menu");
	this->active_keys = new std::vector<int>();
	this->active_mouse = new std::vector<int>();
	this->menu_factory = new MenuFactory();
	this->menu_factory->AddFromXML("main");
	//this->menu_factory->SetMenu("main menu");

	mouse_clicked = false;
	ui_factory = new UIFactory();
	int font_color[4] = {0,255,0,255};
	int bg_color[4] = {255,255,255,128};
	//console = Console( CPoint(0,0), CPoint( 300, 500) );
	ui_factory->AddConsole("debug console",CPoint(10,10),CPoint(300,500));
	//ui_factory->AddTextInput( "console",CPoint(0,0),CPoint(100,100),18,font_color,bg_color);
	effect_factory->AddFadeEffect("test",window_width,window_height,1);
}

Game::~Game()
{
	//delete sdl_window;
	delete screen_surface;
	delete sdl_render;
	delete effect_factory;
}

void Game::BeginContact(b2Contact* contact)
{
	b2Body* collide_body_a = contact->GetFixtureA()->GetBody();
	b2Body* collide_body_b = contact->GetFixtureB()->GetBody();
	
	if( collide_body_a == player->b2d_body || collide_body_b == player->b2d_body) {
		player->jump_triggered = false;
	}
	B2_NOT_USED(contact);
}

void Game::EndContact(b2Contact* contact)
{
	B2_NOT_USED(contact);
}

int Game::InitDisplay()
{
	return 0;
}

void Game::GameLogic()
{
	if(state == "start") {
	}
}

void Game::CheckPlayerCollision()
{

}

void Game::RenderScene()
{
	tile_map->RenderScene(window_width,window_height);
	player->RenderPlayer(tile_map->GetCamera());
}

void Game::AutoAdjustCamera()
{
	float w = player->width;
	float h = player->height;
	float bx = player->b2d_body->GetPosition().x;
	float by = player->b2d_body->GetPosition().y;
	CPoint ply_point = Globals::GetInstance().ToPixels(bx,by,w,h);
	//ply_point.y = window_height - ply_point.y - player->height;
	float c_x = (float) ply_point.x / Globals::GetInstance().res_width;
    float c_y = (float) ply_point.y / Globals::GetInstance().res_height;
	float dx = 0.5 - c_x - tile_map->GetCamera().x;
	float dy = 0.5 - c_y - tile_map->GetCamera().y;
	tile_map->AdjustCamera(dx,dy);
}

GLuint Game::setupGLTest()
{
	ILuint img_id = 0;
	GLuint bytes;
	GLuint* pixels;
	ilGenImages(1,&img_id);
	ilBindImage(img_id);
	ILboolean s = ilLoadImage("res/test.png");
	ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
	pixels = (GLuint*)ilGetData();
	GLuint width = (GLuint)ilGetInteger(IL_IMAGE_WIDTH);
	GLuint height = (GLuint)ilGetInteger(IL_IMAGE_HEIGHT);
	glGenTextures(1,&bytes);
	glBindTexture(GL_TEXTURE_2D,bytes);
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,width,height,0,GL_RGBA,GL_UNSIGNED_BYTE,pixels);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D,NULL);
	return bytes;
}

void Game::SetUpOpenGL()
{
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION,3);
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION,1);
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );
	if( SDL_Init( SDL_INIT_VIDEO) < 0 )
	{
		Globals::GetInstance().Log("Error init widow" + (std::string)SDL_GetError());
	}
	else
	{
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
		InitScreen();
	}
	TTF_Init();
	Globals::GetInstance().res_width = 1280;
	Globals::GetInstance().res_height = 700;
	glViewport(0,0, Globals::GetInstance().screen_width, Globals::GetInstance().screen_height);
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
	{
		Globals::GetInstance().Log( "Error occurted intializing projection matrix! " );
	}
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024) == -1)
	{
		Globals::GetInstance().Log("Failed to init audio: " + (std::string) Mix_GetError());
	}
	ilInit();
	ilClearColour(255,255,255,0);
	//ilOriginFunc(IL_ORIGIN_SET);
	ilEnable(IL_ORIGIN_SET);
	ILenum ilError = ilGetError();
	if( ilError != IL_NO_ERROR)
		Globals::GetInstance().Log("Error with Devil Library " + std::to_string(ilError));
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void Game::InitScreen()
{
	Globals::GetInstance().sdl_window = SDL_CreateWindow("test", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, window_width, window_height, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_BORDERLESS | SDL_WINDOW_MOUSE_FOCUS );
	if (Globals::GetInstance().sdl_window == NULL)
	{
		Globals::GetInstance().Log("Window cannot be created " + (std::string)SDL_GetError() );
	}
	else
	{
		sdl_context = SDL_GL_CreateContext(Globals::GetInstance().sdl_window);
		if (sdl_context == NULL)
			Globals::GetInstance().Log("Error creating context "  + (std::string)SDL_GetError());
		else
		{
			if (this->full_screen) 
				if (SDL_SetWindowFullscreen(Globals::GetInstance().sdl_window, SDL_WINDOW_FULLSCREEN) < 0)
					Globals::GetInstance().Log("Error setting fullscreen " + (std::string)SDL_GetError());
			glewInit();
			if (SDL_GL_SetSwapInterval(this->vsync) < 0)
				Globals::GetInstance().Log("Error setting vsync " + (std::string)SDL_GetError());
		}
	}
	//screen_surface = SDL_GetWindowSurface(Globals::GetInstance().sdl_window);
}


void Game::Reinit()
{
	//InitDisplay();
	Config main_config = Config(Globals::GetInstance().config_name);
	int new_width = main_config.GetCPointValue("resolution").x;
	int new_height = main_config.GetCPointValue("resolution").y;
	GLfloat test_x = (float)new_width / (float)Globals::GetInstance().screen_width;
	Globals::GetInstance().screen_ratx = (float) new_width / (float) Globals::GetInstance().screen_width;
	Globals::GetInstance().screen_raty = (float) new_height / (float) Globals::GetInstance().screen_height;
	Globals::GetInstance().screen_width = new_width;
	Globals::GetInstance().screen_height = new_height;
	this->FPS = atoi(main_config.GetValue("cap").c_str());
	//Set Resolution
	SDL_SetWindowSize(Globals::GetInstance().sdl_window, Globals::GetInstance().screen_width, Globals::GetInstance().screen_height);
	this->full_screen = main_config.GetBoolValue("fullscreen");
	this->vsync = main_config.GetBoolValue("vsync");
	//Vsync Enabled?
	if (SDL_GL_SetSwapInterval(this->vsync) < 0)
		Globals::GetInstance().Log("Error setting vsync " + (std::string)SDL_GetError());
	//Fullscreen Enabled
	if (this->full_screen) {
		SDL_SetWindowFullscreen(Globals::GetInstance().sdl_window, 0);
		SDL_Delay(5000);
		if (SDL_SetWindowFullscreen(Globals::GetInstance().sdl_window, SDL_WINDOW_FULLSCREEN) < 0)
			Globals::GetInstance().Log("Error setting fullscreen " + (std::string)SDL_GetError());
	}
	else {
		SDL_SetWindowFullscreen(Globals::GetInstance().sdl_window, 0);
	}
	SDL_SetWindowPosition(Globals::GetInstance().sdl_window, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
	glViewport(0, 0, Globals::GetInstance().screen_width, Globals::GetInstance().screen_height);
	//SDL_GL_CreateContext(Globals::GetInstance().sdl_window);
}

void Game::TestScale(SDL_Renderer* sdl_render)
{
	int* pixels = (int*)this->screen_surface->pixels;
	int color =  SDL_MapRGBA(this->screen_surface->format,0,0xFF,0,0);
	int pixel_count = (screen_surface->pitch/4)*screen_surface->h;
	for(int i=0; i < pixel_count; i++) {
		pixels[i] = color;
	}
}

void Game::AddKeyDownEvent(int key)
{
	active_keys->push_back(key);
}

int Game::GameLoop()
{
	int fps = 60;
	int ticks = 0;
	SDL_Event sdl_event;
	int auto_step_test = 0;
	int auto_step_dir = 200;
	Globals::GetInstance().ticks = SDL_GetTicks();
	int delay = 0;
	SDL_Delay(2000);
	GLuint test = setupGLTest();
	Globals::GetInstance().shift = false;
	int start = 0;
	int end = 0;
	int sec_timer = SDL_GetTicks();
	Globals::GetInstance().fps = this->FPS;
	while(1)
	{
			start = SDL_GetTicks();
			Globals::GetInstance().frame++;
			Globals::GetInstance().click = false;
			this->tile_map->tile_world->Step( 1.0f/(float)fps, 8, 3);
			while( SDL_PollEvent(&sdl_event) != 0) {
				if(sdl_event.type == SDL_QUIT) {
					return 1;
				}
				if(sdl_event.type == SDL_KEYDOWN) {
					int mod = Globals::GetInstance().key_mod;
					int scancode = sdl_event.key.keysym.scancode;
					int asciicode = sdl_event.key.keysym.sym;
					if (scancode == SDL_SCANCODE_ESCAPE)
						return 0;
					if( scancode == SDL_SCANCODE_LSHIFT || scancode == SDL_SCANCODE_RSHIFT ) {
						Globals::GetInstance().shift = true;
						Globals::GetInstance().key_mod = 32;
					}
					if( asciicode == 13 )
						Globals::GetInstance().enter = true;
					if( asciicode == 8 || (asciicode >= 32 && asciicode <= 126) ) {
						if( Globals::GetInstance().shift ) {
							if( asciicode >= 97 && asciicode <= 122 )
								Globals::GetInstance().key_ascii = sdl_event.key.keysym.sym - mod;
							else if( asciicode >= 48 && asciicode <= 57 ) {
								int new_ascii = (48-asciicode)*-1;
								Globals::GetInstance().key_ascii = Globals::GetInstance().num_mod[ new_ascii ];
							}
							else if( asciicode >= 91 && asciicode <= 93 ) {
								Globals::GetInstance().key_ascii = asciicode + 32;
							}
							else if( asciicode == int('-') )
								Globals::GetInstance().key_ascii = int('_');
							else if( asciicode == int('=') )
								Globals::GetInstance().key_ascii = int('+');
							else if( asciicode == int(';') )
								Globals::GetInstance().key_ascii = int(':');
							else if( asciicode == int('\'') )
								Globals::GetInstance().key_ascii = int('"');
							else if( asciicode == int(',') )
								Globals::GetInstance().key_ascii = int('<');
							else if( asciicode == int('.') )
								Globals::GetInstance().key_ascii = int('>');
							else if( asciicode == int('/') )
								Globals::GetInstance().key_ascii = int('?');
							else if( asciicode == int('`') )
								Globals::GetInstance().key_ascii = int('~');
						}
						else
							Globals::GetInstance().key_ascii = sdl_event.key.keysym.sym;
					}
					//AddKeyDownEvent( sdl_event.key.keysym.sym );
				}
				if(sdl_event.type == SDL_KEYUP) {
					if( sdl_event.key.keysym.scancode == SDL_SCANCODE_LSHIFT || sdl_event.key.keysym.scancode == SDL_SCANCODE_RSHIFT ) {
						Globals::GetInstance().shift = false;
						Globals::GetInstance().key_mod = 0;
					}
				}
				if(sdl_event.type == SDL_MOUSEMOTION) {
					mouse_x = sdl_event.motion.x;
					mouse_y = sdl_event.motion.y;
				}
				if(sdl_event.type == SDL_MOUSEBUTTONUP) {
					if(mouse_down) {
						Globals::GetInstance().click = true;
						mouse_down = false;
					}
				}
				if(sdl_event.type == SDL_MOUSEBUTTONDOWN)
					mouse_down = true;
				else {
					mouse_down = false;
				}
			}
			if(!use_opengl) {
				SDL_SetRenderDrawColor( sdl_render, 0xFF, 0xFF, 0xFF, 0xFF );
				SDL_RenderClear( sdl_render );
				state_machine->ControlStates(this);
				SDL_RenderPresent( sdl_render);
			} else {
				glClear( GL_COLOR_BUFFER_BIT);
				state_machine->ControlStates(this);
				SDL_GL_SwapWindow(Globals::GetInstance().sdl_window);
				Globals::GetInstance().key_ascii = 0;
			}
			end = SDL_GetTicks();
		int diff = SDL_GetTicks() - start;
		Globals::GetInstance().update_text = false;
		if( 1000 < (SDL_GetTicks() - sec_timer)) {
			int cur_fps = Globals::GetInstance().frame;
			Globals::GetInstance().update_text = true;
			//Globals::GetInstance().fps = Globals::GetInstance().frame;
			Globals::GetInstance().frame = 0;
			sec_timer = SDL_GetTicks();
		}
		if (diff < (1000.0 / this->FPS)) {
			double delay = (1000.0 / this->FPS) - diff;
			SDL_Delay(delay);
		}
		float diff_sec = ((SDL_GetTicks() - start)*0.001);
		float cur_fps = 1.0 / diff_sec ;
		Globals::GetInstance().sec = diff_sec;
		Globals::GetInstance().fps = cur_fps;
		Globals::GetInstance().reinit_flag = false;
		Globals::GetInstance().enter = false;
		if (Globals::GetInstance().quit_flag)
			return 1;
	}
	return 1;
}