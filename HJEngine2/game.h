#ifndef GAME_H
#define GAME_H

#include <cstdint>
#include <stdio.h>
#include <Box2D/Box2D.h>
#include <SDL.h>
#include <SDL_keyboard.h>
#include <SDL_keycode.h>
#include <SDL_mixer.h>
#include <GL/glew.h>
#include <IL/il.h>
#include "tile_map.h"
#include "effects.h"
#include "player.h"
#include "states.h"
#include "globals.h"
#include "menu.h"
#include "gltext.h"
#include "ui_factory.h"
#include "console.h"

class StateMachine;

class Game : public b2ContactListener {

public:
	Game(std::string config);
	~Game();
	Player* player;
	MenuFactory* menu_factory;
	int InitDisplay();
	int GameLoop();
	std::vector<int>* active_keys;
	std::vector<int>* active_mouse;
	int window_width;
	int window_height;
	SDL_Renderer* sdl_render;
	void SetUpOpenGL();
	void OpenGLTest();
	void GameLogic();
	void RenderScene();
	void AutoAdjustCamera();
	void CheckPlayerCollision();
	void TestScale(SDL_Renderer* );
	void BeginContact(b2Contact* contact);
	void EndContact(b2Contact* contact);
	void Reinit();
	void InitScreen();
	GLuint  setupGLTest();
	int mouse_x;
	int mouse_y;
	bool mouse_down;
	bool mouse_clicked;
	bool use_opengl;
	bool reinit_flag;
	EffectFactory* effect_factory;
	SDL_Surface* screen_surface;
	GLTextFactory* gl_text_factory;
	UIFactory* ui_factory;
	Console console;
private:
	void AddKeyDownEvent(int);
	TileMap* tile_map;
	std::string state;
	bool redraw;
	bool vsync;
	bool full_screen;
	int FPS;
	SDL_GLContext sdl_context;
	SDL_Window* sdl_window;
	StateMachine* state_machine;
};

#endif