#include "shader.h"
#include "globals.h"

Shader::Shader(std::string fname)
{
	programID = glCreateProgram();
	vertex_shader = LoadShaderFromFile("shaders/" + fname + ".glvs",GL_VERTEX_SHADER);
	glAttachShader(programID, vertex_shader);
	frag_shader = LoadShaderFromFile("shaders/" + fname + ".glfs",GL_FRAGMENT_SHADER);
	glAttachShader( programID, frag_shader);
	glLinkProgram(programID);

	glDeleteShader(vertex_shader);
	glDeleteShader(frag_shader);

	GLint success = GL_TRUE;
	glGetProgramiv( programID, GL_LINK_STATUS, &success);

	if( success != GL_TRUE)
	{
		Globals::GetInstance().Log("Unable to create shader");
	}
	proMatrixLocation = glGetUniformLocation( programID, "LProjectionMatrix");
	modMatrixLocation = glGetUniformLocation( programID, "LModelViewMatrix");
	glUseProgram( programID );
	SetModMatrix( glm::mat4() );
	SetProMatrix( glm::ortho<GLfloat>(0.0,1.0,0.0,1.0,0.0,1.0) );
	glUseProgram( 0 );
}

Shader::~Shader()
{
	glDeleteProgram( programID );
}

GLuint Shader::LoadShaderFromFile(std::string fname, GLenum shaderType)
{
	GLuint shaderID=0;
	std::string shaderProgram = "";
	std::ifstream inputStream(fname, std::ifstream::in);
	if(inputStream.is_open()) {
		for( std::string line; std::getline( inputStream, line ); )
		{
			shaderProgram += line + '\n';
		}
		//shaderProgram.c
	}

	//shaderProgram.assign( ( std::istreambuf_iterator< char >( inputStream ) ), std::istreambuf_iterator< char >() );


	inputStream.close();
	shaderID = glCreateShader(shaderType);
	const GLchar* shaderSource = (const GLchar *)shaderProgram.c_str();
	glShaderSource( shaderID, 1, &shaderSource, NULL);
	glCompileShader( shaderID );
	GLint shaderCompiled = GL_FALSE;
	glGetShaderiv( shaderID, GL_COMPILE_STATUS, &shaderCompiled);
	if( shaderCompiled != GL_TRUE )
	{
		//shaderID = 0;
		Globals::GetInstance().Log("Unable to compile shader: " + fname);
		GLint max_length = 0;
		glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &max_length);
		std::vector<GLchar> info_log (max_length);
		glGetShaderInfoLog(shaderID, max_length, &max_length, &info_log[0]);
		std::string log_string(info_log.begin(), info_log.end());
		Globals::GetInstance().Log("Shader error for " + fname + " :" + log_string);
		glDeleteShader(shaderID);
	}
	return shaderID;
}

void Shader::BindShader()
{
	glUseProgram(programID);
	//GLenum error = glGetError();
	//if( error != GL_NO_ERROR)
	//	std::cout << "Errors!";
}

void Shader::SetProMatrix(glm::mat4 proj)
{
	glUniformMatrix4fv( proMatrixLocation, 1, GL_FALSE, glm::value_ptr(proj));
}

void Shader::SetModMatrix(glm::mat4 mod)
{
	glUniformMatrix4fv( modMatrixLocation, 1, GL_FALSE, glm::value_ptr(mod));
}


void Shader::UnbindShader()
{
	glUseProgram(0);
}