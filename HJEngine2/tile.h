#ifndef TILE_H
#define TILE_H

#include <bitset>
#include <Box2D/Box2D.h>
#include <iostream>
#include "point.h"
#include "globals.h"

class TileValue {
public:
	TileValue(b2World*, CPoint, unsigned char*);
	TileValue();
	CPoint sel;
	b2Body* body;
	int* col;
	bool view;
};

class HardTile {
public:
	HardTile(b2World*, FPoint, float, float, int*);
	~HardTile();
	b2Body* body;
	float true_width;
	float true_height;
	FPoint pnt;
};

#endif