#ifndef CONFIG_H
#define CONFIG_H

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <boost/algorithm/string.hpp>
#include "rapidxml.hpp"
#include "rapidxml_print.hpp"
#include "point.h"

class Config {
public:
	Config(std::string fname);
	Config();
	void SaveConfig();
	int GetValueInt(std::string key);
	std::string GetValue(std::string key);
	void SetValue(std::string key, std::string value);
	CPoint GetCPointValue(std::string key);
	bool GetBoolValue(std::string key);

private:
	void Reload();
	std::string fname;
	std::map<std::string,std::string>* values;
};


#endif CONFIG_H