#ifndef POLY_SHADER_H
#define POLY_SHADER_H

#include <stdio.h>
#include <iostream>
#include <GL/glew.h>
#include "shader.h"

class PolyShader : public Shader {
public:
	PolyShader(std::string);
	~PolyShader();
	void SetVertexPointer(GLsizei, const GLvoid*);
	void SetTexCoordPointer(GLsizei, const GLvoid*);
	void SetAngle(GLfloat angle);
	void UnsetVertexPointer();
	void UnsetCoordPointer();
	void EnableVertexPointer();
	void EnableCoordPointer();
	void SetTexture(GLint);
	void SetRes(GLfloat w, GLfloat h);
	void SetTransVector(GLfloat, GLfloat);
	void RenderStatic();
	void LoadArrayData( GLuint* index_data, GLfloat* vertex_data, GLfloat* clip_data, int index_size, int vertex_size, int clip_size);
	GLuint vertexPosLocation;
	GLuint texCoordLocation;
	GLuint texColorLocation;
	GLuint texUnitLocation;
	GLuint angleLocation;
	GLuint transVectorLocation;
	GLuint res_location;
	GLuint VBO[2];
	GLuint VAO;
	GLuint IBO;
	int num_vertex;
	int num_clip;
	int num_index;
};

#endif