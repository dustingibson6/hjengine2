#include "image_head.h"

ImageHead::ImageHead(CPoint pnt, std::map<std::string, Definable> definable, ObjectFactory* object_factory)
	: UI("image", pnt, definable, object_factory)
{
	this->size = definables.at("size").ToCPoint();
	//this->poly_shader = new PolyShader("texture");
	std::string image_name = definables.at("image name").ToString();
	current_texture = object_factory->GetObjectInstanceByName("menu")->GetTextureValue(image_name);
	current_texture->SetupPolyShader();
	LoadVertexData();
}

void ImageHead::Sizing()
{
	this->size.x = (float)this->size.x * Globals::GetInstance().screen_ratx;
	this->size.y = (float)this->size.y * Globals::GetInstance().screen_raty;
	x = (float)x * Globals::GetInstance().screen_ratx;
	y = (float)y * Globals::GetInstance().screen_raty;
	LoadVertexData();
}

void ImageHead::LoadVertexData()
{
	std::vector<GLfloat> vertex_data_vector = std::vector<GLfloat>();
	std::vector<GLuint> index_data_vector = std::vector<GLuint>();
	std::vector<GLfloat> clip_data_vector = std::vector<GLfloat>();

	float temp_y = Globals::GetInstance().screen_height - y - size.y;

	float r_x = (float)x / Globals::GetInstance().screen_width;
	float r_y = (float)temp_y / Globals::GetInstance().screen_height;
	float rw_r = (float)size.x / Globals::GetInstance().screen_width;
	float rh_r = (float)size.y / Globals::GetInstance().screen_height;

	vertex_data_vector.push_back(r_x);
	vertex_data_vector.push_back(r_y);
	clip_data_vector.push_back(0.0f);
	clip_data_vector.push_back(0.0f);

	vertex_data_vector.push_back(r_x + rw_r);
	vertex_data_vector.push_back(r_y);
	clip_data_vector.push_back(1.0f);
	clip_data_vector.push_back(0.0f);

	vertex_data_vector.push_back(r_x + rw_r);
	vertex_data_vector.push_back(r_y + rh_r);
	clip_data_vector.push_back(1.0f);
	clip_data_vector.push_back(1.0f);

	vertex_data_vector.push_back(r_x);
	vertex_data_vector.push_back(r_y + rh_r);
	clip_data_vector.push_back(0.0f);
	clip_data_vector.push_back(1.0f);

	index_data_vector.push_back(0);
	index_data_vector.push_back(1);
	index_data_vector.push_back(2);
	index_data_vector.push_back(3);

	GLuint* index_data = &index_data_vector[0];
	GLfloat* vertex_data = &vertex_data_vector[0];
	GLfloat* clip_data = &clip_data_vector[0];

	int index_data_size = sizeof(GLuint)*index_data_vector.size();
	int vertex_data_size = sizeof(GLfloat)*vertex_data_vector.size();

	current_texture->polyShader->LoadArrayData(index_data, vertex_data, clip_data, index_data_vector.size(), vertex_data_vector.size(), clip_data_vector.size());


}

void ImageHead::RenderUI()
{
	glBindTexture(GL_TEXTURE_2D, current_texture->GetTexture());
	current_texture->polyShader->BindShader();
	current_texture->polyShader->SetAngle(0.0f);
	current_texture->polyShader->SetTransVector(0.0f, 0.0f);
	current_texture->polyShader->RenderStatic();
	glBindTexture(GL_TEXTURE_2D, 0);
	current_texture->polyShader->UnbindShader();
}

