#ifndef LABEL_H
#define LABEL_H

#include "ui.h"
#include "gltext.h"
#include "color.h"
#include "object.h"

class Label : public UI {
public:
	Label();
	Label(CPoint pnt, std::map<std::string, Definable> definable, ObjectFactory* object_factory);
	void RenderUI();
	void Sizing();
	void PollInput();
private:
	std::string font;
	std::string text;
	int font_size;
	Color font_color;
	GLText gl_text;
};

#endif