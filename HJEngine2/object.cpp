#include "object.h"
#include "globals.h"

ObjectFactory::ObjectFactory()
{
	this->obj_instance_list = new std::vector<ObjectInstance*>();
	this->asset_map = new std::map<std::string,Asset>();
	this->object_templates = new std::map<std::string, ObjectTemplate>();
}

ObjectFactory::~ObjectFactory()
{
	delete asset_map;
	delete object_templates;
	delete obj_instance_list;
}

void ObjectFactory::AddObject(std::string name, std::map<std::string, Property*>* prop_map, CPoint pnt, int size)
{
	//Reserve player object instance
	if(name == "player")
		this->player_instance = new ObjectInstance(name,prop_map,pnt,size);
	else if(name == "globals")
		this->globals_instance = new ObjectInstance(name,prop_map,pnt,size);
	else
		obj_instance_list->insert(obj_instance_list->begin(), new ObjectInstance(name, prop_map, pnt, size));
}

ObjectInstance* ObjectFactory::GetObjectInstanceByName(std::string name)
{
	for (int i = 0; i < obj_instance_list->size(); i++) {
		if (obj_instance_list->at(i)->name == name)
			return obj_instance_list->at(i);
	}
	return NULL;
}

void ObjectFactory::AddTemplate(std::string name,std::map<std::string, Property>* prop_map,ImageStates image_states)
{
	ObjectTemplate obj_template = ObjectTemplate(name, prop_map, image_states);
	this->object_templates->insert(std::pair<std::string,ObjectTemplate>(name,obj_template));
}

ObjectInstance* ObjectFactory::GetInstanceAtIndex(int index)
{
	return obj_instance_list->at(index);
}

ObjectTemplate ObjectFactory::GetObjectTemplateFromName(std::string name)
{
	return object_templates->at(name);
}

int ObjectFactory::GetObjectTemplateSize()
{
	return object_templates->size();
}

int ObjectFactory::GetObjectInstanceSize()
{
	return obj_instance_list->size();
}

Image* ObjectFactory::GetImageFromInstance(ObjectInstance* obj_inst)
{
	return object_templates->at(obj_inst->name).GetImageFromInstance(obj_inst);
}

void ObjectFactory::AddAssetMap(std::string name,std::string data_type,int size,unsigned char* buffer)
{
	Asset asset = Asset(name,data_type,size,buffer);
	asset_map->insert(std::pair<std::string,Asset>(name,asset));
}

void ObjectFactory::RenderObjects(FPoint cam_pnt,int screen_width, int screen_height)
{
	for(int i=0; i < GetObjectInstanceSize(); i++) {
		ObjectInstance* cur_instance = obj_instance_list->at(i);
		if( cur_instance->state == "wait" && cur_instance->visible) {
			std::string obj_name = cur_instance->name;
			ObjectTemplate cur_template = object_templates->at(obj_name);
			Image* cur_image = GetImageFromInstance(cur_instance);
			cur_instance->RenderObject(cam_pnt,cur_image,screen_width,screen_height);
		}
	}
}

ObjectInstance::ObjectInstance(std::string name, std::map<std::string, Property*>* prop_map, CPoint pnt, int size)
{
	this->name = name;
	this->prop_map = prop_map;
	this->pnt = pnt;
	this->visible = visible;
	this->state = "wait";
	this->image_state = "default";
}

ObjectInstance::~ObjectInstance()
{
	delete prop_map;
}

void ObjectInstance::RenderObject(FPoint cam_pnt, Image* img, int screen_width, int screen_height)
{
	float x = (float) pnt.x;
	float y = (float) screen_height - pnt.y - img->GetHeight();
	float rx = (float) x / screen_width + cam_pnt.x;
	float ry = (float) y / screen_height - cam_pnt.y;
	float pw_r = (float) img->GetWidth() / screen_width;
	float ph_r = (float) img->GetHeight() / screen_height;
	glBindTexture(GL_TEXTURE_2D,img->GetTextureGroup()->GetTexture());
	glBegin( GL_QUADS );
		glTexCoord2f( 0.0f, 0.0f ); glVertex2f(rx, ry);
		glTexCoord2f( 1.0f, 0.0f ); glVertex2f(rx + pw_r, ry);
		glTexCoord2f( 1.0f, 1.0f ); glVertex2f(rx + pw_r, ry + ph_r);
		glTexCoord2f( 0.0f, 1.0f ); glVertex2f(rx, ry + ph_r);
	glEnd();
	glBindTexture(GL_TEXTURE_2D,NULL);
	//SDL_Rect text_rect = {pnt.x + cam_pnt.x,pnt.y + cam_pnt.y,img->GetWidth(),img->GetHeight()};
	//TODO: Decide which texture to render
	//SDL_Texture* cur_texture = img->GetTextureGroup()->GetRegTex();
	//SDL_RenderCopy(render,cur_texture,NULL,&text_rect);
}

Mix_Chunk* ObjectInstance::GetAudioValue(std::string name)
{
	return  this->prop_map->at(name)->audio_data;
}

std::string ObjectInstance::GetStringValue(std::string prop_name)
{
	return this->prop_map->at(prop_name)->string_data;
}

float ObjectInstance::GetFloatValue(std::string prop_name)
{
	return std::stof(this->prop_map->at(prop_name)->string_data);
}

int ObjectInstance::GetIntValue(std::string prop_name)
{
	return this->prop_map->at(prop_name)->integer_data;
}

TextureGroup* ObjectInstance::GetTextureValue(std::string prop_name)
{
	return this->prop_map->at(prop_name)->texture_data;
}

ObjectTemplate::ObjectTemplate(std::string name, std::map<std::string,Property>* prop_map,ImageStates image_states)
{
	this->prop_map = prop_map;
	this->image_states = image_states;
	this->name = name;
}

Image* ObjectTemplate::GetImageFromInstance(ObjectInstance* obj_inst)
{
	std::string state = obj_inst->image_state;
	return image_states.GetImage(state);
}

ObjectTemplate::~ObjectTemplate()
{
	//delete prop_map;
}

Property::Property(std::string name, std::string type, int size, unsigned char* value)
{
	this->name = name;
	this->data_type = type;
	this->size = size;
	this->value = value;
	ClassifyData();
}

Property::Property(const Property &prop)
{
	texture_data = new TextureGroup();
	audio_data = new Mix_Chunk();
	if (prop.data_type == "Bitmap")
		*texture_data = *prop.texture_data;
	if (prop.data_type == "Audio" && prop.audio_data != NULL)
		*audio_data = *prop.audio_data;
}

Property::Property(std::string name, std::string type, int use_asset, std::string asset_name, int size, unsigned char* value)
{
	this->data_type = type;
	this->name = name;
	this->value = value;
	this->size = size;
	this->use_asset = use_asset;
	if(this->use_asset == 0)
		this->asset_name = "";
	else
		this->asset_name = asset_name;
	ClassifyData();
}

void Property::ClassifyData()
{
	if(this->data_type == "Bitmap")
		this->texture_data = new TextureGroup(this->value,this->size);
		//this->texture_data = ToTexture(sdl_window,screen_render);
	if(this->data_type == "String")
		this->string_data = ToString();
	if(this->data_type == "Integer")
		this->integer_data = ToInteger();
	if(this->data_type == "Audio")
		this->audio_data = ToAudio();
}

Mix_Chunk* Property::ToAudio()
{
	if(this->size == 0)
		return NULL;
	SDL_RWops* audio_buffer = SDL_RWFromMem(this->value,this->size);
	Mix_Chunk* resulting_audio = Mix_LoadWAV_RW(audio_buffer,0);
	if (!resulting_audio)
		Globals::GetInstance().Log(Mix_GetError());
	return resulting_audio;
}

int Property::ToInteger()
{
	if( this->size == 0)
		return NULL;
	int resulting_int = 0;
	if(size == 4) {
		resulting_int = (resulting_int << 8) + this->value[3];
		resulting_int = (resulting_int << 8) + this->value[2];
		resulting_int = (resulting_int << 8) + this->value[1];
		resulting_int = (resulting_int << 8) + this->value[0];
		//int return_int = (int_buffer[3] << 24) | (int_buffer[2] << 16) | (int_buffer[1] << 8) | (int_buffer[0]);
		//return return_int;
	}
	else {
		resulting_int = (resulting_int << 8) + this->value[1];
		resulting_int = (resulting_int << 8) + this->value[0];
	}
	return resulting_int;
}

std::string Property::ToString()
{
	if( this->size == 0)
		return "";
	return std::string((char*)this->value);
}

Property::~Property()
{
	//delete value;
}

Asset::Asset(std::string name, std::string type, int size, unsigned char* value)
	: Property(name, type, size, value)
{

}

ImageStates::ImageStates()
{
	state_map = new std::map<std::string,Image*>();
}

ImageStates::~ImageStates()
{
	//delete state_map;
}

void ImageStates::AddState(std::string name, TextureGroup* image)
{
	int temp_texture_width, temp_texture_height;
	Image* tmp_image = new Image(image);
	std::pair<std::string, Image*>;
	state_map->insert(std::pair<std::string, Image*>(name,tmp_image));
}

Image* ImageStates::GetImage(std::string name)
{
	return state_map->at(name);
}