#include "definable.h"

Definable::Definable(std::string value)
{
	this->value = value;
}

Definable::Definable(int value)
{
	PutInt(value);
}

Definable::Definable(CPoint value)
{
	PutCPoint(value);
}

Definable::Definable(Color v)
{
	PutColor(v);
}

Definable::Definable(GLfloat v)
{
	PutGLfloat(v);
}

CPoint Definable::ToCPoint()
{
	std::vector<std::string> split_vector = std::vector<std::string>();
	boost::split(split_vector, value, boost::is_any_of(","));
	return CPoint(atoi(split_vector[0].c_str()),atoi(split_vector[1].c_str()));
}

void Definable::PutString(std::string v)
{
	this->value = v;
}

void Definable::AddToDefinables(std::string name, std::map<std::string, Definable> &definables)
{
	definables.insert(std::pair<std::string, Definable>(name, *this));
}

void Definable::PutInt(int v)
{
	this->value = std::to_string(v);
}

void Definable::PutColor(Color v)
{
	this->value = std::to_string(v.r) + "," + std::to_string(v.g) + "," + std::to_string(v.b) + "," + std::to_string(v.a);
}

void Definable::PutGLfloat(GLfloat v)
{
	this->value = std::to_string(v);
}

void Definable::PutCPoint(CPoint v)
{
	this->value = std::to_string(v.x) + "," + std::to_string(v.y);
}

int Definable::ToInt()
{
	return atoi(value.c_str());
}

std::string Definable::ToString()
{
	return value;
}

Color Definable::ToColor()
{
	return Color(value);
}

GLfloat Definable::ToGLfloat()
{
	return atof(value.c_str());
}