#pragma once

#ifndef SELECT_BOX_H
#define SELECT_BOX_H

#include "ui.h"
#include "gltext.h"
#include "surface_shader.h"
#include "triangle_shader.h"
#include "definable.h"
#include "color.h"
#include "object.h"
#include <math.h>


class SelectChoice
{
public:
	SelectChoice(std::string text, std::string value, bool selected);
	std::string text;
	std::string value;
	bool selected;
};


class SelectBox : public UI
{
public:
	SelectBox();
	SelectBox(CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory* object_Factory, std::vector<SelectChoice> choices);
	void Sizing();
	void RenderUI();
	void PollInput();
	void SetValue(std::string value);
	std::string GetValueString();
	CPoint GetValueCPoint();
	std::vector<SelectChoice> choices;
	int choice_index;
private:
	std::string text;
	std::string status;
	std::string left_arrow_status;
	std::string right_arrow_status;
	std::string font;
	CPoint text_pnt;
	CPoint left_arrow_pnt;
	CPoint arrow_dim;
	CPoint right_arrow_pnt;
	CPoint surface_dim;
	CPoint surface_pnt;
	int font_size;
	int font_label_size;
	int border_size;
	GLText gl_text_label;
	GLText gl_text_choice;
	SurfaceShader* surface_shader;
	TriangleShader* left_triangle;
	TriangleShader* right_triangle;
	GLfloat hover_constant;
	Color font_color;
	Color bg_color;
	Color border_color;
};

#endif SELECT_BOX_H