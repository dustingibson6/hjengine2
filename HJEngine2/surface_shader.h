#ifndef SURFACE_SHADER_H
#define SURFACE_SHADER_H

#include <stdio.h>
#include <iostream>
#include <String>
#include <GL/glew.h>
#include "poly_shader.h"
#include "globals.h"
#include "color.h"

class SurfaceShader : public PolyShader {
public:
	SurfaceShader( std::string fname, Color color, Color border_color, CPoint pnt, CPoint size, int border_size, float angle=0.0f);
	void BuildArrayData();
	void ChangePosition( int x, int y );
	void ChangePositionAndSize( int x, int y, int w, int h );
	void ChangeSize( int w, int h );
	void ChangeColors(Color new_color);
	void Sizing();
	//void SetRotationVector(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2);
	~SurfaceShader();
private:
	int x;
	int y;
	int w;
	int h;
	Color color;
	GLfloat angle;
	GLuint VBO;
	GLuint VAO;
	GLuint IBO;
	GLuint color_buffer_id;
	GLuint tex_color_location;
	GLuint vertex_location;
	GLuint border_size_location;
	GLuint center_location;
	GLuint border_colors_location;
	int border_size;
	FPoint center;
	GLfloat c_x;
	GLfloat c_y;
};

#endif