#ifndef TILESHEET_H
#define TILESHEET_H

#include <stdio.h>
#include <SDL.h>
#include <iostream>
#include <map>
#include "texture_group.h"
#include "point.h"

class TileSet {
public:
	TileSet(int,int,int,int);
	~TileSet();
	void AddTile(TextureGroup*,CPoint);
	int CountTileImages(CPoint);
	TextureGroup* GetBitmap(CPoint pnt);
	int tile_width;
	int tile_height;
private:
	std::map<CPoint,TextureGroup*>* tile_images;
	int row;
	int col;
	//CPoint crap;
};

#endif