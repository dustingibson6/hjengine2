#ifndef TEXTURE_VERTEX_DATA_H
#define TEXTURE_VERTEX_DATA_H

#include <glew.h>

class TexturePos
{
public:
	GLfloat x;
	GLfloat y;
};

class TextureVex
{
public:
	GLfloat x;
	GLfloat y;
};

class TextureVertexData
{
public:
	TexturePos texPos;
	TextureVex texCoord;
};


#endif