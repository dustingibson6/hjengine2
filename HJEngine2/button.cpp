#include "button.h"

Button::Button( CPoint pnt, std::map<std::string, Definable> definables, ObjectFactory * object_factory) :
	UI("button", pnt, definables, object_factory)
{
	this->max_size = definables.at("size").ToCPoint();
	this->hover_constant = definables.at("hover constant").ToGLfloat();
	this->label = definables.at("text").ToString();
	this->font = definables.at("font").ToString();
	this->pnt_size = definables.at("font size").ToInt();
	this->font_color = definables.at("font color").ToColor();
	gl_text = GLText(font, label, pnt, max_size, pnt_size, font_color);
	this->size = gl_text.GetCurrentDim();
	status = "normal";
	hover_sound = object_factory->GetObjectInstanceByName("menu")->GetAudioValue("hover_sound");
	//surface_shader = new SurfaceShader("surface", bg_colors, pnt, size, CPoint(3, 3));
}

void Button::Sizing()
{
	this->x = (float)this->x * Globals::GetInstance().screen_ratx;
	this->y = (float)this->y * Globals::GetInstance().screen_raty;
	this->max_size.x = (float)this->max_size.x * Globals::GetInstance().screen_ratx;
	this->max_size.y = (float)this->max_size.y * Globals::GetInstance().screen_raty;
	gl_text = GLText(font, label, CPoint(x,y), max_size, pnt_size, font_color);
	this->size = gl_text.GetCurrentDim();
	this->hover_constant = hover_constant;
	status = "normal";
}

void Button::PollInput()
{
	events.Reset();
	int mouse_x = 0;
	int mouse_y = 0;
	Color new_color = font_color.GetScaledColor(hover_constant);
	SDL_GetMouseState(&mouse_x, &mouse_y);
	if (mouse_x >= this->x && mouse_x <= this->x + this->size.x && mouse_y >= this->y && mouse_y <= this->y + this->size.y) {
		if (Globals::GetInstance().click) {
			events.click = true;
		}
		if( status == "normal")
			status = "hover start";
	}
	else {
		if (status == "hover")
			status = "hover out";
	}
	if (status == "hover start") {
		gl_text.ChangeColors(new_color);
		Mix_PlayChannel(-1, hover_sound, 0);
		status = "hover";
	}
	if (status == "hover out") {
		gl_text.ChangeColors(font_color);
		status = "normal";
	}
}

void Button::Reset()
{

}

void  Button::RenderUI()
{
	gl_text.RenderText();
}