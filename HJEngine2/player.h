#ifndef PLAYER_H
#define PLAYER_H

#include <stdio.h>
#include <SDL.h>
#include <map>
#include <string>
#include <vector>
#include <iostream>
#include <Box2D/Box2D.h>
#include "texture_group.h"
#include "point.h"
#include "object.h"
#include "globals.h"
#include "texture_group.h"
#include <cmath>

class Player {
public:	
	Player(b2World*,ObjectInstance*);
	CPoint Traverse(int,int);
	void Traverse(int);
	void RenderPlayer( FPoint camera_pnt );
	void SetVelocity(int,int);
	void ResetVelocity();
	void UpdateMovement();
	void SetForce(int);
	void SetVelocity(int);
	void SetBuffers(Property* cur_prop, CPoint pnt);
	void ReloadBuffers(Property* cur_prop, CPoint pnt);
	void SetPartsBuffer();
	void SetAllBuffers();
	FPoint GetPosition();
	CPoint pnt;
	CPoint ply_pnt;
	FPoint b2_pnt;
	int width;
	int height;
	float vx_c;
	float vy_c;
	float vx;
	float vy;
	int vy_cap;
	bool jump_triggered;
	std::vector<int> col_dir;
	std::vector<Property*> parts;
	Property* head;
	Property* left_arm;
	Property* right_arm;
	Property* body;
	Property* left_leg;
	Property* right_leg;
	b2Body* b2d_body;

private:
	

};

#endif