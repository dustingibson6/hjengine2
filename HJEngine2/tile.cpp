#include "tile.h"

TileValue::TileValue(b2World* tile_world, CPoint sel, unsigned char* col_assign) 
{
	this->sel = sel;
    //std::bitset<4> x(col_assign);
	col = new int[4];
	for(int i=0; i < 4; i++)
		col [i] = (int)col_assign[i];
	this->view = false;
}

TileValue::TileValue()
{
}

HardTile::HardTile(b2World* world, FPoint pnt, float width, float height, int* col)
{
	this->pnt = pnt;
	true_width = width * 0.1;
	true_height = height * 0.1;
	//float x = pnt.x * 0.1;
	//float y = pnt.y * -0.1;
	FPoint tile_pnt = Globals::GetInstance().ToMeters(pnt.x,pnt.y,width,height);

	b2BodyDef body_def;
	body_def.type = b2_staticBody;
	body_def.position.Set(tile_pnt.x, tile_pnt.y);
	body = world->CreateBody(&body_def);

	if (col[0])
	{
		b2FixtureDef box_fixture;
		box_fixture.density = 1;
		b2Vec2 vert[4];
		b2PolygonShape box_shape;
		vert[0].Set(-true_width / 2, true_height / 2 - 0.1);
		vert[1].Set(true_width / 2, true_height / 2 - 0.1);
		vert[2].Set(true_width / 2, true_height / 2 );
		vert[3].Set(-true_width / 2, true_height / 2 );
		box_shape.Set(vert, 4);
		box_fixture.shape = &box_shape;
		body->CreateFixture(&box_fixture);
	}
	if (col[1])
	{
		b2FixtureDef box_fixture;
		box_fixture.density = 1;
		b2Vec2 vert[4];
		b2PolygonShape box_shape;
		vert[0].Set(true_width / 2 - 0.1, -true_height / 2);
		vert[1].Set(true_width / 2, -true_height / 2);
		vert[2].Set(true_width / 2, true_height / 2);
		vert[3].Set(true_width / 2 - 0.1, true_height / 2);
		box_shape.Set(vert, 4);
		box_fixture.shape = &box_shape;
		body->CreateFixture(&box_fixture);
	}
	if (col[2])
	{
		b2FixtureDef box_fixture;
		box_fixture.density = 1;
		b2Vec2 vert[4];
		b2PolygonShape box_shape;
		vert[0].Set(-true_width / 2, -true_height / 2);
		vert[1].Set(true_width / 2, -true_height / 2);
		vert[2].Set(true_width / 2, -true_height / 2 + 0.1);
		vert[3].Set(-true_width / 2, -true_height / 2 + 0.1);
		box_shape.Set(vert, 4);
		box_fixture.shape = &box_shape;
		body->CreateFixture(&box_fixture);
	}
	if (col[3])
	{
		b2FixtureDef box_fixture;
		box_fixture.density = 1;
		b2Vec2 vert[4];
		b2PolygonShape box_shape;
		vert[0].Set(-true_width / 2, -true_height / 2);
		vert[1].Set(-true_width / 2 + 0.1, -true_height / 2);
		vert[2].Set(-true_width / 2 + 0.1, true_height / 2);
		vert[3].Set(-true_width / 2, true_height / 2);
		box_shape.Set(vert, 4);
		box_fixture.shape = &box_shape;
		body->CreateFixture(&box_fixture);
	}
	//box_shape.SetAsBox( true_width/2, true_height/2);
	//box_shape.SetAsBox(true_width / 2, true_height / 2, b2Vec2(tile_pnt.x, tile_pnt.y), 0);
}