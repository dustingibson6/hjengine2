﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace MapEditor
{
    public partial class AssetDialog : Form
    {
        public List<ObjectInstance> objInstList;
        public Dictionary<String, Asset> assetDict;
        public String name;
        public Byte[] dataBytes;

        public AssetDialog(String name, Dictionary<String,Asset> assetDict, List<ObjectInstance> objInstList)
        {
            this.objInstList = objInstList;
            this.assetDict = assetDict;
            this.name = name;
            InitializeComponent();
            updateForm();
            nameText.Enabled = false;
        }

        public AssetDialog(Dictionary<String,Asset> assetDict,List<ObjectInstance> objInstList)
        {
            this.objInstList = objInstList;
            this.assetDict = assetDict;
            name = "";
            InitializeComponent();
        }

        public Byte[] stringToBuffer(String bufString, int size)
        {
            Byte[] strBuffer = new Byte[size];
            strBuffer = System.Text.Encoding.ASCII.GetBytes(bufString.PadRight(size, '\0'));
            return strBuffer;
        }

        public String bufferToString(Byte[] byteBuffer)
        {
            if (byteBuffer == null || byteBuffer.Length <= 0)
                return null;
            String resultingString;
            resultingString = System.Text.Encoding.Default.GetString(byteBuffer);
            String newResultingString = "";
            for (int i = 0; i < resultingString.Length; i++)
            {
                if (resultingString[i] == '\0')
                    break;
                else
                    newResultingString += resultingString[i];
            }
            return newResultingString;
        }

        public int bufferToInteger(Byte[] byteBuffer)
        {
            if (byteBuffer == null || byteBuffer.Length <= 0)
                return 0;
            int resultingInt = 0;
            if (byteBuffer.Length == 4)
                resultingInt = BitConverter.ToInt32(byteBuffer, 0);
            else
                resultingInt = (int)BitConverter.ToInt16(byteBuffer, 0);
            return resultingInt;
        }

        public Bitmap bufferToBitmap(Byte[] byteBuffer)
        {
            if (byteBuffer == null || byteBuffer.Length <= 0)
                return null;
            TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
            Bitmap finalBitmap = (Bitmap)tc.ConvertFrom(byteBuffer);
            return finalBitmap;
        }

        private void updateForm()
        {
            valueText.Enabled = true;
            dataBrowseButton.Enabled = false;
            updateLink();
            if (assetDict.ContainsKey(name))
            {
                dataTypeBox.SelectedItem = assetDict[name].dataType;
                nameText.Text = name;
                if (assetDict[name].dataType == "String")
                {
                    valueText.Text = bufferToString(assetDict[name].defaultValue);
                    sizeNum.Value = valueText.Text.Length + 1;
                }
                if (assetDict[name].dataType == "Integer")
                {
                    valueText.Text = bufferToInteger(assetDict[name].defaultValue).ToString();
                    sizeNum.Value = 4;
                }
                if (assetDict[name].dataType == "Bitmap" || assetDict[name].dataType == "Audio")
                {
                    dataBrowseButton.Enabled = true;
                    valueText.Enabled = false;
                    if (assetDict[name].defaultValue != null)
                    {
                        sizeNum.Value = assetDict[name].defaultValue.Length;
                        previewBox.Image = bufferToBitmap(assetDict[name].defaultValue);
                    }
                }

            }
        }

        private void updateLink()
        {
            if (assetDict.ContainsKey(nameText.Text))
            {
                existsLink.Enabled = true;
                existsLink.Text = "Asset exists\n(click to update)";
            }
            else
            {
                existsLink.Enabled = false;
                existsLink.Text = "";
            }
        }


        private void addButton_Click(object sender, EventArgs e)
        {
            Byte[] assetValue;
            this.DialogResult = DialogResult.OK;
            this.name = nameText.Text;
            String dataType = dataTypeBox.SelectedItem.ToString();
            if (dataType == "String")
                assetValue = stringToBuffer(valueText.Text, (int)sizeNum.Value);
            else if (dataType == "Integer")
                assetValue = BitConverter.GetBytes(Int32.Parse(valueText.Text));
            else
                assetValue = dataBytes;
            if (!assetDict.ContainsKey(name))
                assetDict.Add(name, new Asset(name, dataType, (int)sizeNum.Value, assetValue));
            else
                assetDict[name] = new Asset(name, dataType, (int)sizeNum.Value, assetValue);
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void existsLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.name = nameText.Text;
            updateForm();
        }

        private void nameText_TextChanged(object sender, EventArgs e)
        {
            updateLink();
        }

        private void AssetDialog_Load(object sender, EventArgs e)
        {

        }

        private void dataTypeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dataTypeBox.SelectedItem.ToString() == "Integer")
                sizeNum.Value = 4;
        }

        private void valueText_TextChanged(object sender, EventArgs e)
        {
            if (dataTypeBox.SelectedItem.ToString() == "String")
                sizeNum.Value = valueText.Text.Length + 1;
        }

        private void dataBrowseButton_Click(object sender, EventArgs e)
        {
            if (openDataFileDlg.ShowDialog() == DialogResult.OK)
                dataBytes = File.ReadAllBytes(openDataFileDlg.FileName);
            if (dataBytes != null)
                previewBox.Image = bufferToBitmap(dataBytes);
            sizeNum.Value = dataBytes.Length;
        }
    }
}
