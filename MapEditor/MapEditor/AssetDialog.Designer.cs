﻿namespace MapEditor
{
    partial class AssetDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.nameText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.sizeNum = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.dataBrowseButton = new System.Windows.Forms.Button();
            this.valueText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.previewBox = new System.Windows.Forms.PictureBox();
            this.dataTypeBox = new System.Windows.Forms.ComboBox();
            this.existsLink = new System.Windows.Forms.LinkLabel();
            this.openDataFileDlg = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.sizeNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.previewBox)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Name:";
            // 
            // nameText
            // 
            this.nameText.Location = new System.Drawing.Point(96, 34);
            this.nameText.Name = "nameText";
            this.nameText.Size = new System.Drawing.Size(177, 20);
            this.nameText.TabIndex = 16;
            this.nameText.TextChanged += new System.EventHandler(this.nameText_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Data Type:";
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(12, 246);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 20;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // sizeNum
            // 
            this.sizeNum.Enabled = false;
            this.sizeNum.Location = new System.Drawing.Point(96, 166);
            this.sizeNum.Maximum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            0});
            this.sizeNum.Name = "sizeNum";
            this.sizeNum.Size = new System.Drawing.Size(120, 20);
            this.sizeNum.TabIndex = 32;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 173);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 31;
            this.label3.Text = "Size:";
            // 
            // dataBrowseButton
            // 
            this.dataBrowseButton.Location = new System.Drawing.Point(332, 125);
            this.dataBrowseButton.Name = "dataBrowseButton";
            this.dataBrowseButton.Size = new System.Drawing.Size(75, 23);
            this.dataBrowseButton.TabIndex = 35;
            this.dataBrowseButton.Text = "Browse";
            this.dataBrowseButton.UseVisualStyleBackColor = true;
            this.dataBrowseButton.Click += new System.EventHandler(this.dataBrowseButton_Click);
            // 
            // valueText
            // 
            this.valueText.Location = new System.Drawing.Point(96, 125);
            this.valueText.Name = "valueText";
            this.valueText.Size = new System.Drawing.Size(227, 20);
            this.valueText.TabIndex = 34;
            this.valueText.TextChanged += new System.EventHandler(this.valueText_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(28, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 33;
            this.label4.Text = "Value:";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(154, 246);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 36;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // previewBox
            // 
            this.previewBox.Location = new System.Drawing.Point(267, 173);
            this.previewBox.Name = "previewBox";
            this.previewBox.Size = new System.Drawing.Size(128, 96);
            this.previewBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.previewBox.TabIndex = 37;
            this.previewBox.TabStop = false;
            // 
            // dataTypeBox
            // 
            this.dataTypeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dataTypeBox.FormattingEnabled = true;
            this.dataTypeBox.Items.AddRange(new object[] {
            "Integer",
            "String",
            "Bitmap",
            "Audio"});
            this.dataTypeBox.Location = new System.Drawing.Point(93, 83);
            this.dataTypeBox.Name = "dataTypeBox";
            this.dataTypeBox.Size = new System.Drawing.Size(308, 21);
            this.dataTypeBox.TabIndex = 38;
            this.dataTypeBox.SelectedIndexChanged += new System.EventHandler(this.dataTypeBox_SelectedIndexChanged);
            // 
            // existsLink
            // 
            this.existsLink.AutoSize = true;
            this.existsLink.Enabled = false;
            this.existsLink.Location = new System.Drawing.Point(329, 37);
            this.existsLink.Name = "existsLink";
            this.existsLink.Size = new System.Drawing.Size(0, 13);
            this.existsLink.TabIndex = 39;
            this.existsLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.existsLink_LinkClicked);
            // 
            // AssetDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 305);
            this.Controls.Add(this.existsLink);
            this.Controls.Add(this.dataTypeBox);
            this.Controls.Add(this.previewBox);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.dataBrowseButton);
            this.Controls.Add(this.valueText);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.sizeNum);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nameText);
            this.Name = "AssetDialog";
            this.Text = "AssetDialog";
            this.Load += new System.EventHandler(this.AssetDialog_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sizeNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.previewBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nameText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.NumericUpDown sizeNum;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button dataBrowseButton;
        private System.Windows.Forms.TextBox valueText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.PictureBox previewBox;
        private System.Windows.Forms.ComboBox dataTypeBox;
        private System.Windows.Forms.LinkLabel existsLink;
        private System.Windows.Forms.OpenFileDialog openDataFileDlg;
    }
}