﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MapEditor
{
    public class Property
    {
        public String name; 
        public String dataType;
        public int size;
        public int useAsset;
        public String assetName;
        public Byte[] defaultValue;

        public Property(String name, String type, int useAsset, String assetName, int size, Byte[] value)
        {
            this.dataType = type;
            this.name = name;
            defaultValue = value;
            this.size = size;
            this.useAsset = useAsset;
            if (useAsset == 0)
                this.assetName = "";
            else
                this.assetName = assetName;
        }

        public Property(String name, String type, int size, Byte[] value)
        {
            this.dataType = type;
            this.name = name;
            defaultValue = value;
            this.size = size;
            this.useAsset = 0;
            this.assetName = "";
        }
    }
}
