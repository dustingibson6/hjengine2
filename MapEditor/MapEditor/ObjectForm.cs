﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MapEditor
{
    public partial class ObjectForm : Form
    {
        public Dictionary<String, Property> propDict;
        List<ObjectInstance> selInstance;
        public ImageStates imgStates;
        public String objName;
        public List<ObjectInstance> objInstList;

        public ObjectForm(String name, Dictionary<String,Property> propDict, ImageStates imgStates, List<ObjectInstance> objInstList)
        {
            InitializeComponent();
            this.propDict = propDict;
            this.imgStates = imgStates;
            objName = name;
            nameText.Text = objName;
            nameText.Enabled = false;
            this.objInstList = objInstList;
            loadPropertyList();
        }

        public ObjectForm()
        {
            InitializeComponent();
            propDict = new Dictionary<String, Property>();
            this.objInstList = new List<ObjectInstance>();
            imgStates = new ImageStates();
        }

        public void resetUI()
        {
            propertyList.Items.Clear();
            propertyNameText.Clear();
            //dataTypeText.Clear();
            //defaultValueText.Clear();
            stateListView.Items.Clear();
            stateNameText.Clear();
            imgFileTxt.Clear();
        }

        public void loadPropertyList()
        {
            resetUI();
            foreach (KeyValuePair<String, Property> propEntry in propDict)
            {
                String propName = propEntry.Key;
                Property selProp = propEntry.Value;
                String propType = selProp.dataType;
                ListViewItem propItem = new ListViewItem(new[] { propName, propType} );
                propertyList.Items.Add(propItem);
            }
            foreach (KeyValuePair<String, Bitmap> stateEntry in imgStates.stateDict)
            {
                String stateName = stateEntry.Key;
                ListViewItem stateItem = new ListViewItem(new[] { stateName });
                stateListView.Items.Add(stateItem);
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            String propName = propertyNameText.Text;
            String dataType = dataTypeBox.SelectedItem.ToString();
            Property newProperty = new Property(propName, dataType, 0, null);
            if (propDict.ContainsKey(propName))
                propDict[propName] = newProperty;
            else
            {
                foreach( ObjectInstance curInst in objInstList)
                    if( curInst.objName == objName)
                        curInst.propDict.Add(propName, newProperty);
                propDict.Add(propName, newProperty);
            }
            loadPropertyList();
        }

        private void propertyList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (propertyList.SelectedItems.Count > 0 )
            {
                dataTypeBox.Enabled = true;
                ListViewItem selPropItem = propertyList.SelectedItems[0];
                String propName = selPropItem.Text;
                if (propDict.ContainsKey(propName))
                {
                    dataTypeBox.Enabled = false;
                    propertyNameText.Text = propName;
                    dataTypeBox.SelectedItem = propDict[propName].dataType;
                    //defaultValueText.Text = propDict[propName].defaultValue;
                }
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            objName = nameText.Text;
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void ObjectForm_Load(object sender, EventArgs e)
        {

        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            String propName = propertyNameText.Text;
            foreach (ObjectInstance curInst in objInstList)
                if (curInst.objName == objName)
                    curInst.propDict.Remove(propName);
            if (propDict.ContainsKey(propName))
                propDict.Remove(propName);
            loadPropertyList();
        }

        private void stateListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (stateListView.SelectedItems.Count > 0)
            {
                ListViewItem stateItem = stateListView.SelectedItems[0];
                String stateName = stateItem.Text;
                if (imgStates.nameExists(stateName))
                {
                    stateNameText.Text = stateName;
                    previewBox.Image = imgStates.getImg(stateName);
                }
            }
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            if (imgFileDlg.ShowDialog() == DialogResult.OK)
            {
                imgFileTxt.Text = imgFileDlg.FileName;
            }
        }

        private void saveStateButton_Click(object sender, EventArgs e)
        {
            if (imgStates.nameExists(stateNameText.Text))
                imgStates.modifyImage(stateNameText.Text, new Bitmap(imgFileTxt.Text));
            else
                imgStates.addState(stateNameText.Text, new Bitmap(imgFileTxt.Text));
            loadPropertyList();
        }

        private void deleteStateButton_Click(object sender, EventArgs e)
        {
            String stateName = stateNameText.Text;
            if (imgStates.nameExists(stateName))
                imgStates.delState(stateName);
            loadPropertyList();
        }

        private void dataTypeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void propertyNameText_TextChanged(object sender, EventArgs e)
        {
            if (propDict.ContainsKey(propertyNameText.Text))
                dataTypeBox.Enabled = false;
            else
                dataTypeBox.Enabled = true;
        }
    }
}
