﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Runtime.Serialization.Formatters.Binary;
using System.Drawing.Imaging;

namespace MapEditor
{
    public class TileSet
    {
        public Dictionary<Point,Bitmap> tileImgs;
        public Dictionary<Point, Color> averageColor;
        public int row;
        public int col;
        public int width;
        public int height;
        public int maxCol;
        public Bitmap fullImage;

        public long getBitmapSize(Bitmap img)
        {
            ImageConverter converter = new ImageConverter();
            Byte[] imgBytes = (byte[])converter.ConvertTo(img, typeof(byte[]));
            return imgBytes.Length;
        }

        public TileSet(Bitmap tileSetImage, int tw, int th)
        {
            tileImgs = new Dictionary<Point,Bitmap>();
            averageColor = new Dictionary<Point, Color>();
            fullImage = tileSetImage;
            col = fullImage.Width / tw;
            row = fullImage.Height / th;
              width = tw;
            height = th;
            int cnt = 0;
            for(int i=0; i < col; i++)
                for (int j = 0; j < row; j++)
                {
                    Rectangle imgDim = new Rectangle(i*tw,j*th,tw,th);
                    Bitmap tileImg = fullImage.Clone(imgDim, PixelFormat.Format32bppArgb);
                    //tileImg.MakeTransparent(Color.FromArgb(255, 40, 40, 40));
                    tileImgs.Add(new Point(i,j), tileImg);
                    cnt = cnt + 1;
                    maxCol = i;
                }
        }

        public TileSet(int tw, int th)
        {
            tileImgs = new Dictionary<Point, Bitmap>();
            averageColor = new Dictionary<Point, Color>();
            width = tw;
            height = th;
            width = 20;
            row = 0;
        }

        public Point exportAdd(Bitmap sampleBitmap)
        {
            foreach(KeyValuePair<Point,Bitmap> compBitmap in tileImgs)
            {
                if(compBitmap.Equals(sampleBitmap))
                    return compBitmap.Key;
            }
            int returnRow = row;
            int returnCol = maxCol;
            tileImgs.Add(new Point(maxCol, row), sampleBitmap);
            maxCol = maxCol + 1;
            if (maxCol > col)
            {
                maxCol = 0;
                row = row + 1;
            }
            return new Point(returnRow, maxCol);
        }

        public void colorSwap(bool isAlpha, Color from, Color to)
        {
            List<Point> pointKeys = new List<Point>(tileImgs.Keys);
            foreach (Point curKey in pointKeys)
            {
                Bitmap sampleBitmap = tileImgs[curKey];
                if (isAlpha)
                    sampleBitmap.MakeTransparent(from);
                else
                    for (int i = 0; i < width; i++)
                        for (int j = 0; j < height; j++)
                        {
                                if (sampleBitmap.GetPixel(i, j) == from)
                                    sampleBitmap.SetPixel(i, j, to);
                        }
                    tileImgs[curKey] = sampleBitmap;
            }
        }

        public TileSet(int tw, int th, int col, int row)
        {
            averageColor = new Dictionary<Point, Color>();
            tileImgs = new Dictionary<Point, Bitmap>();
            this.row = row;
            this.col = col;
            this.width = tw;
            this.height = th;
        }

        public int pointToInt(Point pnt)
        {
            //y*w-(h-x)
            int y = pnt.X+1;
            int x = pnt.Y+1;
            int w = row;
            return (w * (y - 1) + x)-1;
        }

    }
}
