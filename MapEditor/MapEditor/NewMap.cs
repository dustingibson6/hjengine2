﻿using System;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MapEditor
{
    public partial class NewMap : Form
    {
        public TileMap tileMap;
        public String folderPath;
        public bool isExport;
        public Bitmap exportBitmap;

        public NewMap()
        {
            isExport = false;
            InitializeComponent();
        }

        private XmlElement createElement(XmlDocument xmlDoc, String name, String text)
        {
            XmlElement newElement = xmlDoc.CreateElement(name);
            newElement.InnerText = text;
            return newElement;
        }

        private void createButton_Click(object sender, EventArgs e)
        {
            String tileSetLocation = tilesetLocationText.Text;
            String folderSaveDialog = saveLocationText.Text;
            int row = (int)rowNum.Value;
            int col = (int)colNum.Value;
            int width = (int)widthNum.Value;
            int height = (int)heightNum.Value;
            String name = nameText.Text;
            Bitmap tileSetImg = new Bitmap(tileSetLocation);
            if (exportCheck.Checked && exportBitmap != null)
                this.tileMap = new TileMap(name, exportBitmap, width, height, row, col);
            else
                this.tileMap =  new TileMap(name,folderSaveDialog, tileSetImg, width, height, row, col);
            folderPath = folderSaveDialog;
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void browseTileSetButton_Click(object sender, EventArgs e)
        {
            if (openTileSet.ShowDialog() == DialogResult.OK)
            {
                tilesetLocationText.Text = openTileSet.FileName;
            }
        }

        private void saveBrowseButton_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                saveLocationText.Text = saveFileDialog.FileName;
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void NewMap_Load(object sender, EventArgs e)
        {

        }

        private void NewMap_Paint(object sender, PaintEventArgs e)
        {

        }

        private void exportBrowseButton_Click(object sender, EventArgs e)
        {
            if (exportFileBrowse.ShowDialog() == DialogResult.OK)
            {
                exportText.Text = exportFileBrowse.FileName;
                exportBitmap = new Bitmap(exportText.Text);
            }
        }
    }
}
