﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace MapEditor
{
    public class TileValue
    {
        public Point sel;
        public int col;

        public TileValue(Point sel, int col)
        {
            this.sel = sel;
            this.col = col;
        }

    }
}
