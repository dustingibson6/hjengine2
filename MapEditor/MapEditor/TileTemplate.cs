﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace MapEditor
{
    public class TileTemplate
    {
        public Dictionary<String, Dictionary<Point, Point>> templateDict;
        //public Dictionary<Point,Point> curPointDict;
        String mode;

        public TileTemplate()
        {

        }

        public bool keyTemplateExist(String name)
        {
            return templateDict.ContainsKey(name);
        }

        public void addTileTemplate(String name, Dictionary<Point,Point> pointDict)
        {
            templateDict.Add(name, pointDict);
        }
    }
}
