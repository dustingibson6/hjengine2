﻿namespace MapEditor
{
    partial class ColorCodeDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.fromRed = new System.Windows.Forms.NumericUpDown();
            this.fromGreen = new System.Windows.Forms.NumericUpDown();
            this.fromBlue = new System.Windows.Forms.NumericUpDown();
            this.toBlue = new System.Windows.Forms.NumericUpDown();
            this.toGreen = new System.Windows.Forms.NumericUpDown();
            this.toRed = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.fromAlpha = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.toAlpha = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.alphaCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.fromRed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fromGreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fromBlue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toBlue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toGreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toRed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fromAlpha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toAlpha)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Red:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Green:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Blue:";
            // 
            // fromRed
            // 
            this.fromRed.Location = new System.Drawing.Point(68, 25);
            this.fromRed.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.fromRed.Name = "fromRed";
            this.fromRed.Size = new System.Drawing.Size(120, 20);
            this.fromRed.TabIndex = 0;
            // 
            // fromGreen
            // 
            this.fromGreen.Location = new System.Drawing.Point(68, 65);
            this.fromGreen.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.fromGreen.Name = "fromGreen";
            this.fromGreen.Size = new System.Drawing.Size(120, 20);
            this.fromGreen.TabIndex = 1;
            // 
            // fromBlue
            // 
            this.fromBlue.Location = new System.Drawing.Point(68, 108);
            this.fromBlue.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.fromBlue.Name = "fromBlue";
            this.fromBlue.Size = new System.Drawing.Size(120, 20);
            this.fromBlue.TabIndex = 2;
            // 
            // toBlue
            // 
            this.toBlue.Location = new System.Drawing.Point(59, 106);
            this.toBlue.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.toBlue.Name = "toBlue";
            this.toBlue.Size = new System.Drawing.Size(120, 20);
            this.toBlue.TabIndex = 6;
            // 
            // toGreen
            // 
            this.toGreen.Location = new System.Drawing.Point(59, 63);
            this.toGreen.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.toGreen.Name = "toGreen";
            this.toGreen.Size = new System.Drawing.Size(120, 20);
            this.toGreen.TabIndex = 5;
            // 
            // toRed
            // 
            this.toRed.Location = new System.Drawing.Point(59, 25);
            this.toRed.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.toRed.Name = "toRed";
            this.toRed.Size = new System.Drawing.Size(120, 20);
            this.toRed.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Blue:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Green:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Red:";
            // 
            // fromAlpha
            // 
            this.fromAlpha.Location = new System.Drawing.Point(68, 151);
            this.fromAlpha.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.fromAlpha.Name = "fromAlpha";
            this.fromAlpha.Size = new System.Drawing.Size(120, 20);
            this.fromAlpha.TabIndex = 3;
            this.fromAlpha.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 158);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Alpha:";
            // 
            // toAlpha
            // 
            this.toAlpha.Location = new System.Drawing.Point(59, 149);
            this.toAlpha.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.toAlpha.Name = "toAlpha";
            this.toAlpha.Size = new System.Drawing.Size(120, 20);
            this.toAlpha.TabIndex = 7;
            this.toAlpha.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 156);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Alpha:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.fromAlpha);
            this.groupBox1.Controls.Add(this.fromRed);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.fromGreen);
            this.groupBox1.Controls.Add(this.fromBlue);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(212, 191);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "From:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.alphaCheckBox);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.toAlpha);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.toBlue);
            this.groupBox2.Controls.Add(this.toRed);
            this.groupBox2.Controls.Add(this.toGreen);
            this.groupBox2.Location = new System.Drawing.Point(248, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 225);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "To:";
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(80, 267);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 8;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(295, 267);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 9;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // alphaCheckBox
            // 
            this.alphaCheckBox.AutoSize = true;
            this.alphaCheckBox.Location = new System.Drawing.Point(9, 190);
            this.alphaCheckBox.Name = "alphaCheckBox";
            this.alphaCheckBox.Size = new System.Drawing.Size(83, 17);
            this.alphaCheckBox.TabIndex = 18;
            this.alphaCheckBox.Text = "Transparent";
            this.alphaCheckBox.UseVisualStyleBackColor = true;
            // 
            // ColorCodeDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 302);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "ColorCodeDialog";
            this.Text = "ColorCodeDialog";
            this.Load += new System.EventHandler(this.ColorCodeDialog_Load);
            ((System.ComponentModel.ISupportInitialize)(this.fromRed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fromGreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fromBlue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toBlue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toGreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toRed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fromAlpha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toAlpha)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown fromRed;
        private System.Windows.Forms.NumericUpDown fromGreen;
        private System.Windows.Forms.NumericUpDown fromBlue;
        private System.Windows.Forms.NumericUpDown toBlue;
        private System.Windows.Forms.NumericUpDown toGreen;
        private System.Windows.Forms.NumericUpDown toRed;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown fromAlpha;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown toAlpha;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.CheckBox alphaCheckBox;
    }
}