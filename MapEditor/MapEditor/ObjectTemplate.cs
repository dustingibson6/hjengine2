﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace MapEditor
{
    public class ObjectTemplate
    {
        public Dictionary<String, Property> propertyDict;
        public ImageStates imgStates;
        public String name;

        public ObjectTemplate(String name, Dictionary<String, Property> propDict,ImageStates imgStates)
        {
            propertyDict = propDict;
            this.imgStates = imgStates;
            this.name = name;
            Bitmap defaultBitmap = new Bitmap(32, 32);
            if (!imgStates.nameExists("default"))
            {
                for (int i = 0; i < 32; i++)
                    for (int j = 0; j < 32; j++)
                    {
                        defaultBitmap.SetPixel(i, j, Color.Black);
                    }
                this.imgStates.addState("default", defaultBitmap);
            }
        }
    }
}
