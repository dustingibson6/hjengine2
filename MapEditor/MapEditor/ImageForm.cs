﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace MapEditor
{
    public partial class ImageForm : Form
    {
        Bitmap previewImg;

        public ImageForm(Bitmap previewImg)
        {
            InitializeComponent();
            this.previewImg = previewImg;
        }

        private void ImageForm_Load(object sender, EventArgs e)
        {
            previewBox.Image = this.previewImg;
        }
    }
}
