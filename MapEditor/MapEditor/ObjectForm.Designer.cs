﻿namespace MapEditor
{
    partial class ObjectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataTypeBox = new System.Windows.Forms.ComboBox();
            this.deleteButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.propertyNameText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.nameText = new System.Windows.Forms.TextBox();
            this.propertyList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.previewBox = new System.Windows.Forms.PictureBox();
            this.browseButton = new System.Windows.Forms.Button();
            this.deleteStateButton = new System.Windows.Forms.Button();
            this.saveStateButton = new System.Windows.Forms.Button();
            this.imgFileTxt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.stateNameText = new System.Windows.Forms.TextBox();
            this.stateListView = new System.Windows.Forms.ListView();
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cancelButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.imgFileDlg = new System.Windows.Forms.OpenFileDialog();
            this.openDataFileDlg = new System.Windows.Forms.OpenFileDialog();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.previewBox)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(531, 503);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataTypeBox);
            this.tabPage1.Controls.Add(this.deleteButton);
            this.tabPage1.Controls.Add(this.saveButton);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.propertyNameText);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.nameText);
            this.tabPage1.Controls.Add(this.propertyList);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(523, 477);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Properties";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataTypeBox
            // 
            this.dataTypeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dataTypeBox.FormattingEnabled = true;
            this.dataTypeBox.Items.AddRange(new object[] {
            "Integer",
            "String",
            "Bitmap",
            "Audio"});
            this.dataTypeBox.Location = new System.Drawing.Point(109, 349);
            this.dataTypeBox.Name = "dataTypeBox";
            this.dataTypeBox.Size = new System.Drawing.Size(308, 21);
            this.dataTypeBox.TabIndex = 17;
            this.dataTypeBox.SelectedIndexChanged += new System.EventHandler(this.dataTypeBox_SelectedIndexChanged);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(256, 403);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(75, 23);
            this.deleteButton.TabIndex = 10;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(109, 403);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 9;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 357);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Data Type:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 314);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Property Name:";
            // 
            // propertyNameText
            // 
            this.propertyNameText.Location = new System.Drawing.Point(109, 311);
            this.propertyNameText.Name = "propertyNameText";
            this.propertyNameText.Size = new System.Drawing.Size(308, 20);
            this.propertyNameText.TabIndex = 5;
            this.propertyNameText.TextChanged += new System.EventHandler(this.propertyNameText_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Name:";
            // 
            // nameText
            // 
            this.nameText.Location = new System.Drawing.Point(76, 20);
            this.nameText.Name = "nameText";
            this.nameText.Size = new System.Drawing.Size(177, 20);
            this.nameText.TabIndex = 3;
            // 
            // propertyList
            // 
            this.propertyList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.propertyList.FullRowSelect = true;
            this.propertyList.GridLines = true;
            this.propertyList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.propertyList.Location = new System.Drawing.Point(10, 60);
            this.propertyList.Name = "propertyList";
            this.propertyList.Size = new System.Drawing.Size(440, 223);
            this.propertyList.TabIndex = 2;
            this.propertyList.UseCompatibleStateImageBehavior = false;
            this.propertyList.View = System.Windows.Forms.View.Details;
            this.propertyList.SelectedIndexChanged += new System.EventHandler(this.propertyList_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Property Name";
            this.columnHeader1.Width = 109;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Data Type";
            this.columnHeader2.Width = 118;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Controls.Add(this.browseButton);
            this.tabPage2.Controls.Add(this.deleteStateButton);
            this.tabPage2.Controls.Add(this.saveStateButton);
            this.tabPage2.Controls.Add(this.imgFileTxt);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.stateNameText);
            this.tabPage2.Controls.Add(this.stateListView);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(523, 477);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "States";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.previewBox);
            this.panel2.Location = new System.Drawing.Point(29, 175);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(439, 131);
            this.panel2.TabIndex = 23;
            // 
            // previewBox
            // 
            this.previewBox.Location = new System.Drawing.Point(3, 3);
            this.previewBox.Name = "previewBox";
            this.previewBox.Size = new System.Drawing.Size(433, 125);
            this.previewBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.previewBox.TabIndex = 22;
            this.previewBox.TabStop = false;
            // 
            // browseButton
            // 
            this.browseButton.Location = new System.Drawing.Point(360, 373);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(75, 23);
            this.browseButton.TabIndex = 21;
            this.browseButton.Text = "Browse";
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // deleteStateButton
            // 
            this.deleteStateButton.Location = new System.Drawing.Point(266, 434);
            this.deleteStateButton.Name = "deleteStateButton";
            this.deleteStateButton.Size = new System.Drawing.Size(75, 23);
            this.deleteStateButton.TabIndex = 18;
            this.deleteStateButton.Text = "Delete";
            this.deleteStateButton.UseVisualStyleBackColor = true;
            this.deleteStateButton.Click += new System.EventHandler(this.deleteStateButton_Click);
            // 
            // saveStateButton
            // 
            this.saveStateButton.Location = new System.Drawing.Point(129, 434);
            this.saveStateButton.Name = "saveStateButton";
            this.saveStateButton.Size = new System.Drawing.Size(75, 23);
            this.saveStateButton.TabIndex = 17;
            this.saveStateButton.Text = "Save";
            this.saveStateButton.UseVisualStyleBackColor = true;
            this.saveStateButton.Click += new System.EventHandler(this.saveStateButton_Click);
            // 
            // imgFileTxt
            // 
            this.imgFileTxt.Location = new System.Drawing.Point(129, 373);
            this.imgFileTxt.Name = "imgFileTxt";
            this.imgFileTxt.Size = new System.Drawing.Size(224, 20);
            this.imgFileTxt.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(40, 376);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Image File:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(40, 338);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "State Name:";
            // 
            // stateNameText
            // 
            this.stateNameText.Location = new System.Drawing.Point(129, 335);
            this.stateNameText.Name = "stateNameText";
            this.stateNameText.Size = new System.Drawing.Size(308, 20);
            this.stateNameText.TabIndex = 13;
            // 
            // stateListView
            // 
            this.stateListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4});
            this.stateListView.Dock = System.Windows.Forms.DockStyle.Top;
            this.stateListView.FullRowSelect = true;
            this.stateListView.GridLines = true;
            this.stateListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.stateListView.Location = new System.Drawing.Point(3, 3);
            this.stateListView.Name = "stateListView";
            this.stateListView.Size = new System.Drawing.Size(517, 153);
            this.stateListView.TabIndex = 1;
            this.stateListView.UseCompatibleStateImageBehavior = false;
            this.stateListView.View = System.Windows.Forms.View.Details;
            this.stateListView.SelectedIndexChanged += new System.EventHandler(this.stateListView_SelectedIndexChanged);
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "State Name";
            this.columnHeader4.Width = 464;
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(318, 3);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 14;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(58, 3);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 13;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cancelButton);
            this.panel1.Controls.Add(this.okButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 472);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(531, 31);
            this.panel1.TabIndex = 1;
            // 
            // imgFileDlg
            // 
            this.imgFileDlg.FileName = "openFileDialog1";
            // 
            // openDataFileDlg
            // 
            this.openDataFileDlg.FileName = "openFileDialog1";
            // 
            // ObjectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 503);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabControl1);
            this.Name = "ObjectForm";
            this.Text = "ObjectForm";
            this.Load += new System.EventHandler(this.ObjectForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.previewBox)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListView stateListView;
        private System.Windows.Forms.ListView propertyList;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox propertyNameText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nameText;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button deleteStateButton;
        private System.Windows.Forms.Button saveStateButton;
        private System.Windows.Forms.TextBox imgFileTxt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox stateNameText;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.PictureBox previewBox;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.OpenFileDialog imgFileDlg;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.OpenFileDialog openDataFileDlg;
        private System.Windows.Forms.ComboBox dataTypeBox;
    }
}