﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace MapEditor
{
    public class ObjectInstance
    {
        public String objName;
        public Dictionary<String, Property> propDict;
        public Point pnt;
        public int visible;

        public ObjectInstance(String objName, Dictionary<String, Property> propDict, Point pnt, int visible)
        {
            this.objName = objName;
            this.propDict = propDict;
            this.pnt = pnt;
            this.visible = visible;
        }

    }
}
