﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.ComponentModel;
using System.Xml;

namespace MapEditor
{
    public class TileMap
    {
        public List<ObjectInstance> objInstList;
        public String saveDirectory;
        public String tileSetFname;
        //public Dictionary<Point,TileValue> mapSet;
        public Dictionary<String, Asset> assetDict;
        //public Dictionary<Point, int> collisionSet;
        public int tileWidth;
        public int tileHeight;
        public int colNum;
        public int rowNum;
        public int tileSetX = 0;
        public int tileSetY = 0;
        public String name;
        public TileSet tileSet;
        public Dictionary<String, ObjectTemplate> objTemplates;
        public List<Dictionary<Point, TileValue>> layerSet;

        public TileMap(String name, Bitmap staticImage, int tileWidth, int tileHeight, int rowNum, int colNum)
        {
            objInstList = new List<ObjectInstance>();
            objTemplates = new Dictionary<String, ObjectTemplate>();
            this.name = name;
            this.tileWidth = tileWidth;
            this.tileHeight = tileHeight;
            this.rowNum = rowNum;
            this.colNum = colNum;
            this.layerSet = new List<Dictionary<Point, TileValue>>();
            this.assetDict = new Dictionary<String, Asset>();
            layerSet.Add(exportMap(staticImage));
            packData();

        }

        public TileMap(String name, String saveDirectory, Bitmap tileSetImage, int tileWidth, int tileHeight, int rowNum, int colNum)
        {
            objInstList = new List<ObjectInstance>();
            objTemplates = new Dictionary<String, ObjectTemplate>();
            this.name = name;
            this.saveDirectory = saveDirectory;
            //this.tileSetFname = tileSetFname;
            this.tileWidth = tileWidth;
            this.tileHeight = tileHeight;
            this.rowNum = rowNum;
            this.colNum = colNum;
            //this.mapSet = new Dictionary<Point,TileValue>();
            this.layerSet = new List<Dictionary<Point, TileValue>>();
            this.assetDict = new Dictionary<String, Asset>();
            Dictionary<Point, TileValue> mapSet = new Dictionary<Point, TileValue>();
            if (!File.Exists(saveDirectory))
            {
                tileSet = new TileSet(tileSetImage, tileWidth, tileHeight);
                for (int i = 0; i < colNum; i++)
                    for (int j = 0; j < rowNum; j++)
                    {
                        mapSet.Add(new Point(i, j), new TileValue(new Point(-1, -1), 0));
                    }
                layerSet.Add(mapSet);
                packData();
            }
            else
            {
                loadData();
            }
        }

        public Dictionary<Point, TileValue> exportMap(Bitmap staticMap)
        {
            Dictionary<Point, Bitmap> bitmapMap = new Dictionary<Point, Bitmap>();
            TileSet workingSet = new TileSet(tileWidth, tileHeight);
            Dictionary<Point, TileValue> mapSet = new Dictionary<Point, TileValue>();
            for (int i = 0; i < colNum; i++)
                for (int j = 0; j < rowNum; j++)
                {
                    Bitmap sampleBitmap = staticMap.Clone(new Rectangle(new Point(i * tileWidth, j * tileHeight), new Size(tileWidth, tileHeight)), PixelFormat.Format32bppArgb);
                    Point returnPoint = tileSet.exportAdd(sampleBitmap);
                    mapSet.Add(new Point(i, j), new TileValue(returnPoint, -1));
                }
            return mapSet;
        }

        public TileMap(String saveDirectory)
        {
            objInstList = new List<ObjectInstance>();
            objTemplates = new Dictionary<String, ObjectTemplate>();
            this.saveDirectory = saveDirectory;
            this.layerSet = new List<Dictionary<Point, TileValue>>();
            //this.mapSet = new Dictionary<Point, TileValue>();
            this.assetDict = new Dictionary<String, Asset>();
            //loadObjects(xmlDoc);
            loadData();
        }

        public TileMap(TileMap tileMap)
        {
            objInstList = new List<ObjectInstance>();
            objTemplates = new Dictionary<String, ObjectTemplate>();
            this.layerSet = new List<Dictionary<Point, TileValue>>(tileMap.layerSet);
            //this.layerSet = new Dictionary<Point, TileValue>(tileMap.layerSet);
        }

        public Boolean keyExists(int curLayer, Point pnt)
        {
            return layerSet[curLayer].ContainsKey(pnt);
        }

        public void initBuffer(Byte[] tmpBuffer, int size)
        {
            for (int i = 0; i < size; i++)
                tmpBuffer[i] = 0;
        }

        public int traverseToInt(Byte[] byteBuffer, ref int counter, int destSize)
        {
            int resultingInt = 0;
            Byte[] tmpBuffer = new Byte[destSize];
            Array.Copy(byteBuffer, counter, tmpBuffer, 0, destSize);
            counter = counter + destSize;
            if (destSize == 4)
                resultingInt = BitConverter.ToInt32(tmpBuffer, 0);
            else
                resultingInt = (int)BitConverter.ToInt16(tmpBuffer, 0);
            return resultingInt;
        }

        public String traverseToString(Byte[] byteBuffer, ref int counter, int destSize)
        {
            String resultingString;
            Byte[] tmpBuffer = new Byte[destSize];
            Array.Copy(byteBuffer, counter, tmpBuffer, 0, destSize);
            resultingString = System.Text.Encoding.Default.GetString(tmpBuffer);
            counter = counter + destSize;
            String newResultingString = "";
            for (int i = 0; i < resultingString.Length; i++)
            {
                if (resultingString[i] == '\0')
                    break;
                else
                    newResultingString += resultingString[i];
            }

            return newResultingString;
        }

        public Byte[] traverseToByte(Byte[] byteBuffer, ref int counter, int destSize)
        {
            Byte[] tmpBuffer = new Byte[destSize];
            Array.Copy(byteBuffer, counter, tmpBuffer, 0, destSize);
            counter = counter + destSize;
            return tmpBuffer;
        }

        public Bitmap traverseToBitmap(Byte[] byteBuffer, ref int counter, int destSize)
        {
            Byte[] tmpBuffer = new Byte[destSize];
            TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
            Array.Copy(byteBuffer, counter, tmpBuffer, 0, destSize);
            Bitmap finalBitmap = (Bitmap)tc.ConvertFrom(tmpBuffer);
            counter = counter + destSize;
            return finalBitmap;
        }

        public Boolean containsObject(String name)
        {
            return objTemplates.ContainsKey(name);
        }

        public ObjectTemplate getObjectTemplate(String selName)
        {
            return objTemplates[selName];
        }

        public void addObject(String objName, Dictionary<String, Property> propDict, ImageStates imgStates)
        {
            objTemplates.Add(objName, new ObjectTemplate(objName, propDict, imgStates));
        }

        public void deleteObject(String selName)
        {
            List<ObjectInstance> objInstListCopy = new List<ObjectInstance>(objInstList);
            foreach (ObjectInstance curInst in objInstListCopy)
                if (curInst.objName == selName)
                    objInstList.Remove(curInst);
            objTemplates.Remove(selName);
        }

        public void editObject(String selName, String objName, Dictionary<String, Property> propDict, ImageStates imgStates, List<ObjectInstance> objInstList)
        {
            objTemplates[objName].propertyDict = propDict;
            objTemplates[objName].imgStates = imgStates;
            this.objInstList = objInstList;
        }

        public void loadData()
        {
            Byte[] mainBuffer = File.ReadAllBytes(saveDirectory);
            int counter = 0;
            //Map name
            name = traverseToString(mainBuffer, ref counter, 20);
            //Tile Width
            tileWidth = traverseToInt(mainBuffer, ref counter, 2);
            //Tile Height
            tileHeight = traverseToInt(mainBuffer, ref counter, 2);
            //Row Number
            rowNum = traverseToInt(mainBuffer, ref counter, 2);
            //Column Number
            colNum = traverseToInt(mainBuffer, ref counter, 2);
            //Tile Set Col
            tileSetX = traverseToInt(mainBuffer, ref counter, 2);
            //Tile Set Row
            tileSetY = traverseToInt(mainBuffer, ref counter, 2);
            //Tile Set MaxCol
            tileSet = new TileSet(tileWidth, tileHeight, tileSetX, tileSetY);
            tileSet.maxCol = traverseToInt(mainBuffer, ref counter, 2);
            //Number of Layers
            int numLayer = traverseToInt(mainBuffer, ref counter, 2);
            //Tile Set Image
            for (int i = 0; i < tileSetX; i++)
                for (int j = 0; j < tileSetY; j++)
                {
                    if (j == tileSetY - 1 && tileSet.maxCol < i)
                        break;
                    int imgSize = traverseToInt(mainBuffer, ref counter, 4);
                    addTile(traverseToBitmap(mainBuffer, ref counter, imgSize), new Point(i, j));
                    //fullBitmap.Clone(new Rectangle(i * tileWidth, j * tileHeight, tileWidth, tileHeight),fullBitmap.PixelFormat);
                }
            int fullBitmapSize = traverseToInt(mainBuffer, ref counter, 4);
            Bitmap fullBitmap = traverseToBitmap(mainBuffer, ref counter, fullBitmapSize);
            //Full Tileset Image Size
            //Map Set
            for (int k = 0; k < numLayer; k++)
            {
                Dictionary<Point, TileValue> mapSet = new Dictionary<Point, TileValue>();
                for (int i = 0; i < colNum; i++)
                    for (int j = 0; j < rowNum; j++)
                    {
                        int tmpX = traverseToInt(mainBuffer, ref counter, 2);
                        int tmpY = traverseToInt(mainBuffer, ref counter, 2);
                        int tmpValueX = traverseToInt(mainBuffer, ref counter, 2);
                        int tmpValueY = traverseToInt(mainBuffer, ref counter, 2);
                        int tmpCol = traverseToInt(mainBuffer, ref counter, 2);
                        mapSet.Add(new Point(tmpX, tmpY), new TileValue(new Point(tmpValueX, tmpValueY), tmpCol));
                    }
                layerSet.Add(mapSet);
            }
            //Number of Object Templates
            int numObjTemplate = traverseToInt(mainBuffer, ref counter, 2);
            for (int i = 0; i < numObjTemplate; i++)
            {
                //Object Name
                String objTempName = traverseToString(mainBuffer, ref counter, 20);
                Dictionary<String, Property> propDict = new Dictionary<String, Property>();
                ImageStates imgStates = new ImageStates();
                //Number of Properties
                int propNum = traverseToInt(mainBuffer, ref counter, 4);
                for (int k = 0; k < propNum; k++)
                {
                    //Property name
                    String propName = traverseToString(mainBuffer, ref counter, 20);
                    //Property data type
                    String propDataType = traverseToString(mainBuffer, ref counter, 20);
                    propDict.Add(propName, new Property(propName, propDataType, 0, null));
                }
                int numStates = traverseToInt(mainBuffer, ref counter, 2);
                for (int j = 0; j < numStates; j++)
                {
                    String stateName = traverseToString(mainBuffer, ref counter, 20);
                    int bitmapSize = traverseToInt(mainBuffer, ref counter, 4);
                    Bitmap stateImg = traverseToBitmap(mainBuffer, ref counter, bitmapSize);
                    imgStates.addState(stateName, stateImg);
                }
                objTemplates.Add(objTempName, new ObjectTemplate(objTempName, propDict, imgStates));
            }
            //Number of Object Instances
            int numObjInstance = traverseToInt(mainBuffer, ref counter, 2);
            for (int i = 0; i < numObjInstance; i++)
            {
                //Name of Object
                String objInstName = traverseToString(mainBuffer, ref counter, 20);
                //Visible
                int visible = traverseToInt(mainBuffer, ref counter, 2);
                //Int x
                int xInst = traverseToInt(mainBuffer, ref counter, 2);
                //Int y
                int yInst = traverseToInt(mainBuffer, ref counter, 2);
                //Number of Properties
                int numProp = traverseToInt(mainBuffer, ref counter, 2);
                Dictionary<String, Property> curProperty = new Dictionary<String, Property>();
                for (int j = 0; j < numProp; j++)
                {
                    //Property name
                    String propName = traverseToString(mainBuffer, ref counter, 20);
                    //Property Data Type
                    String propDataType = traverseToString(mainBuffer, ref counter, 20);
                    //Use asset?
                    int useAsset = traverseToInt(mainBuffer, ref counter, 2);
                    //Asset name
                    String assetName = traverseToString(mainBuffer, ref counter, 20);
                    //Property size
                    int propSize = traverseToInt(mainBuffer, ref counter, 4);
                    //Property value
                    Byte[] propValue = traverseToByte(mainBuffer, ref counter, propSize);
                    curProperty.Add(propName, new Property(propName, propDataType, useAsset, assetName, propSize, propValue));
                }
                objInstList.Add(new ObjectInstance(objInstName, curProperty, new Point(xInst, yInst), visible));
            }
            //Number of Assets
            int numAssets = traverseToInt(mainBuffer, ref counter, 2);
            for (int i = 0; i < numAssets; i++)
            {
                //Asset name
                String assetName = traverseToString(mainBuffer, ref counter, 20);
                //Asset datatype
                String assetDataType = traverseToString(mainBuffer, ref counter, 20);
                //Asset size
                int assetSize = traverseToInt(mainBuffer, ref counter, 4);
                //Asset buffer
                Byte[] assetBuffer = traverseToByte(mainBuffer, ref counter, assetSize);
                assetDict.Add(assetName, new Asset(assetName, assetDataType, assetSize, assetBuffer));
            }
        }

        public TileMap getCopy()
        {
            return (TileMap)this.MemberwiseClone();
        }

        public long getBitmapSize(Bitmap img)
        {
            ImageConverter converter = new ImageConverter();
            Byte[] imgBytes = (byte[])converter.ConvertTo(img, typeof(byte[]));
            return imgBytes.Length;
        }

        public Byte[] getBitmapBytes(Bitmap img)
        {
            ImageConverter converter = new ImageConverter();
            Byte[] imgBytes = (byte[])converter.ConvertTo(img, typeof(byte[]));
            return imgBytes;
        }

        private Byte[] getStringBuffer(String buf, int size)
        {
            Byte[] strBuffer = new Byte[size];
            strBuffer = System.Text.Encoding.ASCII.GetBytes(buf.PadRight(20, '\0'));
            return strBuffer;
        }

        private Byte[] packNumBytes(int data, int size)
        {
            if (size == 4)
                return BitConverter.GetBytes(data);
            else
                return BitConverter.GetBytes((short)data);
        }

        private void AddToBitmap(Bitmap srcBitmap, Bitmap destBitmap, int x, int y, int w, int h)
        {
            for (int i = 0; i < w; i++)
                for (int j = 0; j < h; j++)
                {
                    Color col = srcBitmap.GetPixel(i, j);
                    destBitmap.SetPixel(x+i, y+j, col);
                }
        }

        public void packData()
        {
            FileStream fs = File.Create(saveDirectory);
            BinaryWriter bw = new BinaryWriter(fs);
            //Map Name
            bw.Write(getStringBuffer(name, 20));
            //Tile Width
            bw.Write(packNumBytes(tileWidth, 2));
            //Tile Height
            bw.Write(packNumBytes(tileHeight, 2));
            //Row Number
            bw.Write(packNumBytes(rowNum, 2));
            //Column Number
            bw.Write(packNumBytes(colNum, 2));
            //TileSet Row
            bw.Write(packNumBytes(tileSet.col, 2));
            //TileSet Col
            bw.Write(packNumBytes(tileSet.row, 2));
            //Max Col
            bw.Write(packNumBytes(tileSet.maxCol, 2));
            //Number of Layers
            bw.Write(packNumBytes(layerSet.Count(), 2));
            //Tileset Size and Tile Images
            Bitmap fullBitmap = new Bitmap(tileSet.col * tileWidth, tileSet.row * tileHeight);
            for (int i = 0; i < tileSet.col; i++)
                for (int j = 0; j < tileSet.row; j++)
                {
                    if (tileIndexExists(new Point(i, j)))
                    {
                        int bitmapSize = (int)getBitmapSize(getBitmapByIndex(new Point(i, j)));
                        //Bitmap Size foreach Bitmap
                        bw.Write(BitConverter.GetBytes(bitmapSize));
                        //Bitmap foreach Bitmap
                        bw.Write(getBitmapBytes(getBitmapByIndex(new Point(i, j))));
                        AddToBitmap(getBitmapByIndex(new Point(i, j)), fullBitmap, i * tileWidth, j * tileHeight, tileWidth, tileHeight);
                    }
                }
            //Full Bitmap Size
            int fullBitmapSize = (int)getBitmapSize(fullBitmap);
            bw.Write(BitConverter.GetBytes(fullBitmapSize));
            //Full Bitamp
            bw.Write(getBitmapBytes(fullBitmap));
            foreach (Dictionary<Point, TileValue> mapSet in layerSet)
                foreach (Point key in mapSet.Keys)
                {
                    //(X,Y) position foreach map tile
                    //(X,Y) tilesheet key foreach map tile
                    //COllision value foreach map tile
                    bw.Write(packNumBytes(key.X, 2));
                    bw.Write(packNumBytes(key.Y, 2));
                    bw.Write(packNumBytes(mapSet[key].sel.X, 2));
                    bw.Write(packNumBytes(mapSet[key].sel.Y, 2));
                    bw.Write(packNumBytes(mapSet[key].col, 2));
                }
            //Number of object templates
            int numObjTemplates = objTemplates.Count;
            bw.Write(packNumBytes(numObjTemplates, 2));
            foreach (KeyValuePair<String, ObjectTemplate> curTemplate in objTemplates)
            {
                ImageStates curState = curTemplate.Value.imgStates;
                //Associate template name foreach template
                bw.Write(getStringBuffer(curTemplate.Key, 20));
                //Number of properties
                bw.Write(packNumBytes(curTemplate.Value.propertyDict.Count, 4));
                foreach (KeyValuePair<String, Property> curProperty in curTemplate.Value.propertyDict)
                {
                    //Property name foreach property
                    bw.Write(getStringBuffer(curProperty.Value.name, 20));
                    //Property datatype foreach property
                    bw.Write(getStringBuffer(curProperty.Value.dataType, 20));
                }
                //Number of states within that templated
                bw.Write(packNumBytes(curState.stateDict.Count, 2));
                foreach (KeyValuePair<String, Bitmap> curStateDict in curState.stateDict)
                {
                    //State name foreach state
                    bw.Write(getStringBuffer(curStateDict.Key, 20));
                    //Bitmap size foreach state
                    int imgStateSize = (int)getBitmapSize(curStateDict.Value);
                    bw.Write(imgStateSize);
                    //State bitmap foreach state
                    bw.Write(getBitmapBytes(curStateDict.Value));
                }
            }
            //Number of object instance
            bw.Write(packNumBytes(objInstList.Count, 2));
            foreach (ObjectInstance selObjInst in objInstList)
            {
                //Object name
                bw.Write(getStringBuffer(selObjInst.objName, 20));
                //Is visible
                bw.Write(packNumBytes(selObjInst.visible, 2));
                //InstX
                bw.Write(packNumBytes(selObjInst.pnt.X, 2));
                //InstY
                bw.Write(packNumBytes(selObjInst.pnt.Y, 2));
                //Number of Properties
                bw.Write(packNumBytes(selObjInst.propDict.Count, 2));
                foreach (KeyValuePair<String, Property> curProp in selObjInst.propDict)
                {
                    //Property name
                    bw.Write(getStringBuffer(curProp.Key, 20));
                    //Property data type
                    bw.Write(getStringBuffer(curProp.Value.dataType, 20));
                    //Use asset?
                    bw.Write(packNumBytes(curProp.Value.useAsset, 2));
                    //Asset name
                    bw.Write(getStringBuffer(curProp.Value.assetName, 20));
                    //Property size
                    bw.Write(curProp.Value.size);
                    //Property value
                    if (curProp.Value.size > 0)
                        bw.Write(curProp.Value.defaultValue);
                }
            }
            //Number of assets
            bw.Write(packNumBytes(assetDict.Count, 2));
            foreach (KeyValuePair<String, Asset> curAsset in assetDict)
            {
                //Asset name
                bw.Write(getStringBuffer(curAsset.Key, 20));
                //Asset data type
                bw.Write(getStringBuffer(curAsset.Value.dataType, 20));
                //Asset size
                bw.Write(packNumBytes(curAsset.Value.size, 4));
                //Asset bytes
                bw.Write(curAsset.Value.defaultValue);
            }
            fs.Close();
            bw.Close();
        }

        public void setTile(int curLayer, Point pnt, TileValue val)
        {
            if (pnt.X < colNum && pnt.Y < rowNum && pnt.X >= 0 && pnt.Y >= 0)
                layerSet[curLayer][pnt] = val;
            Console.WriteLine(layerSet[curLayer].Count());
        }

        public void addTile(Bitmap finalTile, Point pnt)
        {
            tileSet.tileImgs.Add(pnt, finalTile);
            Color avgColor = getAverageColor(finalTile);
            tileSet.averageColor.Add(pnt, avgColor);
        }

        public Color getAverageColor(Bitmap sample)
        {
            int avgR = 0;
            int avgB = 0;
            int avgG = 0;
            for (int i = 0; i < sample.Width; i++)
                for (int j = 0; j < sample.Height; j++)
                {
                    Color tempCol = sample.GetPixel(i, j);
                    avgR += tempCol.R;
                    avgB += tempCol.B;
                    avgG += tempCol.G;
                }
            avgR = avgR / (sample.Width * sample.Height);
            avgB = avgB / (sample.Width * sample.Height);
            avgG = avgG / (sample.Width * sample.Height);
            return Color.FromArgb(avgR, avgG, avgB);
        }

        public void addTile(Bitmap finalTile)
        {
            int pntCol = tileSet.maxCol;
            Point pnt;
            if (tileSet.maxCol + 1 == tileSet.col)
            {
                //pnt = new Point(tileSet.maxCol, tileSet.row - 1);
                tileSet.row += 1;
                tileSet.maxCol = 0;
            }
            else
            {
                tileSet.maxCol = tileSet.maxCol + 1;
            }
            pnt = new Point(tileSet.maxCol, tileSet.row - 1);
            tileSet.tileImgs.Add(pnt, finalTile);
            tileSet.averageColor.Add(pnt, getAverageColor(finalTile));
            //pnt = new Point(tileSet.maxCol, tileSet.row - 1);
            //pnt = new Point(tileSet.maxCol, tileSet.row - 1);

        }

        public void replaceTile(Point pnt, Bitmap newImg)
        {
            if (tileSet.tileImgs.ContainsKey(pnt))
            {
                tileSet.tileImgs[pnt] = newImg;
                tileSet.averageColor[pnt] = getAverageColor(newImg);
            }
        }

        public void removeTile(Point pnt)
        {
            int c = tileSet.col;
            int r = tileSet.row;
            int startY = pnt.Y;
            int startX = pnt.X;
            for (int i = startY; i < tileSet.row; i++)
            {
                for (int j = startX; j < tileSet.col; j++)
                {
                    int xKey = (j + 1) % c;
                    int yMod = i;
                    if (xKey == 0)
                    {
                        yMod += 1;
                    }
                    //int yKey = pnt.Y + 1 % r;
                    if (tileSet.tileImgs.ContainsKey(new Point(xKey, yMod)))
                    {
                        tileSet.tileImgs[new Point(j, i)] = tileSet.tileImgs[new Point(xKey, yMod)];
                    }
                    else
                    {
                        tileSet.tileImgs.Remove(new Point(j, i));
                    }
                    startY = 0;
                }
                startX = 0;
            }

            if (tileSet.maxCol == 0)
            {
                tileSet.row -= 1;
                tileSet.maxCol = tileSet.col - 1;
            }
            else
            {
                tileSet.maxCol -= 1;
            }
        }

        public void loadObjects(XmlDocument objDoc)
        {
            //Dictionary<String, ObjectTemplate> objTemplateCopy = new Dictionary<String, ObjectTemplate>(objTemplates); 
            //objTemplates.Clear();
            //XmlNodeList objElements = objDoc.DocumentElement.SelectNodes("//Object");
            //foreach (XmlNode objElement in objElements)
            //{
            //    String name = objElement.Attributes["name"].Value;
            //    Dictionary<String, Property> propDict = new Dictionary<String, Property>();
            //    XmlNodeList propElements = objElement.SelectNodes("Property");
            //    foreach (XmlNode propElement in propElements)
            //    {
            //        String propName = propElement.Attributes["name"].Value;
            //        String propType = propElement.Attributes["type"].Value;
            //        //String propValue = propElement.Attributes["value"].Value;
            //        propDict.Add(propName, new Property(propName, propType,0, null));
            //    }
            //    ImageStates addedState = new ImageStates();
            //    if (objTemplateCopy.ContainsKey(name))
            //        addedState = objTemplateCopy[name].imgStates;
            //    objTemplates.Add(objElement.Attributes["name"].Value, new ObjectTemplate(objElement.Attributes["name"].Value,propDict, addedState));
            //}
        }

        public XmlDocument saveObjects()
        {
            XmlDocument objDoc = new XmlDocument();
            //XmlElement rootElement = objDoc.CreateElement("ObjectTemplate");
            //objDoc.AppendChild(rootElement);
            //foreach (KeyValuePair<String, ObjectTemplate> objTemplate in objTemplates)
            //{
            //    XmlElement objElement = objDoc.CreateElement("Object");
            //    String objName = objTemplate.Key;
            //    objElement.SetAttribute("name", objName);
            //    foreach (KeyValuePair<String, Property> property in objTemplate.Value.propertyDict)
            //    {
            //        XmlElement propElement = objDoc.CreateElement("Property");
            //        String propName = property.Value.name;
            //        String propType = property.Value.dataType;
            //        Byte[] propValue = property.Value.defaultValue;
            //        propElement.SetAttribute("name", propName);
            //        propElement.SetAttribute("type", propType);
            //        //propElement.SetAttribute("value", propValue.ToString());
            //        objElement.AppendChild(propElement);
            //    }
            //    objDoc.DocumentElement.SelectNodes("//ObjectTemplate")[0].AppendChild(objElement);
            //}

            return objDoc;
        }

        public void colorSwap(bool isAlpha, Color from, Color to)
        {
            tileSet.colorSwap(isAlpha, from, to);
        }

        public void addNewLayer()
        {
            Dictionary<Point, TileValue> mapSet = new Dictionary<Point, TileValue>();
            for (int i = 0; i < colNum; i++)
                for (int j = 0; j < rowNum; j++)
                {
                    mapSet.Add(new Point(i, j), new TileValue(new Point(-1, -1), 0));
                }
            //layerSet.Add(mapSet);
            layerSet.Insert(0, mapSet);
        }

        public void removeTileVals(Point pnt)
        {
            for (int i = 0; i < layerSet.Count; i++)
                if (layerSet[i].ContainsKey(pnt))
                    layerSet[i].Remove(pnt);
        }

        public void expandTileVals(Point pnt)
        {
            for (int i = 0; i < layerSet.Count; i++)
                if (!layerSet[i].ContainsKey(pnt))
                    layerSet[i].Add(pnt, new TileValue(new Point(-1, -1), 0));
        }

        public bool tileIndexExists(Point pnt)
        {
            return tileSet.tileImgs.ContainsKey(pnt);
        }

        public TileValue getValue(int curLayer, Point pnt)
        {
            if (layerSet[curLayer].ContainsKey(pnt))
                return layerSet[curLayer][pnt];
            else
                return new TileValue(new Point(-1, -1), 0);
        }

        public int getTileCount()
        {
            return tileSet.tileImgs.Count;
        }

        public Bitmap getBitmapByIndex(Point pnt)
        {
            return tileSet.tileImgs[pnt];
        }

        public int pointToInt(Point pnt)
        {
            return tileSet.pointToInt(pnt);
        }

    }

}
