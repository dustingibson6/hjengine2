﻿namespace MapEditor
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Bitmap");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Audio");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("String");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("Integer");
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.newButton = new System.Windows.Forms.ToolStripButton();
            this.loadButton = new System.Windows.Forms.ToolStripButton();
            this.saveButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.resizeButton = new System.Windows.Forms.ToolStripButton();
            this.exportButton = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.objTree = new System.Windows.Forms.TreeView();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.addObjectButton = new System.Windows.Forms.ToolStripButton();
            this.editButton = new System.Windows.Forms.ToolStripButton();
            this.deleteObjectButton = new System.Windows.Forms.ToolStripButton();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.assetTree = new System.Windows.Forms.TreeView();
            this.toolStrip5 = new System.Windows.Forms.ToolStrip();
            this.addAssetButton = new System.Windows.Forms.ToolStripButton();
            this.editAssetButton = new System.Windows.Forms.ToolStripButton();
            this.deleteAssetButton = new System.Windows.Forms.ToolStripButton();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.layerListBox = new System.Windows.Forms.ListBox();
            this.toolStrip6 = new System.Windows.Forms.ToolStrip();
            this.addLayerButton = new System.Windows.Forms.ToolStripButton();
            this.removeLayerButton = new System.Windows.Forms.ToolStripButton();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.miniMapBox = new System.Windows.Forms.PictureBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tileSetBox = new System.Windows.Forms.PictureBox();
            this.toolStrip4 = new System.Windows.Forms.ToolStrip();
            this.addTileButton = new System.Windows.Forms.ToolStripButton();
            this.delTileButton = new System.Windows.Forms.ToolStripButton();
            this.replaceTileButton = new System.Windows.Forms.ToolStripButton();
            this.transButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.colText = new System.Windows.Forms.ToolStripTextBox();
            this.colButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.mapBox = new System.Windows.Forms.PictureBox();
            this.loadMapDlg = new System.Windows.Forms.FolderBrowserDialog();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.selectionButton = new System.Windows.Forms.ToolStripButton();
            this.placementButton = new System.Windows.Forms.ToolStripButton();
            this.deletionButton = new System.Windows.Forms.ToolStripButton();
            this.moveButton = new System.Windows.Forms.ToolStripButton();
            this.fillButton = new System.Windows.Forms.ToolStripButton();
            this.placeObjectButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.northButton = new System.Windows.Forms.ToolStripMenuItem();
            this.eastButton = new System.Windows.Forms.ToolStripMenuItem();
            this.southButton = new System.Windows.Forms.ToolStripMenuItem();
            this.westButton = new System.Windows.Forms.ToolStripMenuItem();
            this.collisionButton = new System.Windows.Forms.ToolStripButton();
            this.copyButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.collisionViewButton = new System.Windows.Forms.ToolStripButton();
            this.objectViewButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tileGroupButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.scaleText = new System.Windows.Forms.ToolStripTextBox();
            this.zoomButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.tileTemplateButton = new System.Windows.Forms.ToolStripButton();
            this.loadTileDlg = new System.Windows.Forms.OpenFileDialog();
            this.openReplaceTileDlg = new System.Windows.Forms.OpenFileDialog();
            this.openTileDlg = new System.Windows.Forms.OpenFileDialog();
            this.runButton = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.toolStrip3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.toolStrip5.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.toolStrip6.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.miniMapBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tileSetBox)).BeginInit();
            this.toolStrip4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mapBox)).BeginInit();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newButton,
            this.loadButton,
            this.saveButton,
            this.toolStripSeparator4,
            this.resizeButton,
            this.exportButton,
            this.runButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(869, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // newButton
            // 
            this.newButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newButton.Image = ((System.Drawing.Image)(resources.GetObject("newButton.Image")));
            this.newButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newButton.Name = "newButton";
            this.newButton.Size = new System.Drawing.Size(23, 22);
            this.newButton.Text = "New Map";
            this.newButton.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // loadButton
            // 
            this.loadButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.loadButton.Image = ((System.Drawing.Image)(resources.GetObject("loadButton.Image")));
            this.loadButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(23, 22);
            this.loadButton.Text = "Load Map";
            this.loadButton.Click += new System.EventHandler(this.loadButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveButton.Image = ((System.Drawing.Image)(resources.GetObject("saveButton.Image")));
            this.saveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(23, 22);
            this.saveButton.Text = "Save Map";
            this.saveButton.Click += new System.EventHandler(this.saveFolder_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // resizeButton
            // 
            this.resizeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.resizeButton.Image = ((System.Drawing.Image)(resources.GetObject("resizeButton.Image")));
            this.resizeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.resizeButton.Name = "resizeButton";
            this.resizeButton.Size = new System.Drawing.Size(23, 22);
            this.resizeButton.Text = "Resize";
            this.resizeButton.Click += new System.EventHandler(this.resizeButton_Click);
            // 
            // exportButton
            // 
            this.exportButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.exportButton.Image = ((System.Drawing.Image)(resources.GetObject("exportButton.Image")));
            this.exportButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.exportButton.Name = "exportButton";
            this.exportButton.Size = new System.Drawing.Size(23, 22);
            this.exportButton.Text = "toolStripButton1";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 50);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AutoScroll = true;
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Panel2.Resize += new System.EventHandler(this.splitContainer1_Panel2_Resize);
            this.splitContainer1.Size = new System.Drawing.Size(869, 492);
            this.splitContainer1.SplitterDistance = 170;
            this.splitContainer1.TabIndex = 1;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.tabControl1);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.tabControl2);
            this.splitContainer3.Size = new System.Drawing.Size(170, 492);
            this.splitContainer3.SplitterDistance = 253;
            this.splitContainer3.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(170, 253);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.objTree);
            this.tabPage1.Controls.Add(this.toolStrip3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(162, 227);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Objects";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // objTree
            // 
            this.objTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.objTree.Location = new System.Drawing.Point(3, 28);
            this.objTree.Name = "objTree";
            this.objTree.Size = new System.Drawing.Size(156, 196);
            this.objTree.TabIndex = 0;
            this.objTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.objTree_AfterSelect);
            // 
            // toolStrip3
            // 
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addObjectButton,
            this.editButton,
            this.deleteObjectButton});
            this.toolStrip3.Location = new System.Drawing.Point(3, 3);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.Size = new System.Drawing.Size(156, 25);
            this.toolStrip3.TabIndex = 1;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // addObjectButton
            // 
            this.addObjectButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.addObjectButton.Image = ((System.Drawing.Image)(resources.GetObject("addObjectButton.Image")));
            this.addObjectButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addObjectButton.Name = "addObjectButton";
            this.addObjectButton.Size = new System.Drawing.Size(23, 22);
            this.addObjectButton.Text = "Add Object";
            this.addObjectButton.Click += new System.EventHandler(this.addObjectButton_Click);
            // 
            // editButton
            // 
            this.editButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.editButton.Image = ((System.Drawing.Image)(resources.GetObject("editButton.Image")));
            this.editButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(23, 22);
            this.editButton.Text = "Edit Object";
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // deleteObjectButton
            // 
            this.deleteObjectButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deleteObjectButton.Image = ((System.Drawing.Image)(resources.GetObject("deleteObjectButton.Image")));
            this.deleteObjectButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deleteObjectButton.Name = "deleteObjectButton";
            this.deleteObjectButton.Size = new System.Drawing.Size(23, 22);
            this.deleteObjectButton.Text = "Remove Object";
            this.deleteObjectButton.Click += new System.EventHandler(this.deleteObjectButton_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.assetTree);
            this.tabPage2.Controls.Add(this.toolStrip5);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(162, 227);
            this.tabPage2.TabIndex = 2;
            this.tabPage2.Text = "Assets";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // assetTree
            // 
            this.assetTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.assetTree.Location = new System.Drawing.Point(3, 28);
            this.assetTree.Name = "assetTree";
            treeNode5.Name = "Node0";
            treeNode5.Text = "Bitmap";
            treeNode6.Name = "Node1";
            treeNode6.Text = "Audio";
            treeNode7.Name = "Node2";
            treeNode7.Text = "String";
            treeNode8.Name = "Node3";
            treeNode8.Text = "Integer";
            this.assetTree.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode5,
            treeNode6,
            treeNode7,
            treeNode8});
            this.assetTree.Size = new System.Drawing.Size(156, 196);
            this.assetTree.TabIndex = 7;
            // 
            // toolStrip5
            // 
            this.toolStrip5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addAssetButton,
            this.editAssetButton,
            this.deleteAssetButton});
            this.toolStrip5.Location = new System.Drawing.Point(3, 3);
            this.toolStrip5.Name = "toolStrip5";
            this.toolStrip5.Size = new System.Drawing.Size(156, 25);
            this.toolStrip5.TabIndex = 6;
            this.toolStrip5.Text = "toolStrip5";
            // 
            // addAssetButton
            // 
            this.addAssetButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.addAssetButton.Image = ((System.Drawing.Image)(resources.GetObject("addAssetButton.Image")));
            this.addAssetButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addAssetButton.Name = "addAssetButton";
            this.addAssetButton.Size = new System.Drawing.Size(23, 22);
            this.addAssetButton.Text = "Add Asset";
            this.addAssetButton.Click += new System.EventHandler(this.addAssetButton_Click);
            // 
            // editAssetButton
            // 
            this.editAssetButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.editAssetButton.Image = ((System.Drawing.Image)(resources.GetObject("editAssetButton.Image")));
            this.editAssetButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editAssetButton.Name = "editAssetButton";
            this.editAssetButton.Size = new System.Drawing.Size(23, 22);
            this.editAssetButton.Text = "Edit Asset";
            // 
            // deleteAssetButton
            // 
            this.deleteAssetButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deleteAssetButton.Image = ((System.Drawing.Image)(resources.GetObject("deleteAssetButton.Image")));
            this.deleteAssetButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deleteAssetButton.Name = "deleteAssetButton";
            this.deleteAssetButton.Size = new System.Drawing.Size(23, 22);
            this.deleteAssetButton.Text = "Remove Asset";
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(0, 0);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(170, 235);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.layerListBox);
            this.tabPage4.Controls.Add(this.toolStrip6);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(162, 209);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Layers";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // layerListBox
            // 
            this.layerListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layerListBox.FormattingEnabled = true;
            this.layerListBox.Location = new System.Drawing.Point(3, 28);
            this.layerListBox.Name = "layerListBox";
            this.layerListBox.Size = new System.Drawing.Size(156, 178);
            this.layerListBox.TabIndex = 1;
            this.layerListBox.SelectedIndexChanged += new System.EventHandler(this.layerListBox_SelectedIndexChanged);
            // 
            // toolStrip6
            // 
            this.toolStrip6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addLayerButton,
            this.removeLayerButton});
            this.toolStrip6.Location = new System.Drawing.Point(3, 3);
            this.toolStrip6.Name = "toolStrip6";
            this.toolStrip6.Size = new System.Drawing.Size(156, 25);
            this.toolStrip6.TabIndex = 0;
            this.toolStrip6.Text = "toolStrip6";
            // 
            // addLayerButton
            // 
            this.addLayerButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.addLayerButton.Image = ((System.Drawing.Image)(resources.GetObject("addLayerButton.Image")));
            this.addLayerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addLayerButton.Name = "addLayerButton";
            this.addLayerButton.Size = new System.Drawing.Size(23, 22);
            this.addLayerButton.Text = "Add Object";
            this.addLayerButton.Click += new System.EventHandler(this.addLayerButton_Click);
            // 
            // removeLayerButton
            // 
            this.removeLayerButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.removeLayerButton.Image = ((System.Drawing.Image)(resources.GetObject("removeLayerButton.Image")));
            this.removeLayerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.removeLayerButton.Name = "removeLayerButton";
            this.removeLayerButton.Size = new System.Drawing.Size(23, 22);
            this.removeLayerButton.Text = "Remove Object";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.miniMapBox);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(162, 209);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Mini-Map";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // miniMapBox
            // 
            this.miniMapBox.Location = new System.Drawing.Point(3, 3);
            this.miniMapBox.Name = "miniMapBox";
            this.miniMapBox.Size = new System.Drawing.Size(156, 203);
            this.miniMapBox.TabIndex = 0;
            this.miniMapBox.TabStop = false;
            this.miniMapBox.Click += new System.EventHandler(this.miniMapBox_Click);
            this.miniMapBox.Paint += new System.Windows.Forms.PaintEventHandler(this.miniMapBox_Paint);
            this.miniMapBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.miniMapBox_MouseMove);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.AutoScroll = true;
            this.splitContainer2.Panel1.Controls.Add(this.panel1);
            this.splitContainer2.Panel1.Controls.Add(this.toolStrip4);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.AutoScroll = true;
            this.splitContainer2.Panel2.Controls.Add(this.mapBox);
            this.splitContainer2.Size = new System.Drawing.Size(695, 492);
            this.splitContainer2.SplitterDistance = 231;
            this.splitContainer2.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.tileSetBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(231, 467);
            this.panel1.TabIndex = 2;
            this.panel1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.panel1_Scroll);
            // 
            // tileSetBox
            // 
            this.tileSetBox.Location = new System.Drawing.Point(3, 3);
            this.tileSetBox.Name = "tileSetBox";
            this.tileSetBox.Size = new System.Drawing.Size(100, 400);
            this.tileSetBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.tileSetBox.TabIndex = 0;
            this.tileSetBox.TabStop = false;
            this.tileSetBox.Click += new System.EventHandler(this.tileSetBox_Click);
            this.tileSetBox.Paint += new System.Windows.Forms.PaintEventHandler(this.tileSetBox_Paint);
            this.tileSetBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.tileSetBox_MouseMove);
            // 
            // toolStrip4
            // 
            this.toolStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addTileButton,
            this.delTileButton,
            this.replaceTileButton,
            this.transButton,
            this.toolStripLabel2,
            this.colText,
            this.colButton,
            this.toolStripButton2});
            this.toolStrip4.Location = new System.Drawing.Point(0, 0);
            this.toolStrip4.Name = "toolStrip4";
            this.toolStrip4.Size = new System.Drawing.Size(231, 25);
            this.toolStrip4.TabIndex = 1;
            this.toolStrip4.Text = "toolStrip4";
            // 
            // addTileButton
            // 
            this.addTileButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.addTileButton.Image = ((System.Drawing.Image)(resources.GetObject("addTileButton.Image")));
            this.addTileButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addTileButton.Name = "addTileButton";
            this.addTileButton.Size = new System.Drawing.Size(23, 22);
            this.addTileButton.Text = "Add Tile";
            this.addTileButton.Click += new System.EventHandler(this.addTileButton_Click);
            // 
            // delTileButton
            // 
            this.delTileButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.delTileButton.Image = ((System.Drawing.Image)(resources.GetObject("delTileButton.Image")));
            this.delTileButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.delTileButton.Name = "delTileButton";
            this.delTileButton.Size = new System.Drawing.Size(23, 22);
            this.delTileButton.Text = "Remove Tile";
            this.delTileButton.Click += new System.EventHandler(this.delTileButton_Click);
            // 
            // replaceTileButton
            // 
            this.replaceTileButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.replaceTileButton.Image = ((System.Drawing.Image)(resources.GetObject("replaceTileButton.Image")));
            this.replaceTileButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.replaceTileButton.Name = "replaceTileButton";
            this.replaceTileButton.Size = new System.Drawing.Size(23, 22);
            this.replaceTileButton.Text = "Replace Tile";
            this.replaceTileButton.Click += new System.EventHandler(this.replaceTileButton_Click);
            // 
            // transButton
            // 
            this.transButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.transButton.Image = ((System.Drawing.Image)(resources.GetObject("transButton.Image")));
            this.transButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.transButton.Name = "transButton";
            this.transButton.Size = new System.Drawing.Size(23, 22);
            this.transButton.Text = "toolStripButton1";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(26, 22);
            this.toolStripLabel2.Text = "col:";
            // 
            // colText
            // 
            this.colText.Name = "colText";
            this.colText.Size = new System.Drawing.Size(30, 25);
            // 
            // colButton
            // 
            this.colButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.colButton.Image = ((System.Drawing.Image)(resources.GetObject("colButton.Image")));
            this.colButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.colButton.Name = "colButton";
            this.colButton.Size = new System.Drawing.Size(23, 22);
            this.colButton.Text = "toolStripButton2";
            this.colButton.Click += new System.EventHandler(this.colButton_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "toolStripButton2";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // mapBox
            // 
            this.mapBox.Location = new System.Drawing.Point(3, 3);
            this.mapBox.Name = "mapBox";
            this.mapBox.Size = new System.Drawing.Size(200, 200);
            this.mapBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.mapBox.TabIndex = 0;
            this.mapBox.TabStop = false;
            this.mapBox.Click += new System.EventHandler(this.mapBox_Click);
            this.mapBox.Paint += new System.Windows.Forms.PaintEventHandler(this.mapBox_Paint);
            this.mapBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.mapBox_MouseDown);
            this.mapBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.mapBox_MouseMove);
            this.mapBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.mapBox_MouseUp);
            this.mapBox.Move += new System.EventHandler(this.mapBox_Move);
            // 
            // loadMapDlg
            // 
            this.loadMapDlg.HelpRequest += new System.EventHandler(this.loadMapDlg_HelpRequest);
            // 
            // toolStrip2
            // 
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectionButton,
            this.placementButton,
            this.deletionButton,
            this.moveButton,
            this.fillButton,
            this.placeObjectButton,
            this.toolStripSeparator2,
            this.toolStripDropDownButton1,
            this.collisionButton,
            this.copyButton,
            this.toolStripSeparator1,
            this.collisionViewButton,
            this.objectViewButton,
            this.toolStripSeparator3,
            this.tileGroupButton,
            this.toolStripSeparator5,
            this.toolStripLabel1,
            this.scaleText,
            this.zoomButton,
            this.toolStripSeparator6,
            this.tileTemplateButton});
            this.toolStrip2.Location = new System.Drawing.Point(0, 25);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(869, 25);
            this.toolStrip2.TabIndex = 2;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // selectionButton
            // 
            this.selectionButton.Checked = true;
            this.selectionButton.CheckOnClick = true;
            this.selectionButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.selectionButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.selectionButton.Image = ((System.Drawing.Image)(resources.GetObject("selectionButton.Image")));
            this.selectionButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.selectionButton.Name = "selectionButton";
            this.selectionButton.Size = new System.Drawing.Size(23, 22);
            this.selectionButton.Text = "Select Mode";
            this.selectionButton.Click += new System.EventHandler(this.selectionButton_Click);
            // 
            // placementButton
            // 
            this.placementButton.CheckOnClick = true;
            this.placementButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.placementButton.Image = ((System.Drawing.Image)(resources.GetObject("placementButton.Image")));
            this.placementButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.placementButton.Name = "placementButton";
            this.placementButton.Size = new System.Drawing.Size(23, 22);
            this.placementButton.Text = "Placement Mode";
            this.placementButton.Click += new System.EventHandler(this.placementButton_Click);
            // 
            // deletionButton
            // 
            this.deletionButton.CheckOnClick = true;
            this.deletionButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deletionButton.Image = ((System.Drawing.Image)(resources.GetObject("deletionButton.Image")));
            this.deletionButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deletionButton.Name = "deletionButton";
            this.deletionButton.Size = new System.Drawing.Size(23, 22);
            this.deletionButton.Text = "Remove";
            this.deletionButton.Click += new System.EventHandler(this.deletionButton_Click);
            // 
            // moveButton
            // 
            this.moveButton.CheckOnClick = true;
            this.moveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.moveButton.Image = ((System.Drawing.Image)(resources.GetObject("moveButton.Image")));
            this.moveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.moveButton.Name = "moveButton";
            this.moveButton.Size = new System.Drawing.Size(23, 22);
            this.moveButton.Text = "Move";
            this.moveButton.Click += new System.EventHandler(this.moveButton_Click);
            // 
            // fillButton
            // 
            this.fillButton.CheckOnClick = true;
            this.fillButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.fillButton.Image = ((System.Drawing.Image)(resources.GetObject("fillButton.Image")));
            this.fillButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fillButton.Name = "fillButton";
            this.fillButton.Size = new System.Drawing.Size(23, 22);
            this.fillButton.Text = "Fill";
            this.fillButton.Click += new System.EventHandler(this.fillButton_Click);
            // 
            // placeObjectButton
            // 
            this.placeObjectButton.CheckOnClick = true;
            this.placeObjectButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.placeObjectButton.Image = ((System.Drawing.Image)(resources.GetObject("placeObjectButton.Image")));
            this.placeObjectButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.placeObjectButton.Name = "placeObjectButton";
            this.placeObjectButton.Size = new System.Drawing.Size(23, 22);
            this.placeObjectButton.Text = "Place Object";
            this.placeObjectButton.Click += new System.EventHandler(this.placeObjectButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.northButton,
            this.eastButton,
            this.southButton,
            this.westButton});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(29, 22);
            this.toolStripDropDownButton1.Text = "Collision Modes";
            // 
            // northButton
            // 
            this.northButton.CheckOnClick = true;
            this.northButton.Name = "northButton";
            this.northButton.Size = new System.Drawing.Size(105, 22);
            this.northButton.Text = "North";
            // 
            // eastButton
            // 
            this.eastButton.CheckOnClick = true;
            this.eastButton.Name = "eastButton";
            this.eastButton.Size = new System.Drawing.Size(105, 22);
            this.eastButton.Text = "East";
            // 
            // southButton
            // 
            this.southButton.CheckOnClick = true;
            this.southButton.Name = "southButton";
            this.southButton.Size = new System.Drawing.Size(105, 22);
            this.southButton.Text = "South";
            // 
            // westButton
            // 
            this.westButton.CheckOnClick = true;
            this.westButton.Name = "westButton";
            this.westButton.Size = new System.Drawing.Size(105, 22);
            this.westButton.Text = "West";
            // 
            // collisionButton
            // 
            this.collisionButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.collisionButton.Image = ((System.Drawing.Image)(resources.GetObject("collisionButton.Image")));
            this.collisionButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.collisionButton.Name = "collisionButton";
            this.collisionButton.Size = new System.Drawing.Size(23, 22);
            this.collisionButton.Text = "Add Collision";
            this.collisionButton.Click += new System.EventHandler(this.collisionButton_Click);
            // 
            // copyButton
            // 
            this.copyButton.CheckOnClick = true;
            this.copyButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.copyButton.Image = ((System.Drawing.Image)(resources.GetObject("copyButton.Image")));
            this.copyButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyButton.Name = "copyButton";
            this.copyButton.Size = new System.Drawing.Size(23, 22);
            this.copyButton.Text = "Copy";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // collisionViewButton
            // 
            this.collisionViewButton.CheckOnClick = true;
            this.collisionViewButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.collisionViewButton.Image = ((System.Drawing.Image)(resources.GetObject("collisionViewButton.Image")));
            this.collisionViewButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.collisionViewButton.Name = "collisionViewButton";
            this.collisionViewButton.Size = new System.Drawing.Size(23, 22);
            this.collisionViewButton.Text = "Collision View";
            // 
            // objectViewButton
            // 
            this.objectViewButton.Checked = true;
            this.objectViewButton.CheckOnClick = true;
            this.objectViewButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.objectViewButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.objectViewButton.Image = ((System.Drawing.Image)(resources.GetObject("objectViewButton.Image")));
            this.objectViewButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.objectViewButton.Name = "objectViewButton";
            this.objectViewButton.Size = new System.Drawing.Size(23, 22);
            this.objectViewButton.Text = "Object Layer";
            this.objectViewButton.Click += new System.EventHandler(this.objectViewButton_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // tileGroupButton
            // 
            this.tileGroupButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tileGroupButton.Image = ((System.Drawing.Image)(resources.GetObject("tileGroupButton.Image")));
            this.tileGroupButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tileGroupButton.Name = "tileGroupButton";
            this.tileGroupButton.Size = new System.Drawing.Size(23, 22);
            this.tileGroupButton.Text = "toolStripButton1";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(42, 22);
            this.toolStripLabel1.Text = "Zoom:";
            // 
            // scaleText
            // 
            this.scaleText.Name = "scaleText";
            this.scaleText.Size = new System.Drawing.Size(30, 25);
            this.scaleText.Click += new System.EventHandler(this.scaleText_Click);
            // 
            // zoomButton
            // 
            this.zoomButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.zoomButton.Image = ((System.Drawing.Image)(resources.GetObject("zoomButton.Image")));
            this.zoomButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.zoomButton.Name = "zoomButton";
            this.zoomButton.Size = new System.Drawing.Size(23, 22);
            this.zoomButton.Text = "Zoom";
            this.zoomButton.Click += new System.EventHandler(this.zoomButton_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // tileTemplateButton
            // 
            this.tileTemplateButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tileTemplateButton.Image = ((System.Drawing.Image)(resources.GetObject("tileTemplateButton.Image")));
            this.tileTemplateButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tileTemplateButton.Name = "tileTemplateButton";
            this.tileTemplateButton.Size = new System.Drawing.Size(23, 22);
            this.tileTemplateButton.Text = "Tile Template";
            this.tileTemplateButton.Click += new System.EventHandler(this.tileTemplateButton_Click);
            // 
            // loadTileDlg
            // 
            this.loadTileDlg.FileName = "openFileDialog1";
            this.loadTileDlg.FileOk += new System.ComponentModel.CancelEventHandler(this.loadTileDlg_FileOk);
            // 
            // openReplaceTileDlg
            // 
            this.openReplaceTileDlg.FileName = "openFileDialog1";
            // 
            // runButton
            // 
            this.runButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.runButton.Image = ((System.Drawing.Image)(resources.GetObject("runButton.Image")));
            this.runButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(23, 22);
            this.runButton.Text = "Run";
            this.runButton.Click += new System.EventHandler(this.runButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 542);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip2);
            this.Controls.Add(this.toolStrip1);
            this.Name = "MainForm";
            this.Text = "Map Editor";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.toolStrip5.ResumeLayout(false);
            this.toolStrip5.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.toolStrip6.ResumeLayout(false);
            this.toolStrip6.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.miniMapBox)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tileSetBox)).EndInit();
            this.toolStrip4.ResumeLayout(false);
            this.toolStrip4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mapBox)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton newButton;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.PictureBox tileSetBox;
        private System.Windows.Forms.PictureBox mapBox;
        private System.Windows.Forms.ToolStripButton loadButton;
        private System.Windows.Forms.FolderBrowserDialog loadMapDlg;
        private System.Windows.Forms.ToolStripButton saveButton;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton selectionButton;
        private System.Windows.Forms.ToolStripButton placementButton;
        private System.Windows.Forms.ToolStripButton deletionButton;
        private System.Windows.Forms.ToolStripButton moveButton;
        private System.Windows.Forms.ToolStripButton fillButton;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TreeView objTree;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton addObjectButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem northButton;
        private System.Windows.Forms.ToolStripMenuItem eastButton;
        private System.Windows.Forms.ToolStripMenuItem southButton;
        private System.Windows.Forms.ToolStripMenuItem westButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton collisionViewButton;
        private System.Windows.Forms.ToolStripButton collisionButton;
        private System.Windows.Forms.ToolStripButton objectViewButton;
        private System.Windows.Forms.ToolStrip toolStrip4;
        private System.Windows.Forms.ToolStripButton addTileButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tileGroupButton;
        private System.Windows.Forms.OpenFileDialog loadTileDlg;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripButton delTileButton;
        private System.Windows.Forms.ToolStripButton replaceTileButton;
        private System.Windows.Forms.OpenFileDialog openReplaceTileDlg;
        private System.Windows.Forms.ToolStripButton editButton;
        private System.Windows.Forms.ToolStripButton deleteObjectButton;
        private System.Windows.Forms.ToolStripButton placeObjectButton;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.ToolStripButton transButton;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TreeView assetTree;
        private System.Windows.Forms.ToolStrip toolStrip5;
        private System.Windows.Forms.ToolStripButton addAssetButton;
        private System.Windows.Forms.ToolStripButton editAssetButton;
        private System.Windows.Forms.ToolStripButton deleteAssetButton;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.PictureBox miniMapBox;
        private System.Windows.Forms.ListBox layerListBox;
        private System.Windows.Forms.ToolStrip toolStrip6;
        private System.Windows.Forms.ToolStripButton addLayerButton;
        private System.Windows.Forms.ToolStripButton removeLayerButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton resizeButton;
        private System.Windows.Forms.ToolStripButton exportButton;
        private System.Windows.Forms.ToolStripButton copyButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripTextBox scaleText;
        private System.Windows.Forms.ToolStripButton zoomButton;
        private System.Windows.Forms.ToolStripTextBox colText;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripButton colButton;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton tileTemplateButton;
        private System.Windows.Forms.OpenFileDialog openTileDlg;
        private System.Windows.Forms.ToolStripButton runButton;
    }
}

