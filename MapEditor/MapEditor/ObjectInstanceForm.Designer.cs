﻿namespace MapEditor
{
    partial class ObjectInstanceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.propertyNameText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.nameText = new System.Windows.Forms.TextBox();
            this.propertyList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.dataTypeCol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.okButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.dataBrowseButton = new System.Windows.Forms.Button();
            this.valueText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.sizeNum = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.openDataFileDlg = new System.Windows.Forms.OpenFileDialog();
            this.previewBox = new System.Windows.Forms.PictureBox();
            this.assetCheckBox = new System.Windows.Forms.CheckBox();
            this.assetBox = new System.Windows.Forms.ComboBox();
            this.visibleCheck = new System.Windows.Forms.CheckBox();
            this.startButton = new System.Windows.Forms.Button();
            this.stopButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.sizeNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.previewBox)).BeginInit();
            this.SuspendLayout();
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(20, 425);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 20;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 279);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Property Name:";
            // 
            // propertyNameText
            // 
            this.propertyNameText.Enabled = false;
            this.propertyNameText.Location = new System.Drawing.Point(137, 276);
            this.propertyNameText.Name = "propertyNameText";
            this.propertyNameText.Size = new System.Drawing.Size(308, 20);
            this.propertyNameText.TabIndex = 16;
            this.propertyNameText.TextChanged += new System.EventHandler(this.propertyNameText_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Name:";
            // 
            // nameText
            // 
            this.nameText.Enabled = false;
            this.nameText.Location = new System.Drawing.Point(83, 22);
            this.nameText.Name = "nameText";
            this.nameText.Size = new System.Drawing.Size(177, 20);
            this.nameText.TabIndex = 14;
            // 
            // propertyList
            // 
            this.propertyList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.dataTypeCol,
            this.columnHeader3});
            this.propertyList.FullRowSelect = true;
            this.propertyList.GridLines = true;
            this.propertyList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.propertyList.Location = new System.Drawing.Point(30, 68);
            this.propertyList.Name = "propertyList";
            this.propertyList.Size = new System.Drawing.Size(440, 184);
            this.propertyList.TabIndex = 13;
            this.propertyList.UseCompatibleStateImageBehavior = false;
            this.propertyList.View = System.Windows.Forms.View.Details;
            this.propertyList.SelectedIndexChanged += new System.EventHandler(this.propertyList_SelectedIndexChanged);
            this.propertyList.DoubleClick += new System.EventHandler(this.propertyList_DoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Property Name";
            this.columnHeader1.Width = 130;
            // 
            // dataTypeCol
            // 
            this.dataTypeCol.DisplayIndex = 2;
            this.dataTypeCol.Text = "Data Type";
            this.dataTypeCol.Width = 170;
            // 
            // columnHeader3
            // 
            this.columnHeader3.DisplayIndex = 1;
            this.columnHeader3.Text = "Value";
            this.columnHeader3.Width = 135;
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(163, 482);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 24;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(267, 482);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(75, 23);
            this.CancelButton.TabIndex = 25;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // dataBrowseButton
            // 
            this.dataBrowseButton.Location = new System.Drawing.Point(373, 314);
            this.dataBrowseButton.Name = "dataBrowseButton";
            this.dataBrowseButton.Size = new System.Drawing.Size(75, 23);
            this.dataBrowseButton.TabIndex = 28;
            this.dataBrowseButton.Text = "Browse";
            this.dataBrowseButton.UseVisualStyleBackColor = true;
            this.dataBrowseButton.Click += new System.EventHandler(this.dataBrowseButton_Click);
            // 
            // valueText
            // 
            this.valueText.Location = new System.Drawing.Point(137, 314);
            this.valueText.Name = "valueText";
            this.valueText.Size = new System.Drawing.Size(227, 20);
            this.valueText.TabIndex = 27;
            this.valueText.TextChanged += new System.EventHandler(this.valueText_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(51, 322);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Default Value";
            // 
            // sizeNum
            // 
            this.sizeNum.Enabled = false;
            this.sizeNum.Location = new System.Drawing.Point(201, 425);
            this.sizeNum.Maximum = new decimal(new int[] {
            1316134911,
            2328,
            0,
            0});
            this.sizeNum.Name = "sizeNum";
            this.sizeNum.Size = new System.Drawing.Size(120, 20);
            this.sizeNum.TabIndex = 30;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(134, 430);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 13);
            this.label8.TabIndex = 29;
            this.label8.Text = "Size";
            // 
            // openDataFileDlg
            // 
            this.openDataFileDlg.FileName = "openFileDialog1";
            // 
            // previewBox
            // 
            this.previewBox.Location = new System.Drawing.Point(357, 419);
            this.previewBox.Name = "previewBox";
            this.previewBox.Size = new System.Drawing.Size(113, 86);
            this.previewBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.previewBox.TabIndex = 31;
            this.previewBox.TabStop = false;
            // 
            // assetCheckBox
            // 
            this.assetCheckBox.AutoSize = true;
            this.assetCheckBox.Location = new System.Drawing.Point(42, 364);
            this.assetCheckBox.Name = "assetCheckBox";
            this.assetCheckBox.Size = new System.Drawing.Size(74, 17);
            this.assetCheckBox.TabIndex = 32;
            this.assetCheckBox.Text = "Use Asset";
            this.assetCheckBox.UseVisualStyleBackColor = true;
            this.assetCheckBox.CheckedChanged += new System.EventHandler(this.assetCheckBox_CheckedChanged);
            // 
            // assetBox
            // 
            this.assetBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.assetBox.FormattingEnabled = true;
            this.assetBox.Location = new System.Drawing.Point(126, 362);
            this.assetBox.Name = "assetBox";
            this.assetBox.Size = new System.Drawing.Size(308, 21);
            this.assetBox.TabIndex = 33;
            this.assetBox.SelectedIndexChanged += new System.EventHandler(this.assetBox_SelectedIndexChanged);
            // 
            // visibleCheck
            // 
            this.visibleCheck.AutoSize = true;
            this.visibleCheck.Location = new System.Drawing.Point(320, 25);
            this.visibleCheck.Name = "visibleCheck";
            this.visibleCheck.Size = new System.Drawing.Size(55, 17);
            this.visibleCheck.TabIndex = 34;
            this.visibleCheck.Text = "visible";
            this.visibleCheck.UseVisualStyleBackColor = true;
            // 
            // startButton
            // 
            this.startButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.startButton.Location = new System.Drawing.Point(12, 475);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(40, 30);
            this.startButton.TabIndex = 35;
            this.startButton.Text = "Play";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // stopButton
            // 
            this.stopButton.Location = new System.Drawing.Point(58, 475);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(40, 30);
            this.stopButton.TabIndex = 36;
            this.stopButton.Text = "Stop";
            this.stopButton.UseVisualStyleBackColor = true;
            // 
            // ObjectInstanceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(505, 517);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.visibleCheck);
            this.Controls.Add(this.assetBox);
            this.Controls.Add(this.assetCheckBox);
            this.Controls.Add(this.previewBox);
            this.Controls.Add(this.sizeNum);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.dataBrowseButton);
            this.Controls.Add(this.valueText);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.propertyNameText);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nameText);
            this.Controls.Add(this.propertyList);
            this.Name = "ObjectInstanceForm";
            this.Text = "ObjectInstance";
            this.Load += new System.EventHandler(this.ObjectInstanceForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sizeNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.previewBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox propertyNameText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nameText;
        private System.Windows.Forms.ListView propertyList;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button dataBrowseButton;
        private System.Windows.Forms.TextBox valueText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown sizeNum;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ColumnHeader dataTypeCol;
        private System.Windows.Forms.OpenFileDialog openDataFileDlg;
        private System.Windows.Forms.PictureBox previewBox;
        private System.Windows.Forms.CheckBox assetCheckBox;
        private System.Windows.Forms.ComboBox assetBox;
        private System.Windows.Forms.CheckBox visibleCheck;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button stopButton;
    }
}