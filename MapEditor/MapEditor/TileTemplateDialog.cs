﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MapEditor
{
    public partial class TileTemplateDialog : Form
    {
        public String mode;
        public Dictionary<Point, Point> selList;
        public TileSet tileSet;
        public TileTemplate tileTemplate;
        private int maxX;
        private int maxY;

        public TileTemplateDialog(String mode,Dictionary<Point,Point> selList,TileSet tileSet)
        {
            InitializeComponent();
            this.mode = mode;
            this.selList = selList;
            this.tileTemplate = new TileTemplate();
            this.tileSet = tileSet;
            List<Point> valueList = new List<Point>(selList.Keys);
            maxX = valueList[0].X;
            maxY = valueList[0].Y;
            for (int i = 0; i < valueList.Count(); i++)
            {
                if (maxX <= valueList[i].X)
                    maxX = valueList[i].X;
                if (maxY <= valueList[i].Y)
                    maxY = valueList[i].Y;
            }
            previewBox.Refresh();
        }

        private void loadList()
        {

        }

        private void TileTemplateDialog_Load(object sender, EventArgs e)
        {

        }

        private void addButton_Click(object sender, EventArgs e)
        {
            String name = nameText.Text;
            if (!tileTemplate.keyTemplateExist(name))
                tileTemplate.addTileTemplate(name,selList);
        }

        private void previewBox_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            foreach (KeyValuePair<Point, Point> keyPoint in selList)
            {
                int x = maxX - keyPoint.Key.X;
                int y = maxY - keyPoint.Key.Y;
                g.DrawImage( tileSet.tileImgs[keyPoint.Value],new Point(x,y) );
            }
        }
    }
}
