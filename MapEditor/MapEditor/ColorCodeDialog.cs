﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MapEditor
{
    public partial class ColorCodeDialog : Form
    {
        public Color fromColor;
        public Color toColor;
        public bool isAlpha;

        public ColorCodeDialog()
        {
            isAlpha = true;
            InitializeComponent();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            fromColor = Color.FromArgb((int)fromAlpha.Value,(int)fromRed.Value, (int)fromGreen.Value, (int)fromBlue.Value);
            toColor = Color.FromArgb((int)toAlpha.Value,(int)toRed.Value, (int)toGreen.Value, (int)toBlue.Value);
            if (alphaCheckBox.Checked)
                isAlpha = true;
            else
                isAlpha = false;
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void ColorCodeDialog_Load(object sender, EventArgs e)
        {

        }

    }
}
