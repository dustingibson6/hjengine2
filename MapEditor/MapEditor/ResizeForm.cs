﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MapEditor
{
    public partial class ResizeForm : Form
    {
        public int width;
        public int height;

        public ResizeForm(int width, int height)
        {
            InitializeComponent();
            widthNum.Value = width;
            heightNum.Value = height;
        }

        private void ResizeForm_Load(object sender, EventArgs e)
        {

        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.width = (int)widthNum.Value;
            this.height = (int)heightNum.Value;
            this.DialogResult = DialogResult.OK;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
