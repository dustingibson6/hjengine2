﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace MapEditor
{
    public class ImageStates
    {

        public Dictionary<String, Bitmap> stateDict;

        public ImageStates()
        {
            stateDict = new Dictionary<String, Bitmap>();
        }

        public void addState(String stateName, Bitmap img)
        {
            stateDict.Add(stateName, img);
        }

        public void modifyImage(String stateName, Bitmap img)
        {
            if (stateDict.ContainsKey(stateName))
            {
                stateDict[stateName] = img;
            }
        }

        public Boolean nameExists(String stateName)
        {
            if(stateDict.ContainsKey(stateName))
            {
                return true;
            }
            return false;
        }

        public Bitmap getImg(String stateName)
        {
            if (stateDict.ContainsKey(stateName))
            {
                return stateDict[stateName];
            }
            return null;
        }

        public void modifyName(String oldStateName, String newStateName)
        {
            Bitmap oldImg = getImg(oldStateName);
            if (oldImg != null)
            {
                delState(oldStateName);
                addState(newStateName, oldImg);
            }

        }

        public void delState(String stateName)
        {
            if (stateDict.ContainsKey(stateName))
            {
                stateDict.Remove(stateName);
            }
        }

        public int numStates(String stateName)
        {
            return stateDict.Count();
        }

    }
}
