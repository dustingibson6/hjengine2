﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Windows.Forms;

namespace MapEditor
{
    public partial class MainForm : Form
    {
        private Dictionary<Point,Point> selList;
        private List<List<TileValue>> copyList;
        private TileMap tileMap;
        private Boolean loaded;
        private Boolean clicked;
        private int mx;
        private int my;
        private int mmx;
        private int mmy;
        private double scale;
        private TileValue tileSetSel;
        private int dragX1;
        private int dragY1;
        private int selW;
        private int selH;
        private int curLayer;
        private Point initSelPnt;
        private Point tileSelPnt;
        private String folderPath;
        private ObjectTemplate selObjTemplate;

        //private Bitmap canvasImg;

        public MainForm()
        {
            loaded = false;
            mx = 0;
            my = 0;
            mmx = 0;
            mmy = 0;
            scale = 1.0;
            tileSetSel = new TileValue(new Point(-1,-1), 0);
            selList = new Dictionary<Point,Point>();
            copyList = new List<List<TileValue>>();
            InitializeComponent();
        }

        private void loadMap()
        {
            //canvasImg = new Bitmap(tileMap.rowNum * tileMap.tileWidth, tileMap.colNum * tileMap.tileHeight);
            //mapBox.BackgroundImage = (Image)canvasImg;
            loaded = true;
            mapBox.Width = tileMap.colNum * tileMap.tileWidth;
            mapBox.Height = tileMap.rowNum * tileMap.tileHeight;
            updateList();
            mapBox.Refresh();
            tileSetBox.Refresh();
            //miniMapBox.Refresh();
        }

        private void drawminiMap()
        {
            Bitmap miniBitmap = new Bitmap(tileMap.colNum, tileMap.rowNum, PixelFormat.Format32bppArgb);
            for (int k = tileMap.layerSet.Count - 1; k >= 0; k--)
                for(int i=0; i < tileMap.colNum; i++)
                    for (int j = 0; j < tileMap.rowNum; j++)
                    {
                        Point selPoint = tileMap.layerSet[k][new Point(i, j)].sel;
                        Color avgColor = Color.FromArgb(0, 0, 0);
                        if (selPoint.X >= 0 && selPoint.Y >= 0)
                        {
                            avgColor = tileMap.tileSet.averageColor[selPoint];
                            miniBitmap.SetPixel(i, j, avgColor);
                        }
                    }
            //miniMapBox.Image = miniBitmap;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            NewMap newMapDlg = new NewMap();
            if (newMapDlg.ShowDialog() == DialogResult.OK)
            {
                folderPath = newMapDlg.folderPath;
                //XmlDocument objDoc = new XmlDocument();
                this.tileMap = newMapDlg.tileMap;
                saveObjects();
                loadMap();
                tileSetBox.Refresh();
            }
        }

        private void loadObjects()
        {
            objTree.Nodes.Clear();
            assetTree.Nodes[0].Nodes.Clear();
            assetTree.Nodes[1].Nodes.Clear();
            assetTree.Nodes[2].Nodes.Clear();
            assetTree.Nodes[3].Nodes.Clear();
            //XmlDocument objDoc = new XmlDocument();
            //objDoc.Load(folderPath + "/objects.xml");
            //tileMap.loadObjects(objDoc);
            foreach (KeyValuePair<String, ObjectTemplate> objTmp in tileMap.objTemplates)
            {
                objTree.Nodes.Add(objTmp.Key);
            }
            foreach (KeyValuePair<String, Asset> assetTmp in tileMap.assetDict)
            {
                if (assetTmp.Value.dataType == "Bitmap")
                    assetTree.Nodes[0].Nodes.Add(assetTmp.Key);
                if (assetTmp.Value.dataType == "Audio")
                    assetTree.Nodes[1].Nodes.Add(assetTmp.Key);
                if (assetTmp.Value.dataType == "String")
                    assetTree.Nodes[2].Nodes.Add(assetTmp.Key);
                if (assetTmp.Value.dataType == "Integer")
                    assetTree.Nodes[3].Nodes.Add(assetTmp.Key);
            }
        }

        private void saveObjects()
        {
        }

        private void updateList()
        {
            layerListBox.Items.Clear();
            for (int i = 0; i < tileMap.layerSet.Count; i++)
            {
                layerListBox.Items.Add("Layer " + (tileMap.layerSet.Count - i).ToString());
            }
        }

        private void tileSetBox_MouseMove(object sender, MouseEventArgs e)
        {
            tileSetBox.Refresh();
            mx = e.X;
            my = e.Y;
        }

        private void tileSetBox_Paint(object sender, PaintEventArgs e)
        {
            if (loaded)
            {
                int tw = tileMap.tileWidth;
                int th = tileMap.tileHeight;
                int trow = tileMap.tileSet.row;
                int tcol = tileMap.tileSet.col;
                int newX = (mx / tw) * tw;
                int newY = (my / th) * th;
                int locX = (Math.Abs(tileSetBox.Location.X) / tw);
                int locY = (Math.Abs(tileSetBox.Location.Y) / th);
                int sizeX = ((splitContainer1.Panel1.Width) / tw) + 1;
                int sizeY = ((splitContainer1.Panel1.Height) / th) + 1;
                Graphics g = e.Graphics;
                int cnt = 0;
                tileSetBox.Width = tcol * tw;
                tileSetBox.Height = trow * th;
                ImageAttributes imgAtt = new ImageAttributes();
                //imgAtt.SetColorKey(Color.FromArgb(255,38, 38, 38), Color.FromArgb(255,42, 42, 42));
                for(int i = locX; i < locX+sizeX+3; i++)
                    for (int j = locY; j < locY+sizeY+3; j++)
                    {
                        if (tileMap.tileIndexExists(new Point(i,j)))
                        {
                            Rectangle destRect = new Rectangle( i * tw, j * th, tw, th);
                            g.DrawImage(tileMap.getBitmapByIndex(new Point(i, j)),destRect,0,0,tw,th,GraphicsUnit.Pixel, imgAtt);
                            cnt = cnt + 1;
                        }
                    }
                g.DrawRectangle(System.Drawing.Pens.Red, newX, newY, tw, th);
                if(tileSelPnt != null)
                    g.DrawRectangle(System.Drawing.Pens.Blue, tileSelPnt.X * tw, tileSelPnt.Y * th, tw, th);
            }
        }


        private void loadButton_Click(object sender, EventArgs e)
        {
            if (openTileDlg.ShowDialog() == DialogResult.OK)
            {
                String openFname = openTileDlg.FileName;
                folderPath = openFname;
                //XmlDocument objDoc = new XmlDocument();
                //objDoc.Load(folderPath + "/objects.xml");
                tileMap = new TileMap(openFname);
                loadMap();
                loadObjects();
                //saveObjects();
            }
        }


        private void mapBox_Click(object sender, EventArgs e)
        {

        }

        private void highlightCells(Graphics g, int x, int y, int width, int height)
        {
            int tw = (int)(tileMap.tileWidth*scale);
            int th = (int)(tileMap.tileHeight*scale);
            selW = width+1;
            selH = height+1;
            initSelPnt = new Point(x, y);
            copyList = new List<List<TileValue>>();
            for (int i = x; i < x + width + 1; i++)
            {
                List<TileValue> tempList = new List<TileValue>();
                for (int j = y; j < y + height + 1; j++)
                {
                    tempList.Add(tileMap.layerSet[curLayer][new Point(i, j)]);
                    if ((tileMap.getValue(curLayer, new Point(i, j)).sel.X > -1 && selectionButton.Checked) || (fillButton.Checked))
                    {
                        selList.Add(new Point(i, j), new Point(i, j));
                    }
                }
                if (!copyButton.Checked)
                    copyList.Add(tempList);
            }
        }

        private void mapBox_Paint(object sender, PaintEventArgs e)
        {
            if (loaded)
            {
                scaleText.Text = (scale*100 ).ToString();
                colText.Text = tileMap.tileSet.col.ToString();
                //updateList();
                Graphics g = e.Graphics;
                int tw = tileMap.tileWidth;
                int th = tileMap.tileHeight;
                int stw = (int)Math.Sqrt((tileMap.tileWidth * tileMap.tileWidth)*scale);
                int sth = stw;
                //int stw = (int)(tileMap.tileWidth*scale);
                //int sth = (int)(tileMap.tileHeight * scale);
                //g.DrawImage((Image)canvasImg, new Point(0, 0));
                //int newX = (mx / tw) * tw;
                //int newY = (my / th) * th;
                int locX = (Math.Abs(mapBox.Location.X) / stw)-1;
                int locY = (Math.Abs(mapBox.Location.Y) / sth)-1;
                int sizeX = ((splitContainer1.Panel2.Width) / stw)+1;
                int sizeY = ((splitContainer1.Panel2.Height) / sth)+1;
                for (int k = tileMap.layerSet.Count-1; k >= curLayer; k--)
                    for (int i = locX; i < locX + sizeX + 2; i++)
                    {
                        if (i < 0)
                            continue;
                        for (int j = locY; j < locY + sizeY + 2; j++)
                        {
                            if (j < 0)
                                continue;
                            Point val = tileMap.getValue(k, new Point(i, j)).sel;
                            //Point val = new Point(0, 0);
                            if (val.X > -1)
                            {
                                int drawX = (int)(i * stw);
                                int drawY = (int)(j * sth);
                                g.DrawImage(tileMap.getBitmapByIndex(val), drawX, drawY, stw, sth);
                                if (collisionViewButton.Checked)
                                {
                                    Point pnt = new Point(i, j);
                                    int curColValue = tileMap.getValue(k, pnt).col;
                                    Byte[] colBytes = BitConverter.GetBytes(curColValue);
                                    Pen colPen = new Pen(Color.Orange, 3);
                                    if (colBytes[0] == 1)
                                    {
                                        g.DrawLine(colPen, (int)(pnt.X * stw), (int)(pnt.Y * sth), (int)((pnt.X * stw) + (stw)), (int)(pnt.Y * sth));
                                    }
                                    if (colBytes[1] == 1)
                                    {
                                        g.DrawLine(colPen, (pnt.X * stw) + (stw), pnt.Y * sth, (pnt.X * stw) + (stw), (pnt.Y * sth) + (sth));
                                    }
                                    if (colBytes[2] == 1)
                                    {
                                        g.DrawLine(colPen, pnt.X * stw, (pnt.Y * sth) + (sth), (pnt.X * stw) + (stw), (pnt.Y * sth) + (sth));
                                    }
                                    if (colBytes[3] == 1)
                                    {
                                        g.DrawLine(colPen, pnt.X * stw, pnt.Y * sth, pnt.X * stw, (pnt.Y * sth) + (sth));
                                    }
                                }

                            }
                        }
                    }
                if (objectViewButton.Checked)
                {
                    foreach (ObjectInstance curInstance in tileMap.objInstList)
                    {
                        Bitmap defaultImg = tileMap.objTemplates[curInstance.objName].imgStates.getImg("default");
                        double norm = Math.Sqrt(defaultImg.Width*defaultImg.Height);
                        double normWidth = defaultImg.Width/norm;
                        double normHeight = defaultImg.Height / norm;
                        double newNorm = Math.Sqrt(defaultImg.Width * defaultImg.Height * scale);
                        double normC = Math.Sqrt(curInstance.pnt.X * curInstance.pnt.Y);
                        double normX = curInstance.pnt.X / normC;
                        double normY = curInstance.pnt.Y / normC;
                        double newNormC = Math.Sqrt(curInstance.pnt.X * curInstance.pnt.Y * scale);
                        g.DrawImage(defaultImg, (int)(normX*newNormC), (int)(normY*newNormC), (int)(normWidth*newNorm), (int)(normHeight*newNorm));
                    }
                }
                int newX = (int)(((mx) / stw) * stw);
                int newY = (int)(((my) / sth) * sth);
                g.DrawRectangle(System.Drawing.Pens.Red, (int)(newX), (int)(newY), stw, sth);
                foreach (Point pnt in selList.Values.ToList())
                {
                    int lineX = (int)(pnt.X * stw * scale );
                    int lineY = (int)( pnt.Y * sth * scale );
                    g.DrawRectangle(System.Drawing.Pens.Red,lineX,lineY,stw,sth);
                }
                if (clicked)
                {
                    if ( (selectionButton.Checked || fillButton.Checked) )
                    {
                        clearSelList();
                        int w = Math.Abs(mx - dragX1);
                        int h = Math.Abs(my - dragY1);
                        if (mx - dragX1 > 0 && my - dragY1 > 0)
                        {
                            g.DrawRectangle(System.Drawing.Pens.Blue, dragX1, dragY1, w, h);
                            highlightCells(g, dragX1 / stw , dragY1 / sth , w / stw , h / sth );
                        }
                        if (mx - dragX1 < 0 && my - dragY1 < 0)
                        {
                            g.DrawRectangle(System.Drawing.Pens.Blue, mx, my, Math.Abs(mx - dragX1), Math.Abs(my - dragY1));
                            highlightCells(g, mx / stw , my / sth , w / stw , h / sth );
                        }
                        if (mx - dragX1 < 0 && my - dragY1 > 0)
                        {
                            g.DrawRectangle(System.Drawing.Pens.Blue, mx, dragY1, Math.Abs(mx - dragX1), Math.Abs(my - dragY1));
                            highlightCells(g, mx / stw , dragY1 / sth , w / stw , h / sth );
                        }
                        if (mx - dragX1 > 0 && my - dragY1 < 0)
                        {
                            g.DrawRectangle(System.Drawing.Pens.Blue, dragX1, my, Math.Abs(mx - dragX1), Math.Abs(my - dragY1));
                            highlightCells(g, dragX1 / stw , my / sth , w / stw , h / sth );
                        }
                    }
                    if (copyButton.Checked)
                    {
                        int aMx = (mx / tw);
                        int aMy = (my / th);
                        for (int i = aMx; i < aMx + copyList.Count; i++)
                            for (int j = aMy; j < aMy + copyList[i-aMx].Count; j++)
                            {
                                tileMap.setTile(curLayer, new Point(i, j), copyList[i-aMx][j-aMy]);
                            }
                    }
                }
                if (placeObjectButton.Checked)
                {
                    int objWidth = selObjTemplate.imgStates.getImg("default").Width;
                    int objHeight = selObjTemplate.imgStates.getImg("default").Height;
                    g.DrawRectangle(System.Drawing.Pens.Red, (int)(mx * scale), (int)(my * scale), (int)(objWidth * scale), (int)(objHeight * scale));
                }
                if (moveButton.Checked)
                {
                    int aMx = (mx / tw);
                    int aMy = (my / th);
                    for(int i = aMx; i < aMx + selW; i++)
                        for (int j = aMy; j < aMy + selH; j++)
                        {
                            g.DrawRectangle(System.Drawing.Pens.Red, (int)(i * stw * scale), (int)(j * sth * scale), stw, sth);
                        }
                }
                //reloadImages = false;
                //miniMapBox.Refresh();
            } 
        }

        private void mapBox_MouseUp(object sender, MouseEventArgs e)
        {
            clicked = false;
        }

        private void mapBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (loaded)
            {
                mx = e.X;
                my = e.Y;
                int stw = (int)Math.Sqrt((tileMap.tileWidth * tileMap.tileWidth) * scale);
                int sth = stw;
                if (clicked)
                {
                    //int markX = (int)Math.Ceiling((double)mx / stw);
                    //int markY = (int)Math.Ceiling((double)my / sth);
                    int markX = (int)((mx) / stw);
                    int markY = (int)((my) / sth)-1;
                    if (placementButton.Checked)
                        tileMap.setTile(curLayer,new Point(markX, markY), tileSetSel);
                    mapBox.Refresh();
                    mapBox.AutoScrollOffset = new Point(5, 5);
                }
                int newX = (mx / stw) * stw;
                int newY = (my / sth) * sth;
                //mapBox.Invalidate(new Rectangle(newX,newY,tw,th));
                mapBox.Refresh();
            }
        }


        private void clearSelList()
        {
            initSelPnt = new Point(0,0);
            selList.Clear();
            //copyList.Clear();
            selW = 0;
            selH = 0;
        }

        private Dictionary<String, Property> sanitizePropDict(Dictionary<String, Property> objectPropDict)
        {
            foreach (Property prop in objectPropDict.Values.ToList())
            {
                prop.defaultValue = null;
            }
            return objectPropDict;
        }

        private void mapBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (loaded)
            {
                mx = e.X;
                my = e.Y;
                clicked = true;
                int tw = tileMap.tileWidth;
                int th = tileMap.tileHeight;
                int markX = (mx / tw);
                int markY = (my / th);
                if (placementButton.Checked )
                    tileMap.setTile(curLayer,new Point(markX, markY), tileSetSel);
                if (selectionButton.Checked || fillButton.Checked)
                {
                    if (clicked)
                    {
                        dragX1 = mx;
                        dragY1 = my;
                    }
                }
                if (placeObjectButton.Checked)
                {
                    String objName = selObjTemplate.name;
                    Dictionary<String, Property> objPropDict = selObjTemplate.propertyDict;
                    objPropDict = sanitizePropDict(objPropDict);
                    tileMap.objInstList.Add(new ObjectInstance(objName, objPropDict, new Point(mx, my),1));
                }
                if (selectionButton.Checked && objectViewButton.Checked)
                {
                    foreach( ObjectInstance objInst in tileMap.objInstList )
                    {
                        Bitmap defImg = tileMap.objTemplates[objInst.objName].imgStates.getImg("default");
                        int instW = defImg.Width;
                        int instH = defImg.Height;
                        int instX = objInst.pnt.X;
                        int instY = objInst.pnt.Y;
                        if( mx >= instX && mx <= instX +instW )
                            if (my >= instY && my <= instY + instH)
                            {
                                ObjectInstanceForm objInstForm = new ObjectInstanceForm(objInst.objName,objInst.propDict,tileMap.assetDict,objInst.visible);
                                if (objInstForm.ShowDialog() == DialogResult.OK)
                                {
                                    clicked = false;
                                    objInst.propDict = objInstForm.propDict;
                                    objInst.visible = objInstForm.visible;
                                    mapBox.Refresh();
                                    break;
                                }
                            }
                    }
                }
                if (deletionButton.Checked)
                {
                    if (objectViewButton.Checked)
                    {
                        foreach (ObjectInstance objInst in tileMap.objInstList)
                        {
                            Bitmap defImg = tileMap.objTemplates[objInst.objName].imgStates.getImg("default");
                            int instW = defImg.Width;
                            int instH = defImg.Height;
                            int instX = objInst.pnt.X;
                            int instY = objInst.pnt.Y;
                            if (mx >= instX && mx <= instX + instW)
                                if (my >= instY && my <= instY + instH)
                                {
                                    tileMap.objInstList.Remove(objInst);
                                    break;
                                }
                        }
                    }
                    if (selList.Count > 0)
                    {
                        foreach (Point pnt in selList.Values.ToList())
                        {
                            tileMap.setTile(curLayer,pnt, new TileValue(new Point(-1, -1), 0));
                            clearSelList();
                        }
                    }
                    else
                    {
                        tileMap.setTile(curLayer,new Point(markX, markY), new TileValue(new Point(-1, -1), 0));
                    }
                }
                if (moveButton.Checked)
                {
                    TileMap tmpTileMap = new TileMap(tileMap);
                    int aMx = mx / tw;
                    int aMy = my / th;
                    Dictionary<Point, Point> tmpSelList = new Dictionary<Point, Point>();
                    Point tmpInitSelPnt = new Point(aMx, aMy);
                    int tmpSelW = selW;
                    int tmpSelH = selH;
                    for (int i = aMx; i < aMx + selW; i++)
                        for (int j = aMy; j < aMy + selH; j++)
                        {
                            int normX = i - aMx + initSelPnt.X;
                            int normY = j - aMy + initSelPnt.Y;
                            if (selList.ContainsKey(new Point(normX, normY)))
                            {
                                TileValue tValue = tmpTileMap.getValue(curLayer, selList[new Point(normX, normY)]);
                                tileMap.setTile(curLayer,new Point(i, j), tValue );
                                tmpSelList.Add(new Point(i, j), new Point(i, j));
                            }
                            //g.DrawRectangle(System.Drawing.Pens.Red, i * tw, j * th, tw, th);
                        }
                    foreach (Point pnt in selList.Values.ToList())
                    {
                        tileMap.setTile(curLayer, pnt, new TileValue(new Point(-1, -1), 0));
                    }
                    clearSelList();
                    selW = tmpSelW;
                    selH = tmpSelH;
                    initSelPnt = tmpInitSelPnt;
                    selList = tmpSelList;
                }
                mapBox.Refresh();
            }
        }

        private void tileSetBox_Click(object sender, EventArgs e)
        {
            if (loaded)
            {
                int tw = tileMap.tileWidth;
                int th = tileMap.tileHeight;
                int markX = (mx / tw);
                int markY = (my / th);
                if(tileMap.tileSet.tileImgs.ContainsKey(new Point(markX,markY)))
                {
                    tileSetSel = new TileValue(new Point(markX, markY), 0);
                    tileSelPnt = new Point(markX, markY);
                }
                if (fillButton.Checked)
                {
                    foreach (Point pnt in selList.Values.ToList())
                    {
                        tileMap.setTile(curLayer,pnt, tileSetSel);
                    }
                    selList.Clear();
                    mapBox.Refresh();
                }
            }
        }

        private int getCollisionInt()
        {
            Byte[] colBytes = { 0,0,0,0 };
            if (northButton.Checked)
                colBytes[0] = 1;
            if (eastButton.Checked)
                colBytes[1] = 1;
            if (southButton.Checked)
                colBytes[2] = 1;
            if (westButton.Checked)
                colBytes[3] = 1;
            return BitConverter.ToInt32(colBytes, 0);
        }

        private void deletionButton_Click(object sender, EventArgs e)
        {
            selectionButton.Checked = false;
            placementButton.Checked = false;
            fillButton.Checked = false;
            moveButton.Checked = false;
            placeObjectButton.Checked = false;
        }

        private void placementButton_Click(object sender, EventArgs e)
        {
            selList.Clear();
            deletionButton.Checked = false;
            selectionButton.Checked = false;
            fillButton.Checked = false;
            moveButton.Checked = false;
            placeObjectButton.Checked = false;
        }

        private void selectionButton_Click(object sender, EventArgs e)
        {
            placementButton.Checked = false;
            deletionButton.Checked = false;
            fillButton.Checked = false;
            moveButton.Checked = false;
            placeObjectButton.Checked = false;
        }

        private void fillButton_Click(object sender, EventArgs e)
        {
            selectionButton.Checked = false;
            placementButton.Checked = false;
            deletionButton.Checked = false;
            moveButton.Checked = false;
            placeObjectButton.Checked = false;
        }

        private void mapBox_Move(object sender, EventArgs e)
        {
            mapBox.Refresh();
        }

        private void splitContainer1_Panel2_Resize(object sender, EventArgs e)
        {
            mapBox.Refresh();
        }

        private void moveButton_Click(object sender, EventArgs e)
        {
            selectionButton.Checked = false;
            placementButton.Checked = false;
            deletionButton.Checked = false;
            fillButton.Checked = false;
            placeObjectButton.Checked = false;
        }

        private void collisionButton_Click(object sender, EventArgs e)
        {
            if (fillButton.Checked)
            {
                foreach (Point pnt in selList.Values.ToList())
                {
                    if(tileMap.keyExists(curLayer,pnt))
                        tileMap.setTile(curLayer,pnt, new TileValue(tileMap.getValue(curLayer,pnt).sel, getCollisionInt()));
                }
                mapBox.Refresh();
            }
        }

        private void saveFolder_Click(object sender, EventArgs e)
        {
            if (tileMap != null)
            {
                tileMap.packData();
                MessageBox.Show("Map saved!"); 
            }
        }

        private void addTileButton_Click(object sender, EventArgs e)
        {
            if (loadTileDlg.ShowDialog() == DialogResult.OK)
            {

                Bitmap tmpBitmap = new Bitmap(loadTileDlg.FileName);
                tmpBitmap.MakeTransparent(Color.FromArgb(255, 0, 255));
                    if (tmpBitmap.Width < tileMap.tileWidth || tmpBitmap.Height < tileMap.tileHeight)
                    {
                        MessageBox.Show("Size must be more than: " + tileMap.tileWidth.ToString() + " x " + tileMap.tileHeight.ToString());
                        return;
                    }
                    int numCols = tmpBitmap.Width / tileMap.tileWidth;
                    int numRows = tmpBitmap.Height / tileMap.tileHeight;
                    for(int i=0; i < numCols; i++)
                        for (int j = 0; j < numRows; j++)
                        {
                            Bitmap bitmapPart = tmpBitmap.Clone(new Rectangle(i * tileMap.tileWidth, j * tileMap.tileHeight, tileMap.tileWidth, tileMap.tileHeight), PixelFormat.Format32bppArgb);
                            tileMap.addTile(bitmapPart);
                        }
                    tileSetBox.Refresh();

            }
        }

        private bool isValidSelection()
        {
            if (tileSetSel.sel == null || !tileMap.tileIndexExists(tileSetSel.sel))
                return false;
            else
                return true;
        }

        private void delTileButton_Click(object sender, EventArgs e)
        {
            if (isValidSelection() == false)
            {
                MessageBox.Show("Selection does not exists of is invalid");
            }
            else
            {
                Dictionary<Point, TileValue> mapCopy = new Dictionary<Point, TileValue>(tileMap.layerSet[curLayer]);
                tileMap.removeTile(tileSetSel.sel);
                foreach (KeyValuePair<Point, TileValue> mapPoint in tileMap.layerSet[curLayer])
                {
                    int tileX = tileSetSel.sel.X;
                    int tileY = tileSetSel.sel.Y;
                    int mapX = mapPoint.Value.sel.X;
                    int mapY = mapPoint.Value.sel.Y;
                    int latX = 0;
                    int latY = 0;
                    if (mapX == -1 || mapY == -1)
                        continue;
                    if (mapX == tileX)
                        latX = 0;
                    if (mapY == tileY)
                        latY = 0;
                    if (mapX > tileX)
                        latX = 1;
                    if (mapY > tileY)
                        latY = 1;
                    if (mapX < tileX)
                        latX = -1;
                    if (mapY < tileY)
                        latY = -1;
                    if (latX == 0 && latY == 0)
                        mapCopy[new Point(mapPoint.Key.X, mapPoint.Key.Y)] = new TileValue(new Point(-1, -1), 0);
                    if ((latX == 1 && latY == 0) || (latX == -1 && latY == 1)
                        || (latX == 0 && latY == 1) || (latX == 1 && latY == 1))
                    {
                        int newX = mapX;
                        int newY = mapY;
                        if (mapX - 1 < 0)
                        {
                            newY -= 1;
                            newX = tileMap.tileSet.col - 1;
                        }
                        else
                        {
                            newX -= 1;
                        }
                        mapCopy[new Point(mapPoint.Key.X, mapPoint.Key.Y)].sel.X = newX;
                        mapCopy[new Point(mapPoint.Key.X, mapPoint.Key.Y)].sel.Y = newY;
                    }
                }
                tileMap.layerSet[curLayer] = mapCopy;
                tileSetBox.Refresh();
                mapBox.Refresh();
            }
        }

        private void replaceTileButton_Click(object sender, EventArgs e)
        {
            if (isValidSelection() == false)
            {
                MessageBox.Show("Selection does not exists of is invalid");
                return;
            }
            if (openReplaceTileDlg.ShowDialog() == DialogResult.OK)
            {
                Bitmap newImg = new Bitmap(openReplaceTileDlg.FileName);
                newImg.MakeTransparent(Color.FromArgb(255, 0, 255));
                if (newImg.Width != tileMap.tileWidth || newImg.Height != tileMap.tileHeight)
                {
                    MessageBox.Show("Size must be: " + tileMap.tileWidth.ToString() + " x " + tileMap.tileHeight.ToString());
                }
                else
                {
                    tileMap.replaceTile(tileSetSel.sel, newImg);
                }
            }
        }

        private void addObjectButton_Click(object sender, EventArgs e)
        {
            ObjectForm objForm = new ObjectForm();
            if (objForm.ShowDialog() == DialogResult.OK)
            {
                tileMap.addObject(objForm.objName, objForm.propDict, objForm.imgStates);
                loadObjects();
            }
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (objTree.SelectedNode != null)
            {
                String selName = objTree.SelectedNode.Text;
                if (tileMap.containsObject(selName))
                {
                    ObjectTemplate selTemplate = tileMap.getObjectTemplate(selName);
                    ImageStates imgState = selTemplate.imgStates;
                    ObjectForm objForm = new ObjectForm(selName, selTemplate.propertyDict, imgState, tileMap.objInstList);
                    List<ObjectInstance> selObjInstList = new List<ObjectInstance>();
                    foreach (ObjectInstance curInst in tileMap.objInstList)
                        if (curInst.objName == selName)
                            selObjInstList.Add(curInst);
                    if (objForm.ShowDialog() == DialogResult.OK)
                    {
                        tileMap.editObject(selName, objForm.objName, objForm.propDict, objForm.imgStates, objForm.objInstList);
                        saveObjects();
                        loadObjects();
                        mapBox.Refresh();
                    }
                }
            }
        }

        private void deleteObjectButton_Click(object sender, EventArgs e)
        {
            if (objTree.SelectedNode != null)
            {
                String selName = objTree.SelectedNode.Text;
                if (tileMap.containsObject(selName))
                {
                    tileMap.deleteObject(selName);
                    saveObjects();
                    loadObjects();
                    mapBox.Refresh();
                }
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void placeObjectButton_Click(object sender, EventArgs e)
        {
            selectionButton.Checked = false;
            placementButton.Checked = false;
            fillButton.Checked = false;
            moveButton.Checked = false;
            deletionButton.Checked = false;
        }

        private void objTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            selObjTemplate = tileMap.objTemplates[objTree.SelectedNode.Text];
        }

        private void objectViewButton_Click(object sender, EventArgs e)
        {
            mapBox.Refresh();
        }

        private void addAssetButton_Click(object sender, EventArgs e)
        {
            AssetDialog assetDlg = new AssetDialog(tileMap.assetDict, tileMap.objInstList);
            if (assetDlg.ShowDialog() == DialogResult.OK)
            {
                tileMap.assetDict = assetDlg.assetDict;
                tileMap.objInstList = assetDlg.objInstList;
                loadObjects();
            }
        }

        private void editAssetButton_Click(object sender, EventArgs e)
        {
            if (assetTree.SelectedNode.Parent != null)
            {
                String selectedText = assetTree.SelectedNode.Text;
                if (tileMap.assetDict.ContainsKey(selectedText))
                {
                    AssetDialog assetDialog = new AssetDialog(selectedText, tileMap.assetDict, tileMap.objInstList);
                    if (assetDialog.ShowDialog() == DialogResult.OK)
                    {
                        tileMap.assetDict = assetDialog.assetDict;
                        tileMap.objInstList = assetDialog.objInstList;
                        loadObjects();
                    }
                }
            }
        }

        private void panel1_Scroll(object sender, ScrollEventArgs e)
        {
            tileSetBox.Refresh();
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            
        }

        private void miniMapBox_Paint(object sender, PaintEventArgs e)
        {
            if (loaded)
            {
                Graphics g = e.Graphics;
                //drawminiMap();
                int posX = mapBox.Location.X / tileMap.tileWidth;
                int posY = mapBox.Location.Y / tileMap.tileHeight;
                int boxWidth = splitContainer2.Panel2.Width / tileMap.tileWidth;
                int boxHeight = splitContainer2.Panel2.Height/ tileMap.tileHeight;
                g.DrawRectangle(System.Drawing.Pens.Red, new Rectangle(posX*-1, posY*-1, boxWidth, boxHeight));
            }
        }

        private void layerListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //curLayer = (tileMap.layerSet.Count - layerListBox.SelectedIndex - 1);
            curLayer = layerListBox.SelectedIndex;
            mapBox.Refresh();
        }

        private void addLayerButton_Click(object sender, EventArgs e)
        {
            tileMap.addNewLayer();
            updateList();

        }

        private void resizeButton_Click(object sender, EventArgs e)
        {
            ResizeForm resizeDlg = new ResizeForm(tileMap.colNum, tileMap.rowNum);
            if (resizeDlg.ShowDialog() == DialogResult.OK)
            {
                int newWidth = resizeDlg.width;
                int newHeight = resizeDlg.height;
                int oldWidth = tileMap.colNum;
                int oldHeight = tileMap.rowNum;
                if (newWidth < oldWidth)
                {
                    for (int i = newWidth; i < tileMap.colNum; i++)
                        for (int j = 0; j < tileMap.rowNum; j++)
                            tileMap.removeTileVals(new Point(i, j));
                    tileMap.colNum = newWidth;

                }
                if (newHeight < oldHeight)
                {
                    for (int i = 0; i < tileMap.colNum; i++)
                        for (int j = newHeight; j < tileMap.colNum; j++)
                            tileMap.removeTileVals(new Point(i, j));
                    tileMap.rowNum = newHeight;
                }
                if (newWidth > oldWidth)
                {
                    for (int i = oldWidth; i < newWidth; i++)
                        for (int j = 0; j < tileMap.rowNum; j++)
                            tileMap.expandTileVals(new Point(i, j));
                    tileMap.colNum = newWidth;
                }
                if (newHeight > oldHeight)
                {
                    for (int i = 0; i < tileMap.colNum; i++)
                        for (int j = oldHeight; j < newHeight; j++)
                            tileMap.expandTileVals(new Point(i, j));
                    tileMap.rowNum = newHeight;
                }
                mapBox.Width = tileMap.colNum * tileMap.tileWidth;
                mapBox.Height = tileMap.rowNum * tileMap.tileHeight;
                mapBox.Refresh();
            }
        }

        private void miniMapBox_Click(object sender, EventArgs e)
        {
            splitContainer2.Panel2.AutoScrollPosition = new Point(mmx*32, mmy*32);
            //mapBox.Location = new Point(-200, -200);
            mapBox.Refresh();
        }

        private void miniMapBox_MouseMove(object sender, MouseEventArgs e)
        {
            mmx = e.X;
            mmy = e.Y;
        }

        private void scaleText_Click(object sender, EventArgs e)
        {

        }

        private void zoomButton_Click(object sender, EventArgs e)
        {
            try
            {
                scale = double.Parse(scaleText.Text) / 100;
            }
            catch (Exception gExp)
            {
                MessageBox.Show("Invalid value");
            }
            mapBox.Refresh();
        }

        private void colButton_Click(object sender, EventArgs e)
        {
            Dictionary<Point, Bitmap> newTileImgs = new Dictionary<Point, Bitmap>() ;
            Dictionary<Point, Point> pointMapping = new Dictionary<Point, Point>();
            int tsw = tileMap.tileSet.col;
            int tsh = tileMap.tileSet.row;
            int newColNum = Int32.Parse(colText.Text);
            int newRowNum = (tsw*tsh)/newColNum;
            int pos = 0;
            for(int i = 0; i < tileMap.tileSet.row; i++)
                for (int j = 0; j < tileMap.tileSet.col; j++)
                {
                    // y*w-(h-x)
                    int newY = pos / newColNum;
                    int newX = newColNum - ((newY * newColNum) - pos) - newColNum;
                    newTileImgs.Add(new Point(newX , newY), tileMap.tileSet.tileImgs[new Point(j, i)]);
                    pointMapping.Add(new Point(j, i), new Point(newX, newY));
                    pos++;
                }
            for (int k = tileMap.layerSet.Count - 1; k >= 0; k--)
                for (int i = 0; i < tileMap.colNum; i++)
                    for (int j = 0; j < tileMap.rowNum; j++)
                    {
                        Point selPoint = tileMap.layerSet[k][new Point(i, j)].sel;
                        if(selPoint.X != -1 && selPoint.Y != -1)
                            tileMap.layerSet[k][new Point(i, j)].sel = pointMapping[selPoint];
                    }
            tileMap.tileSet.tileImgs = newTileImgs;
            tileMap.tileSet.row = newRowNum;
            tileMap.tileSet.col = newColNum;
            tileSetBox.Refresh();
            mapBox.Refresh();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            ColorCodeDialog colorCodeDlg = new ColorCodeDialog();
            if (colorCodeDlg.ShowDialog() == DialogResult.OK)
            {
                Color from = colorCodeDlg.fromColor;
                Color to = colorCodeDlg.toColor;
                tileMap.colorSwap(colorCodeDlg.isAlpha,from, to);
            }
            tileSetBox.Refresh();
            mapBox.Refresh();
        }

        private void tileTemplateButton_Click(object sender, EventArgs e)
        {
            TileTemplateDialog tileTemplateDialog = new TileTemplateDialog("Add", selList,tileMap.tileSet);
            tileTemplateDialog.ShowDialog();
        }

        private void loadTileDlg_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void loadMapDlg_HelpRequest(object sender, EventArgs e)
        {

        }

        private void runButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openExe = new OpenFileDialog();
            if( openExe.ShowDialog() == DialogResult.OK ) 
            {
                String exeName = openExe.FileName;
                String exeDir = Path.GetDirectoryName(exeName);
                System.Diagnostics.Process demoProcess = new System.Diagnostics.Process();
                try
                {
                    //demoProcess.StartInfo.UseShellExecute = false;
                    demoProcess.StartInfo.WorkingDirectory = exeDir;
                    demoProcess.StartInfo.FileName = exeName;
                    demoProcess.StartInfo.Arguments = "demo";
                    demoProcess.Start();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Cannot run");
                }
            }
        }

    }
}
