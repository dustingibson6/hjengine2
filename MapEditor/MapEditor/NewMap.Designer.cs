﻿namespace MapEditor
{
    partial class NewMap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rowNum = new System.Windows.Forms.NumericUpDown();
            this.colNum = new System.Windows.Forms.NumericUpDown();
            this.saveLocationText = new System.Windows.Forms.TextBox();
            this.rowLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.saveBrowseButton = new System.Windows.Forms.Button();
            this.createButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.browseTileSetButton = new System.Windows.Forms.Button();
            this.tilesetLocationText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.widthNum = new System.Windows.Forms.NumericUpDown();
            this.heightNum = new System.Windows.Forms.NumericUpDown();
            this.labelABC = new System.Windows.Forms.Label();
            this.folderSaveDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.openTileSet = new System.Windows.Forms.OpenFileDialog();
            this.label3 = new System.Windows.Forms.Label();
            this.nameText = new System.Windows.Forms.TextBox();
            this.exportCheck = new System.Windows.Forms.CheckBox();
            this.exportBrowseButton = new System.Windows.Forms.Button();
            this.exportText = new System.Windows.Forms.TextBox();
            this.exportFileBrowse = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.rowNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.widthNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.heightNum)).BeginInit();
            this.SuspendLayout();
            // 
            // rowNum
            // 
            this.rowNum.Location = new System.Drawing.Point(76, 72);
            this.rowNum.Maximum = new decimal(new int[] {
            1215752191,
            23,
            0,
            0});
            this.rowNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.rowNum.Name = "rowNum";
            this.rowNum.Size = new System.Drawing.Size(120, 20);
            this.rowNum.TabIndex = 2;
            this.rowNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // colNum
            // 
            this.colNum.Location = new System.Drawing.Point(76, 113);
            this.colNum.Maximum = new decimal(new int[] {
            1410065407,
            2,
            0,
            0});
            this.colNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.colNum.Name = "colNum";
            this.colNum.Size = new System.Drawing.Size(120, 20);
            this.colNum.TabIndex = 3;
            this.colNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // saveLocationText
            // 
            this.saveLocationText.Location = new System.Drawing.Point(92, 308);
            this.saveLocationText.Name = "saveLocationText";
            this.saveLocationText.Size = new System.Drawing.Size(236, 20);
            this.saveLocationText.TabIndex = 10;
            // 
            // rowLabel
            // 
            this.rowLabel.AutoSize = true;
            this.rowLabel.Location = new System.Drawing.Point(12, 72);
            this.rowLabel.Name = "rowLabel";
            this.rowLabel.Size = new System.Drawing.Size(34, 13);
            this.rowLabel.TabIndex = 3;
            this.rowLabel.Text = "Rows";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Columns";
            // 
            // saveBrowseButton
            // 
            this.saveBrowseButton.Location = new System.Drawing.Point(11, 305);
            this.saveBrowseButton.Name = "saveBrowseButton";
            this.saveBrowseButton.Size = new System.Drawing.Size(75, 23);
            this.saveBrowseButton.TabIndex = 7;
            this.saveBrowseButton.Text = "Browse";
            this.saveBrowseButton.UseVisualStyleBackColor = true;
            this.saveBrowseButton.Click += new System.EventHandler(this.saveBrowseButton_Click);
            // 
            // createButton
            // 
            this.createButton.Location = new System.Drawing.Point(27, 429);
            this.createButton.Name = "createButton";
            this.createButton.Size = new System.Drawing.Size(75, 23);
            this.createButton.TabIndex = 8;
            this.createButton.Text = "Create";
            this.createButton.UseVisualStyleBackColor = true;
            this.createButton.Click += new System.EventHandler(this.createButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(199, 429);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 9;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // browseTileSetButton
            // 
            this.browseTileSetButton.Location = new System.Drawing.Point(11, 249);
            this.browseTileSetButton.Name = "browseTileSetButton";
            this.browseTileSetButton.Size = new System.Drawing.Size(75, 23);
            this.browseTileSetButton.TabIndex = 6;
            this.browseTileSetButton.Text = "Browse";
            this.browseTileSetButton.UseVisualStyleBackColor = true;
            this.browseTileSetButton.Click += new System.EventHandler(this.browseTileSetButton_Click);
            // 
            // tilesetLocationText
            // 
            this.tilesetLocationText.Location = new System.Drawing.Point(92, 252);
            this.tilesetLocationText.Name = "tilesetLocationText";
            this.tilesetLocationText.Size = new System.Drawing.Size(236, 20);
            this.tilesetLocationText.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 155);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Tile Width";
            // 
            // widthNum
            // 
            this.widthNum.Location = new System.Drawing.Point(76, 155);
            this.widthNum.Maximum = new decimal(new int[] {
            1215752191,
            23,
            0,
            0});
            this.widthNum.Name = "widthNum";
            this.widthNum.Size = new System.Drawing.Size(120, 20);
            this.widthNum.TabIndex = 4;
            this.widthNum.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
            // 
            // heightNum
            // 
            this.heightNum.Location = new System.Drawing.Point(73, 197);
            this.heightNum.Maximum = new decimal(new int[] {
            1215752191,
            23,
            0,
            0});
            this.heightNum.Name = "heightNum";
            this.heightNum.Size = new System.Drawing.Size(123, 20);
            this.heightNum.TabIndex = 5;
            this.heightNum.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
            // 
            // labelABC
            // 
            this.labelABC.AutoSize = true;
            this.labelABC.Location = new System.Drawing.Point(8, 197);
            this.labelABC.Name = "labelABC";
            this.labelABC.Size = new System.Drawing.Size(58, 13);
            this.labelABC.TabIndex = 12;
            this.labelABC.Text = "Tile Height";
            // 
            // openTileSet
            // 
            this.openTileSet.FileName = "openFileDialog1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Name";
            // 
            // nameText
            // 
            this.nameText.Location = new System.Drawing.Point(76, 30);
            this.nameText.MaxLength = 20;
            this.nameText.Name = "nameText";
            this.nameText.Size = new System.Drawing.Size(238, 20);
            this.nameText.TabIndex = 1;
            // 
            // exportCheck
            // 
            this.exportCheck.AutoSize = true;
            this.exportCheck.Location = new System.Drawing.Point(16, 345);
            this.exportCheck.Name = "exportCheck";
            this.exportCheck.Size = new System.Drawing.Size(56, 17);
            this.exportCheck.TabIndex = 16;
            this.exportCheck.Text = "Export";
            this.exportCheck.UseVisualStyleBackColor = true;
            // 
            // exportBrowseButton
            // 
            this.exportBrowseButton.Location = new System.Drawing.Point(11, 378);
            this.exportBrowseButton.Name = "exportBrowseButton";
            this.exportBrowseButton.Size = new System.Drawing.Size(75, 23);
            this.exportBrowseButton.TabIndex = 17;
            this.exportBrowseButton.Text = "Browse";
            this.exportBrowseButton.UseVisualStyleBackColor = true;
            this.exportBrowseButton.Click += new System.EventHandler(this.exportBrowseButton_Click);
            // 
            // exportText
            // 
            this.exportText.Location = new System.Drawing.Point(92, 378);
            this.exportText.Name = "exportText";
            this.exportText.Size = new System.Drawing.Size(222, 20);
            this.exportText.TabIndex = 18;
            // 
            // exportFileBrowse
            // 
            this.exportFileBrowse.FileName = "openFileDialog1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 230);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Tile Set";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 289);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Save";
            // 
            // NewMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(367, 464);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.exportText);
            this.Controls.Add(this.exportBrowseButton);
            this.Controls.Add(this.exportCheck);
            this.Controls.Add(this.nameText);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.heightNum);
            this.Controls.Add(this.labelABC);
            this.Controls.Add(this.widthNum);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.browseTileSetButton);
            this.Controls.Add(this.tilesetLocationText);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.createButton);
            this.Controls.Add(this.saveBrowseButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rowLabel);
            this.Controls.Add(this.saveLocationText);
            this.Controls.Add(this.colNum);
            this.Controls.Add(this.rowNum);
            this.Name = "NewMap";
            this.Text = "New Map";
            this.Load += new System.EventHandler(this.NewMap_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.NewMap_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.rowNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.widthNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.heightNum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown rowNum;
        private System.Windows.Forms.NumericUpDown colNum;
        private System.Windows.Forms.TextBox saveLocationText;
        private System.Windows.Forms.Label rowLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button saveBrowseButton;
        private System.Windows.Forms.Button createButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button browseTileSetButton;
        private System.Windows.Forms.TextBox tilesetLocationText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown widthNum;
        private System.Windows.Forms.NumericUpDown heightNum;
        private System.Windows.Forms.Label labelABC;
        private System.Windows.Forms.FolderBrowserDialog folderSaveDialog;
        private System.Windows.Forms.OpenFileDialog openTileSet;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox nameText;
        private System.Windows.Forms.CheckBox exportCheck;
        private System.Windows.Forms.Button exportBrowseButton;
        private System.Windows.Forms.TextBox exportText;
        private System.Windows.Forms.OpenFileDialog exportFileBrowse;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}