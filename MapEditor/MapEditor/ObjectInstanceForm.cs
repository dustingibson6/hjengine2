﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Media;

namespace MapEditor
{
    public partial class ObjectInstanceForm : Form
    {
        public String objName;
        public Dictionary<String, Property> propDict;
        public Dictionary<String, Asset> assetDict;
        public Byte[] dataBytes;
        public String selPropText;
        public int useAsset;
        public int listPos;
        public int visible;

        public ObjectInstanceForm(String objName, Dictionary<String,Property> propDict, Dictionary<String,Asset> assetDict, int visible)
        {
            this.objName = objName;
            this.propDict = propDict;
            this.assetDict = assetDict;
            this.visible = visible;
            selPropText = "";
            listPos = 0;
            InitializeComponent();
        }

        public Byte[] stringToBuffer(String bufString, int size)
        {
            Byte[] strBuffer = new Byte[size];
            strBuffer = System.Text.Encoding.ASCII.GetBytes(bufString.PadRight(size, '\0'));
            return strBuffer;
        }

        public String bufferToString(Byte[] byteBuffer)
        {
            if (byteBuffer == null || byteBuffer.Length <= 0)
                return null;
            String resultingString;
            resultingString = System.Text.Encoding.Default.GetString(byteBuffer);
            String newResultingString = "";
            for (int i = 0; i < resultingString.Length; i++)
            {
                if (resultingString[i] == '\0')
                    break;
                else
                    newResultingString += resultingString[i];
            }
            return newResultingString;
        }

        public int bufferToInteger(Byte[] byteBuffer)
        {
            if (byteBuffer == null || byteBuffer.Length <= 0)
                return 0;
            int resultingInt = 0;
            if (byteBuffer.Length == 4)
                resultingInt = BitConverter.ToInt32(byteBuffer, 0);
            else
                resultingInt = (int)BitConverter.ToInt16(byteBuffer, 0);
            return resultingInt;
        }

        public Bitmap bufferToBitmap(Byte[] byteBuffer)
        {
            if (byteBuffer == null || byteBuffer.Length <= 0)
                return null;
            TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
            Bitmap finalBitmap = (Bitmap)tc.ConvertFrom(byteBuffer);
            finalBitmap.MakeTransparent(Color.FromArgb(255, 0, 255));
            return finalBitmap;
        }

        private void ObjectInstanceForm_Load(object sender, EventArgs e)
        {
            if (visible == 1)
                visibleCheck.Checked = true;
            updateList();
            nameText.Text = objName;
        }

        private void updateAssetBox(String dataType)
        {
            assetBox.Items.Clear();
            foreach(KeyValuePair<String,Asset> curAsset in assetDict)
            {
                if (curAsset.Value.dataType == dataType)
                    assetBox.Items.Add(curAsset.Value.name);
            }
        }

        private void updateFormAssets(String assetName)
        {
            Property selProperty = propDict[selPropText];
            if (propDict[selPropText].dataType == "String")
            {
                if (useAsset == 1)
                    valueText.Text = bufferToString(assetDict[assetName].defaultValue);
                else
                    valueText.Text = bufferToString(selProperty.defaultValue);
                sizeNum.Value = valueText.Text.Length + 1;
            }
            else if (propDict[selPropText].dataType == "Integer")
            {
                if (useAsset == 1)
                    valueText.Text = bufferToInteger(assetDict[assetName].defaultValue).ToString();
                else
                    valueText.Text = bufferToInteger(selProperty.defaultValue).ToString();
                sizeNum.Value = 4;
            }
            else if (propDict[selPropText].dataType == "Bitmap")
            {
                valueText.Enabled = false;
                dataBrowseButton.Enabled = true;
                if (propDict[selPropText].defaultValue != null)
                {
                    sizeNum.Value = selProperty.defaultValue.Length;
                    previewBox.Image = bufferToBitmap(propDict[selPropText].defaultValue);
                }
                if (useAsset == 1)
                    previewBox.Image = bufferToBitmap(assetDict[assetName].defaultValue);
            }
            else if (propDict[selPropText].dataType == "Audio")
            {
                valueText.Enabled = false;
                dataBrowseButton.Enabled = true;
                if (propDict[selPropText].defaultValue != null)
                {
                    sizeNum.Value = selProperty.defaultValue.Length;
                }
            }
            else
                sizeNum.Value = 0;
        }

        private void updateForm()
        {
            if( propDict.ContainsKey(selPropText))
            {
                Property selProperty = propDict[selPropText];
                String assetName = "";
                previewBox.Image = null;
                if (assetBox.Items.Count > 0)
                {
                    assetCheckBox.Enabled = true;
                }
                else
                    assetCheckBox.Enabled = false;
                valueText.Enabled = true;
                dataBrowseButton.Enabled = false;
                propertyNameText.Text = selPropText;
                if (useAsset == 1)
                {
                    assetName = assetBox.SelectedItem.ToString();
                    assetCheckBox.Checked = true;
                    assetBox.Enabled = true;
                }
                else
                {
                    assetCheckBox.Checked = false;
                    assetBox.Enabled = false;
                }
                updateFormAssets(assetName);
                if (useAsset == 1)
                {
                    sizeNum.Value = 0;
                    valueText.Enabled = false;
                    dataBrowseButton.Enabled = false;
                }
            }
        }

        private void propertyList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (propertyList.SelectedItems.Count > 0)
            {
                valueText.Enabled = true;
                selPropText = propertyList.SelectedItems[0].Text;
                listPos = propertyList.SelectedIndices[0];
                this.useAsset = propDict[selPropText].useAsset;
                updateAssetBox(propDict[selPropText].dataType);
                if (useAsset == 1)
                    if(propDict[selPropText].assetName != "")
                        assetBox.SelectedItem = propDict[selPropText].assetName;
                    else
                        assetBox.SelectedIndex = 0;
                updateForm();
                //TODO: Find a way to display data
                //valueText.Text = selProperty.defaultValue;
            }
        }

        private void updateList()
        {
            propertyList.Items.Clear();
            previewBox.Image = null;
            //sizeNum.Enabled = false;
            foreach (KeyValuePair<String, Property> propKeyVal in propDict)
            {
                String displayValue = "";
                if (propKeyVal.Value.dataType == "String")
                {
                    if(propKeyVal.Value.useAsset == 1)
                        displayValue = bufferToString(assetDict[propKeyVal.Value.assetName].defaultValue);
                    else
                        displayValue = bufferToString(propKeyVal.Value.defaultValue);
                }
                else if (propKeyVal.Value.dataType == "Integer")
                {
                    if (propKeyVal.Value.useAsset == 1)
                        displayValue = bufferToInteger(assetDict[propKeyVal.Value.assetName].defaultValue).ToString();
                    else
                        displayValue = bufferToInteger(propKeyVal.Value.defaultValue).ToString();
                }
                else if (propKeyVal.Value.dataType == "Bitmap")
                    displayValue = "[BITMAP]";
                else if (propKeyVal.Value.dataType == "Audio")
                    displayValue = "[AUDIO]";
                else
                    displayValue = "[OTHER]";
                ListViewItem propItem = new ListViewItem(new[] { propKeyVal.Key, propKeyVal.Value.dataType,displayValue });
                propertyList.Items.Add(propItem);
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            String propName = propertyNameText.Text;
            if (propDict.ContainsKey(propName))
            {
                Byte[] newPropValue;
                if (propDict[propName].dataType == "String")
                    newPropValue = stringToBuffer(valueText.Text, (int)sizeNum.Value);
                else if (propDict[propName].dataType == "Integer")
                    newPropValue = BitConverter.GetBytes(Int32.Parse(valueText.Text));
                else
                    if (dataBytes != null)
                        newPropValue = dataBytes;
                    else
                        newPropValue = null;
                propDict[propName].defaultValue = newPropValue;
                propDict[propName].size = (int)sizeNum.Value;
                if (useAsset == 1)
                {
                    propDict[propName].useAsset = 1;
                    propDict[propName].assetName = assetBox.SelectedItem.ToString();
                    propDict[propName].defaultValue = null;
                }
                else
                {
                    propDict[propName].useAsset = 0;
                    propDict[propName].assetName = "";
                }
            }
            else
            {
                MessageBox.Show("Name does not exists");
            }
            updateList();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (visibleCheck.Checked)
                visible = 1;
            else
                visible = 0;
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void propertyNameText_TextChanged(object sender, EventArgs e)
        {

        }

        private void valueText_TextChanged(object sender, EventArgs e)
        {
            String propName = propertyNameText.Text;
            if (propDict.ContainsKey(propName))
            {
                if (propDict[propName].dataType == "String")
                {
                    sizeNum.Value = valueText.Text.Length + 1;
                }
            }
        }

        private void propertyList_DoubleClick(object sender, EventArgs e)
        {
            String propName = propertyNameText.Text;
            if (propDict.ContainsKey(propName))
            {
                if (propDict[propName].dataType == "Bitmap")
                {
                    ImageForm imgForm = new ImageForm(bufferToBitmap(propDict[propName].defaultValue));
                    imgForm.ShowDialog();
                }
                MessageBox.Show(propDict[propName].name);
            }
        }

        private void dataBrowseButton_Click(object sender, EventArgs e)
        {
            String propName = propertyNameText.Text;
            if (openDataFileDlg.ShowDialog() == DialogResult.OK)
            {
                this.dataBytes = File.ReadAllBytes(openDataFileDlg.FileName);
                sizeNum.Value = (int)dataBytes.Length;
                if (propDict[propName].dataType == "Bitmap")
                {
                    previewBox.Image = bufferToBitmap(dataBytes);
                }
            }
        }

        private void assetCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (assetCheckBox.Checked)
            {
                useAsset = 1;
                updateAssetBox(propDict[selPropText].dataType);
                if (propDict[selPropText].assetName != "")
                   assetBox.SelectedItem = propDict[selPropText].assetName;
                else
                   assetBox.SelectedIndex = 0;
                updateForm();
                //Byte[] assetBytes = assetDict[assetBox.SelectedItem.ToString()].defaultValue;
                //sizeNum.Value = 0;
                //valueText.Enabled = false;
                //dataBrowseButton.Enabled = false;
                //if (propDict[selPropText].dataType == "Bitmap")
                //    previewBox.Image = bufferToBitmap(assetBytes);
                //if (propDict[selPropText].dataType == "String")
                //    valueText.Text = bufferToString(assetBytes);
                //if (propDict[selPropText].dataType == "Integer")
                //    valueText.Text = bufferToInteger(assetBytes).ToString();
            }
            else
            {
                useAsset = 0;
                updateForm();
                //if (listPos >= 0)
                //{
                //    propertyList.Items[listPos].Selected = true;
                //}
            }
        }

        private void assetBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (useAsset == 1 )
            {
                useAsset = 1;
                String assetName = assetBox.SelectedItem.ToString();
                updateFormAssets(assetName);
            }
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            String propName = propertyNameText.Text;
            if (propDict[propName].dataType == "Audio")
            {
                SoundPlayer soundPlayer = new SoundPlayer(new MemoryStream(propDict[propName].defaultValue));
                soundPlayer.Play();
            }
        }
    }
}
