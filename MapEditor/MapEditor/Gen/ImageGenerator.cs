﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;

namespace MapEditor.Gen
{
    public class ImageGenerator
    {
        public Dictionary<String, String> properties;

        public ImageGenerator()
        {
            properties = new Dictionary<String, String>();
        }

        public Bitmap GenerateImage(int width, int height)
        {
            return new Bitmap(width, height);
        }

    }
}
