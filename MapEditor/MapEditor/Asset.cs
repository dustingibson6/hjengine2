﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MapEditor
{
    public class Asset : Property
    {
        public Asset(String name, String type, int size, Byte[] value) : base(name, type, size, value)
        {
        }
    }
}
